#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>

#include "Allvars.h"
#include "Prototypes.h"

void allocate_variables(char filename[]/* ="NULL"*/){
	
	int i;
	char cmd[100];
	
	// Nodes parameters
	//-------------------------
	//Nod.Nconections = exec("wc -l NodesDegree.txt | awk -F\" \" \'{print $1}\'");
#ifdef CREATEMATRIX

	Netwk.Ndegree = exec("awk 'END {print NR}' ./data/NodesDegree2.txt");
	Netwk.pos = (int *)malloc(Netwk.Ndegree * sizeof(int));

	Netwk.Nnodes = prm[NODES];
	Netwk.Nedges = prm[NEDGES];
	Netwk.indegree = (int *)malloc(Netwk.Ndegree * sizeof(int));
	Netwk.outdegree = (int *)malloc(Netwk.Ndegree * sizeof(int));
	
	
		
	for(i=0;i<NROWS;i++){
	  Netwk.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); // 2 rows
	}
	
//#endif
#else

	
	sprintf(cmd,"awk 'END {print NR}' %s",filename);
	Netwk.Nnodes = exec(cmd);
	Netwk.Ndegree = Netwk.Nnodes;
#endif
	
#ifdef REWIRING
	Netwk.indegree = (int *)calloc(Netwk.Ndegree, sizeof(int));
	Netwk.outdegree = (int *)calloc(Netwk.Ndegree, sizeof(int));
	Netwk.inrnk = (double *)calloc(Netwk.Ndegree, sizeof(double));
	Netwk.outrnk = (double *)calloc(Netwk.Ndegree, sizeof(double));
#endif

	//Network parameters
	//-------------------------
	// Number of rows and columns
	Nr = Netwk.Ndegree;
	Nc = Netwk.Ndegree;
	
	Netwk.CIJ = (int *)malloc(Nr*Nc * sizeof(int));

#ifdef REWIRING
	GV.AIJ = (int *)malloc(Nr*Nc * sizeof(int));
#endif
	

	// Alocation and  Initialitation of rng in gsl
	//gsl_rng_env_setup();
	type = gsl_rng_mt19937;
	rand_generator = gsl_rng_alloc(type);
	gsl_rng_set(rand_generator,time(NULL)*getpid());
	printf("***  Variables was succesfully allocated  ***\n"); FFLUSH
}

void free_memory(void){
	int i;
	free(Netwk.indegree);
 	free(Netwk.outdegree);
// 	free(Netwk.pos);
// 	for(i=0;i<NROWS;i++){
// 	  free(Netwk.edges[i]);
// 	}
// 	free(Netwk.edges);
#ifdef REWIRING
	free(GV.AIJ);
// 	free(GV.edges[0]);
// 	for(i=0;i<NROWS;i++){
// 	  free(GV.edges[i]);
// 	}
// 	free(GV.edges);
	
#endif
	free(Netwk.CIJ);
	gsl_rng_free (rand_generator);
	printf("***  Memory was succesfully liberated  ***\n");fflush(stdout); FFLUSH
}