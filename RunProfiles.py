#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
#filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']

#Number of rewirings
Nrewiring = int(Argv[1])

#profiles folder
DirProfiles = Argv[2]

#Nprocs = int(argv[3]) 

#Reading name of profiles
NAME = np.loadtxt(filenames[0],dtype="|S14")

#Reading conditions
ifile = open(filenames[1])
COND = ifile.read().splitlines()
ifile.close()

# Total number of profiles
Ntotal = len(NAME)

# Verifying if profiledir exist.
system("rm -rf %s"%DirProfiles)
print "Creating each profile folder..."
os.mkdir(DirProfiles)
for i in xrange(Ntotal):
  os.mkdir("%s%s"%(DirProfiles,NAME[i]))  
    
print "*"*70
for i in xrange(Ntotal):
  fileassort = DirProfiles + NAME[i] + "/Assort" + NAME[i] + ".txt"
  filematrix = DirProfiles + NAME[i] + "/Matrix" + NAME[i] + ".txt"
  order = "./DPRewiring.out %d '%s' %s %s"%(Nrewiring,COND[i],fileassort,filematrix)
  
  print "\n--------> Run profile %s  --  progress: %.2f %%"%(NAME[i],(1.0*i)/Ntotal*100)
  system(order)
  print "\n"+"c"*70