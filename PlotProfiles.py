#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import commands as cm
import sys, os,time


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir


#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
#filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']

ParentDir = sys.argv[1]
nr = 100
#Rho = np.zeros((100,4))

COND = list()
NAME = list()
ifile1 = open(filenames[1],"r")
ifile2 = open(filenames[0],"r")

for line in ifile1:
  COND.append(line.strip())

for line in ifile2:
  NAME.append(line.strip())
  
cc = np.loadtxt(filenames[2],dtype='i')
Label = ["In-out","Out-In","In-In","Out-Out"]
  
N = len(COND)
  
for i in xrange(N):
  
  ifname = "./" + ParentDir + NAME[i] + "/Assort" + NAME[i] + ".txt"
  ofname = "./" + ParentDir + NAME[i]
  data = np.loadtxt(ifname)
  rw = data[:,0]
  rho = data[:,1:]
  del data

  
  #***********************************************
  # Plot of assortativity vs effective rewirings
  #***********************************************
  plt.figure()
  for j in xrange(0,4):
    plt.plot(rw,rho[:,j],'-', label=Label[j], lw=2, ms=7)
  plt.grid()
  plt.xlabel("Number of rewiring")
  plt.ylabel("Assortativity")
  plt.legend(loc="upper left",fontsize=15)
  plt.title("profile:   in-out(%d)   out-in(%d)   in-in(%d)   out-out(%d)"%(cc[i,0],cc[i,1],cc[i,2],cc[i,3]),fontsize=18)
  plt.savefig("%s/EffectiveRewiring.pdf"%ofname)
  plt.close()
  
  #***********************************************
  # Plot of assortativity vs real rewirings
  #***********************************************
  plt.figure()
  for j in xrange(4):
    plt.plot(rho[:,j],'-', label=Label[j], lw=2, ms=7)
  plt.grid()
  plt.xlabel("Real Rewiring")
  plt.ylabel("Assortativity")
  plt.legend(loc="upper left",fontsize=15)
  plt.title("profile:   in-out(%d)   out-in(%d)   in-in(%d)   out-out(%d)"%(cc[i,0],cc[i,1],cc[i,2],cc[i,3]),fontsize=18)
  plt.savefig("%s/RealRewiring.pdf"%ofname)
  plt.close()
  
  plt.figure()
  x = range(4)
  plt.plot(x,rho[0,:],'g^--',label='Initial',markersize=8,linewidth=2)
  plt.plot(x,rho[-1,:],'ro--',label='Rewired',markersize=8,linewidth=2)
  plt.xticks(x,Label,fontsize=18)
  plt.yticks(fontsize=18)
  plt.ylabel("Assortativity")
  plt.xlim(xmin=-1,xmax=4)
  plt.ylim(ymin=-1,ymax=1)
  plt.legend(fontsize=12)
  plt.grid()
  plt.savefig("%s/Profiles.pdf"%ofname)
  plt.close()
  
