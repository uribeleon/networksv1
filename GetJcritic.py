#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
from mymodules import *
from Colors import *
import commands as cm
import scipy as sp
import scipy.linalg as la
import scipy.optimize as so
import scipy.stats as st
import scipy.misc as ms
import argparse
import textwrap


import matplotlib as mpl
from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter
from matplotlib.ticker import MultipleLocator, AutoMinorLocator, FormatStrFormatter

#***************************
# DEFINING PARSER 
#***************************
parser=argparse.ArgumentParser(
  prog='JcriticProfiles.py',
  formatter_class=argparse.RawDescriptionHelpFormatter,
  description=textwrap.dedent('''\
**********************************************************************************************************************
  Breve descripcion
    '''),epilog='''
**********************************************************************************************************************
  ''')

#***************************
# PARSER ARGUMENTS OPTIONS
#***************************
#parser.add_argument('option', action="store")
parser.add_argument('-if',dest='ifile',action="store",default=0,help="path to initial file",type=str)
parser.add_argument('-p',dest='pathDir',action="store",default=0,help="path of parent folder",type=str)
args=parser.parse_args()

#OPTION = args.option
PATHD = args.pathDir

#=======================
# SIGMOID FUNCTION
#=======================
def sigmoid(x,*Args):
  a,b,c,d = Args
  return c/(1.0 + np.exp( -b*(x-a) ) ) + d

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
argv = sys.argv



#////////////////////////////////////////
# READING CONDITIONS OF EACH PROFILES
#////////////////////////////////////////
  
ParentDir = args.pathDir

Nl = int(get("ls %sAssort* | wc -l"%ParentDir))  
name = ParentDir.split("/")[-2]  

Jcritic = np.zeros(Nl)
Sigma = np.zeros(Nl)
LABELFILE = """\
###########################################################
 JCRITIC       SIGMA
###########################################################"""

#////////////////////////////////////////
# FITTING FOR INITIAL PROFILE
#////////////////////////////////////////

# Initial Guess
#a=30.0   #10.0 	 
a=30.0   #10.0   
b=0.424   #2.0    
c=95.0   #100.0  
d=0.1   # 0.001 
guess = (a, b, c, d)

# Reading values of Firing rate for each profile
Jo,Ro = np.loadtxt("%sMFRS_Minitial_h0.txt"%(PATHD),unpack=True)


# Making fit of the data
params, params_covariance = so.curve_fit(sigmoid, Jo, Ro, guess)
a,b,c,d = params
Joc = a


#////////////////////////////////////////
# FITTING FOR EACH PROFILE
#////////////////////////////////////////
for n in xrange(Nl):
  
  print "Profile --> %d     progress: %.2f %%"%(n,(100.0*(n+1))/(Nl))
  
  # Reading values of Firing rate for each profile
  J,R = np.loadtxt("%sMFRS_vs_J-h0_V%d.dat"%(ParentDir,n),unpack=True)

  # Initial Guess
  a=30.0   #10.0   
  b=0.424   #2.0    
  c=95.0   #100.0  
  d=0.1   # 0.001 
  guess = (a, b, c, d)

  # Making fit of the data
  params, params_covariance = so.curve_fit(sigmoid, J, R, guess)
  a,b,c,d = params
  ARGS = (a,b,c,d)

  # calculating the theoretical curve with the results of fit
  xr = np.arange(0,40,0.01)
  y = sigmoid(xr,*ARGS)

  #calculating the numerical detivate of theoretical curve
  Yderiv = ms.derivative(sigmoid,xr,dx=1e-6,args=(a,b,c,d))
  #idx = np.where(Yderiv>0.1)
  #jcrit = xr[idx[0]]
  Sigma[n] = np.sqrt(2*np.pi**2/(6*b**2))
  Jcritic[n] = a

    

# Save values in a table
JcProfiles = np.array(zip(Jcritic,Sigma),dtype=[('Jcritic','float64'),('Sigma','float64')])
np.savetxt("%sJcritics_trials.txt"%ParentDir,JcProfiles,fmt="%12f %12f",header=LABELFILE)


#--------------------------------------
# PLOT JCRITIC vs PROFILES (ORDERED)
#--------------------------------------
#JcProfiles.sort(order='Jcritic')
#index = [ i for i in xrange(len(JcProfiles['NAME'])) if JcProfiles['NAME'][i] in NAME]

majorLocator = MultipleLocator(2) #step between ticks
minorLocator = MultipleLocator(1) #number of minor ticks between ticks

fig = plt.figure(figsize=(10,6))
ax=fig.add_subplot(111)
plt.plot(range(1,Nl+1),Jcritic,'go--')
plt.axhline(y=Joc,xmax=Nl-1,label="$J_{0,critic} = %.3f$"%Joc,lw=2)
plt.ylabel("Jcritic")
plt.ylabel("Trials")
plt.axes().xaxis.set_minor_locator(minorLocator)
plt.axes().xaxis.set_major_locator(majorLocator)
plt.grid(which="major",color='k',linestyle=':',lw=0.1,alpha=0.5)
#plt.minorticks_on()
plt.grid(which="minor",axis='y',color='k',linestyle=':',lw=0.1,alpha=0.5)
plt.legend(loc="best")
plt.savefig("%sJcriticTrials.pdf"%ParentDir,bbox_inches='tight')
#plt.close()

#ofile.write("%10s %12f %12f\n"%(NAME[n],Jcritic[n],))
