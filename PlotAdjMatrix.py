import matplotlib.pyplot as plt
import os,sys
import math
import commands as Cm
import scipy.linalg as la
from mymodules import *
from matplotlib import cm
import matplotlib as mpl

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GENERAL PARAMETERS OF PLOT
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mpl.rcParams['xtick.labelsize'] = 17
mpl.rcParams['ytick.labelsize'] = 17
mpl.rcParams['font.family'] = 'Freeserif' #cmr10
#mpl.rcParams['xtick.minor.size'] = 3
mpl.rcParams['font.size'] = 23.0

#==============
# SHORCUTS
#==============
system=os.system
get=Cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#namefile
pathdir = Argv[1]
namefile = Argv[2]


Ai = np.loadtxt(namefile)
#plt.imshow(4*AIJ/6.0,cmap=cm.YlOrRd,vmax=1,vmin=0)

Edgesi = np.transpose(Ai.nonzero())
din,dout = GetDegree(Ai)
colorsi=din[Edgesi[:,0]] + dout[Edgesi[:,0]] + din[Edgesi[:,1]] + dout[Edgesi[:,1]]


#======================================================================
# PLOT OF MATRIX
#======================================================================
plt.figure(figsize=(16,14))
plt.scatter(Edgesi[:,0],Edgesi[:,1],c=colorsi,s=6,edgecolor='none',cmap=cm.jet)
plt.colorbar()
#plt.plot(Edgesi[:,0],Edgesi[:,1],'o',color=(.38,.80,.98),ms=1,mec=(0.03, 0.62, 0.87))
plt.xlabel("Row")
plt.ylabel("Column")
plt.xlim([0,500])
plt.ylim([0,500])
plt.title("Initial Matrix")
plt.savefig("%sInitialMatrix.pdf"%pathdir)