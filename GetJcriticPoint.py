#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import numpy as np
import scipy.linalg as la
import scipy.optimize as so
import scipy.stats as st
import scipy.misc as ms
import scipy.interpolate as sintp
import sys,os
import commands as cm
from scipy.ndimage import gaussian_filter
from Colors import *

import matplotlib as mpl
mpl.use('PDF')
mpl.style.use('classic')
from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter
from matplotlib.ticker import MultipleLocator, AutoMinorLocator, FormatStrFormatter



#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
argv = sys.argv

def CalculateJcritic(J,Fr,maxDeriv=2,method='normal'):

    #---------------------------------------------------
    # Smoothing values of Fr with gaussian filter
    smoothed = gaussian_filter(Fr, 1)

    # determining h
    #---------------------------------------------------
    h = J[1]-J[0]

    #derivative 1
    #---------------------------------------------------
    deriv1 = (smoothed[1:] - smoothed[:-1])/h

    #derivative 2
    #---------------------------------------------------
    deriv2 = np.gradient(smoothed,h)

    # determining statistical values in asymptotic branches
    #---------------------------------------------------
    Ndata = 0
    # >>> derivatives less equal to 2
    locs = deriv2<=maxDeriv
    # >>> points where the sigmoid begins and ends up growing
    cutpoint,cutpointAbove = np.where(locs==False)[0][[0,-1]]
    # calculating the mean and std of asymptotic branches
    mean_cutoff_below= smoothed[:cutpoint][-Ndata:].mean()
    std_cutoff_below= smoothed[:cutpoint][-Ndata:].std()
    mean_cutoff_above = smoothed[cutpointAbove:][-Ndata:].mean()
    std_cutoff_above = smoothed[cutpointAbove:][-Ndata:].std()


    #---------------------------------------------------
    # get the critic points above and below.
    #---------------------------------------------------

    # Determining the value of Jcritic by filter of
    # point above of 3sigma
    #--------------------------------------------------------
    supLimit_below = mean_cutoff_below + 3*std_cutoff_below
    mask = smoothed>supLimit_below
    for i in range(len(mask)-1,0,-1):
        if mask[i]==False:
            argJcr = i
            break


    # Determining the value of Jcritic by filter of
    # point below of 3sigma
    #--------------------------------------------------------
    infLimit_above = mean_cutoff_above - 3*std_cutoff_above
    mask_above = smoothed< infLimit_above
    for i in range(len(mask)-1,0,-1):
        if mask_above[i]==True:
            argJend = i
            break

    # Determining Jcritic using linear interpolation
    #--------------------------------------------------------
    # >>> linear interpolation
    if(method=="linear"):
        # BELOW ZONE ---------------------------------------------------
        # >>> function to get interpolation
        f = sintp.interp1d(J[argJcr:argJcr+2],smoothed[argJcr:argJcr+2])
        # >>> range of x to do interpolation
        xbetween = np.linspace( J[argJcr] , J[argJcr+1] , 100 , endpoint=False)
        # >>> Calculating the point just above the limit
        argSuplim = np.where( f(xbetween) >= supLimit_below )[0][0]
        # >>> Calculating Jcritic with the interpolation function
        Jcr = xbetween[argSuplim] #jcritic
        Fjcr = f(Jcr)
        # ABOVE ZONE ----------------------------------------------------
        # >>> function to get interpolation
        f = sintp.interp1d(J[argJend:argJend+2],smoothed[argJend:argJend+2])
        xbetween = np.linspace(J[argJend],J[argJend+1],100,endpoint=False)
        # >>> Calculating the point just below the limit
        argInflim = np.where( f(xbetween) <= infLimit_above )[0][-1]
        # >>> Calculating J value above with the interpolation function
        Jabove = xbetween[argInflim]
        FRabove = f(Jabove)
    #----------------
    else:
        Jcr = J[argJcr]
        Jabove = J[argJend]
        Fjcr = smoothed[argJcr]
        FRabove = smoothed[argJend]


    # calculating the sigma value of the sigmoid
    #--------------------------------------------------------
    sigma = (Jabove - Jcr)/6.0

    # save values in array
    statBranches = {'meanBelow':mean_cutoff_below,
                    'meanAbove':mean_cutoff_above,
                    'stdBelow':std_cutoff_below,
                    'stdAbove':std_cutoff_above}

    Pbelow = np.array([Jcr,Fjcr])
    Pabove = np.array([Jabove,FRabove])
    if(method=="normal"):
        return np.array([Pbelow,Pabove,sigma,statBranches,smoothed])
    else:
        # interpPoints = np.array([xbetweenB,ybetween])
        return np.array([Pbelow,Pabove,sigma,statBranches,smoothed])



#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#  MAIN PROGRAM
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OPTION = argv[1]
PATHD = argv[2]

#==================================================================================
if OPTION == "ONEP":

    #J,Fr = np.loadtxt("%sMFRS_Minitial_h0.dat"%(PATHD),unpack=True)

    #J,Fr = np.loadtxt("%sMFRS_vs_J-h0.dat"%(PATHD),unpack=True)
    #J,Fr = np.loadtxt("%sMFRS_Minitial_h0.txt"%(PATHD),unpack=True)
    J,Fr = np.loadtxt("%s"%(PATHD),unpack=True)

    # Determinig parameters to calculate Jcritic
    Pbelow,Pabove,stdFr, stat, smootpoints = CalculateJcritic(J,Fr,method='linear')
    #Pbelow,Pabove,stdFr, stat, smootpoints = CalculateJcritic(J,Fr,method='normal')

    Jcritic = Pbelow[0]
    FrJcr = Pbelow[1]
    Jabove = Pabove[0]
    FrJup = Pabove[1]

    # ------------------------------------------------------
    # PLOT THE CURVE
    # ------------------------------------------------------
    plt.figure()
    plt.plot(J,smootpoints,'o-',label="gaussian_filter",ms=2,lw=2)
    plt.plot(J,Fr,'--',label="curve",lw=4)
    # Sigma zone below
    plt.axhspan(ymin=stat["meanBelow"],ymax=stat["meanBelow"] + 3*stat["stdBelow"],color='yellow',alpha=0.5)
    plt.axhline(y=stat["meanBelow"],ls='--',c='k',lw=2)
    plt.axhline(y=stat["meanBelow"] + 3*stat["stdBelow"],ls='-',c='k',lw=2)
    # Sigma zone above
    plt.axhspan(ymin=stat["meanAbove"] - 3*stat["stdAbove"],ymax=stat["meanAbove"],color='yellow',alpha=0.5)
    plt.axhline(y=stat["meanAbove"],ls='--',c='k',lw=2)
    plt.axhline(y=stat["meanAbove"] - 3*stat["stdAbove"],ls='-',c='k',lw=2)
    # Critic points
    plt.plot(Jcritic,FrJcr,'o',ms=10,c='r')
    plt.plot(Jabove,FrJup,'o',ms=10,c='r')
    # Sigma zone of sigmoid
    plt.axvline(x=Jcritic,ls='--',c='c',lw=2)
    plt.axvline(x=Jabove,ls='--',c='c',lw=2)
    #------------------------
    #plt.xticks(np.arange(0,46,1),fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.ylim([0,110])
    plt.xlim([20,30])
    plt.xlabel("J",fontsize=16)
    plt.ylabel("% of active neurons",fontsize=16)
    #------------------------
    plt.grid()
    plt.legend()
    plt.show()

#==================================================================================
if OPTION == "ALLP":
    #////////////////////////////////////////
    # READING CONDITIONS OF EACH PROFILES
    #////////////////////////////////////////
    NAME = list()
    ifile = open("./data/names.txt","r")
    for line in ifile:
        NAME.append(line.strip())


    Nl = len(NAME)
    Size = int(get("awk 'END {print NR}' %s"%(PATHD+"MatrixInitial.txt")))

    Jcritic = np.zeros(Nl)
    Sigma = np.zeros(Nl)
    LABELFILE = """\
###########################################################
 PROFILE      JCRITIC       SIGMA       JcrO
###########################################################"""

    #///////////////////////////////////////////
    # CALCULATING JCRITIC FOR INITIAL PROFILE
    #///////////////////////////////////////////

    Jo,Ro = np.loadtxt("%sMFRS_Minitial_h0.txt"%(PATHD),unpack=True)
    JoPts = CalculateJcritic(Jo,Ro,method='linear')[0]
    Joc = JoPts[0]

    #////////////////////////////////////////
    # CALCULATING JCRITIC FOR EACH PROFILE
    #////////////////////////////////////////
    Nsgm = 3 # delta in sigma
    Nst = 0.5 #delta in sigma for stimulated study
    plt.figure() # to avoid open several figures in RAM

    for n in xrange(Nl):
        print "Profile --> %s     progress: %.2f %%"%(NAME[n],(100.0*(n+1))/(Nl))

        # Reading values of Firing rate for each profile
        J,R = np.loadtxt("%s%s/MFRS_vs_J-h0.dat"%(PATHD,NAME[n]),unpack=True)

        PtsBelow,PtsAbove,stdFr,stat,smoothed = CalculateJcritic(J,R,method='linear')
        Jcritic[n] = PtsBelow[0] #J[locJcr]
        Sigma[n] = stdFr

        # Plot curve
        #-------------------------------------------------------------------------------

        plt.plot(J,R,'-',color=CL["Goldenrod 1"],lw=2,label="Firing Rate")
        plt.plot(J,smoothed,'-',color=CL["Indianred"],lw=2,label="smoothed",alpha=0.7)
        plt.axvline(x=Jcritic[n]+Nsgm*Sigma[n],ymin=0,ymax=100,color="b",lw=2,ls="--")
        plt.axvspan(xmin=Jcritic[n],xmax=Jcritic[n]+ 2*Nsgm*Sigma[n],ymax=100,color="b",alpha=0.2)
        #------------------------------------------------------------------------------------------------
        # annotation of jcritics
        plt.annotate("$J_{critic}$",xy=(Jcritic[n],1),xytext=(Jcritic[n]*0.7,15),
                    xycoords='data',textcoords='data',size='18',horizontalalignment='right',
                    arrowprops=dict(arrowstyle="fancy", #linestyle="dashed",
                                color="0.4",
                                shrinkB=5,
                                connectionstyle="arc3,rad=-0.5",
                                fc=(1,0.4,0.4),
                                ec='r'
                                ),)
        #--------------------------------------------------------------------
        # annotation over J selected to do sensitivity
        plt.annotate("",xy=(Jcritic[n],50),
                     xytext=(Jcritic[n]*Nst,50),xycoords='data',textcoords='data',size='10',
                     arrowprops=dict(arrowstyle="|-|",lw=2))
        plt.annotate("",xy=(Jcritic[n],50),
                     xytext=(Jcritic[n]*Nst,50),xycoords='data',textcoords='data',size='18',
                     arrowprops=dict(arrowstyle="<->"))
        plt.text(Jcritic[n]*0.5*(Nst + 1), 54, '$%.1fJ_{cr}$'%(1-Nst),fontsize='18',horizontalalignment='center')
        #--------------------------------------------------------------------
        # annotation of 3sigma
        a = Jcritic[n] + Nsgm*Sigma[n]
        plt.annotate("",xy=(a,50),xytext=(a+Nsgm*Sigma[n],50),xycoords='data',textcoords='data',size='10',
                    arrowprops=dict(arrowstyle="|-|",ec='r',lw=2))
        plt.text(a+3*Sigma[n]+1, 54, '$%d\sigma$'%Nsgm,fontsize='18',horizontalalignment='center')
        #------------------------------------------------------------------------------------------------
        plt.xlim(xmax=45)
        plt.xlabel("$J$",fontsize="18")
        plt.ylabel("% of active neurons",fontsize="18")
        plt.title("Profile: %s"%NAME[n])
        plt.xticks(range(0,45,5),fontsize=18)
        plt.yticks(fontsize=18)
        plt.grid()
        plt.legend(loc="best")
        plt.savefig("%s%s/Sigmoid2.pdf"%(PATHD,NAME[n]))
        #plt.close()
        plt.clf()

    # Save values in a table
    J0C = np.ones(Nl)*Joc
    #name = ["\"" + namep.replace("_"," ") + "\"" for namep in NAME]
    JcProfiles = np.array(zip(NAME,Jcritic,Sigma,J0C),dtype=[('NAME','S14'),('Jcritic','float64'),
                                                             ('Sigma','float64'),('J0C','float64')])
    np.savetxt("%sJcritics2.txt"%PATHD,JcProfiles,fmt="%12s %12f %12f %12f",header=LABELFILE)

#plt.show(block=False)
#plt.draw()
#plt.pause(0.001)
