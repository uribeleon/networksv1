from mymodules import *


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Creating vector of degrees (in,out) and
# Zero-entrance adjacency matrix
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Od = np.array([5,2,4,5,3,4])
Ind = np.zeros_like(Od)
#while np.sum(Ind)!=np.sum(Od):
#	Ind = np.random.randint(2,6,size=6)
Ind = np.array([4,5,2,4,3,5])
#Ind = np.array([3,5,3,4,5,3])

# Load from file
A = np.loadtxt("./data/MatrixInitial_500.txt")
Ind,Od = GetDegree(A)
Nnodes = len(Od)
Nmax = Nnodes-1
#C = np.zeros((Nnodes,Nnodes))
BiDS = {"In":Ind,"Out":Od}

C,StubsV = StackAlgorithm(BiDS,"OutIn",1)


"""
idOut = np.argsort(Od)
idIn = np.argsort(Ind) # In-degree sorted by arguments (id of the nodes)
OutS = Od[idOut].copy()
InS = Ind[idIn].copy() # In-degree sorted
M = np.sum(Od)

# loop for sorted out degree vector
for i in xrange(Nmax,-1,-1):
	# loop for sorted in degree vector
	for j in xrange(Nmax,-1,-1):

		if InS[j] > 0:
			if C[ idOut[i], idIn[j]] != 1 and idOut[i]!=idIn[j]:
				C[ idOut[i], idIn[j]] = 1
				InS[j] = InS[j] - 1
				OutS[i] = OutS[i] - 1
		else:
			continue

		if OutS[i] == 0: break
"""




