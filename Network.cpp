#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Allvars.h"
#include "Prototypes.h"


void makeCIJ_degreefixed(void){
  int i,j,row,column;
  int trash,Newindex;
  int warning;

  gsl_ran_shuffle(rand_generator,Netwk.edges[1],Netwk.Nedges,sizeof(int));

  for(i=0;i<Netwk.Nedges;i++){
    row = Netwk.edges[0][i];
    column = Netwk.edges[1][i];

    if(Netwk.CIJ[row*Nc+column]==1 or row==column){
      warning = 1;
// 	    printf("row: %d   column: %d i:%d\n",row,column, i );
      while(1){
	Newindex = (int)ceil(Netwk.Nedges*gsl_rng_uniform(rand_generator));
	// Se verifica si el par de conexiones que resultan
	// en la permutacion no existe
	if ( !( Netwk.CIJ[ Netwk.edges[0][i]*Nc + Netwk.edges[1][Newindex] ] or 	
		Netwk.CIJ[ Netwk.edges[0][Newindex]*Nc + Netwk.edges[1][i] ] ) )
	{	
	  //printf("i: %d Ni:%d ,  %d %d %d %d\n",i,Newindex,Netwk.edges[0][i],Netwk.edges[1][Newindex],Netwk.edges[0][Newindex],Netwk.edges[1][i] );
	  if( !( Netwk.edges[0][i] == Netwk.edges[1][Newindex] or 
		Netwk.edges[0][Newindex] == Netwk.edges[1][i] ) )
	  {
// 	 					printf("No Son iguales\n");
	    //Se crea la nueva conexion
	    Netwk.CIJ[ Netwk.edges[0][i]*Nc + Netwk.edges[1][Newindex] ] = 1;

	    // Si la conexion resultante del indice permutado ya existia 
	    // y se habia creado, se elimina dicha conexion.
	    if(Newindex<i){
	      //Se elimina la antigua conexion
	      Netwk.CIJ[ Netwk.edges[0][Newindex]*Nc + Netwk.edges[1][Newindex] ] = 0; 
	      // Se crea la otra conexion
	      Netwk.CIJ[ Netwk.edges[0][Newindex]*Nc + Netwk.edges[1][i] ];
	    }

	    // Se reescribe el vector de conexiones
	    trash = Netwk.edges[1][i];
	    Netwk.edges[1][i] = Netwk.edges[1][Newindex];
	    Netwk.edges[1][Newindex] = trash;
	    break;
	  }
	}
	warning = warning + 1;
	if(warning>2*Netwk.Nedges*Netwk.Nedges){
	  printf(" ### Iteration limit was reached ###\n");
	  gsl_ran_shuffle(rand_generator,Netwk.edges[1],Netwk.Nedges,sizeof(int));
	  warning=1; 
	  i=0;
	  printf(" ===== The program has been terminated ====\n");
	  exit(0);
	}	
      }
		  
    }
    else{
      Netwk.CIJ[row*Nc+column]=1;
    }
  }
  
  printf("***  Matrix was created with exit  ***\n"); FFLUSH
  
  // Saving Matrix in an output file
  SaveMatrix(Netwk.CIJ,"AdjacencyMatrix.txt");
  printf("***  Matrix was saved in file 'AdjacencyMatrix.txt' \n"); FFLUSH
  
  //Saving Edges in an ouput file
  SaveEdges("Edges.txt");
  printf("***  Table of edges was saved in file 'Edges.txt' \n"); FFLUSH
}



void MakeCIJ_Fast(void){
	int i,j,row,column;
		
	// Permuting out-edge vector
	gsl_ran_shuffle(rand_generator,Netwk.edges[1],Netwk.Nedges,sizeof(int));
	
	//Creating Adjacency Matrix
	for(i=0;i<Netwk.Nedges;i++){
	  
	  row = Netwk.edges[0][i];
	  column = Netwk.edges[1][i];
	  Netwk.CIJ[row*Nc+column]+=1;	  
	}
	
	//---------------------------------------------------
	//Eliminating autoloops and repeated edges
	//---------------------------------------------------
	for(i=0;i<Netwk.Nnodes;i++){
	  
	  // Autoloops
	  if(Netwk.CIJ[i*Nc+i]>0){
	    Nloops_deleted = Netwk.CIJ[i*Nc+i] + Nloops_deleted;
	    Netwk.CIJ[i*Nc+i]=0;
	  }
	    
	  for(j=0;j<Netwk.Nnodes;j++){
	    
	    // repeated edges
	    if(Netwk.CIJ[i*Nc+j]>1){
	      Nrepeated_deleted = Netwk.CIJ[i*Nc+j] + Nrepeated_deleted - 1;
	      Netwk.CIJ[i*Nc+j] = 1;
	      
	    }
	    
	  }
	}
	
	printf("***  Matrix was created with exit***\n"); FFLUSH
	
	// Saving Matrix in an output file
	SaveMatrix(Netwk.CIJ,"./data/AdjacencyMatrix0.txt");
	printf("*** Matrix was saved in file './data/AdjacencyMatrix0.txt' \n"); FFLUSH
	
	//Saving Edges in an ouput file
	SaveEdges("./data/Edges.txt");
	printf("*** Table of edges was saved in file 'Edges.txt' \n"); FFLUSH
}

void SaveMatrix(int *CIJ,char filename[],bool tp/*=false*/){
      int i,j;
      FILE *Thefile;
      
      // ASCII
      if(tp == false){
	Thefile = fopen(filename,"w");
	for(i=0;i<Nr;i++){
	  for(j=0;j<Nc;j++){
	    fprintf(Thefile,"%d ",CIJ[i*Nc+j]);
	  }
	  fprintf(Thefile,"\n");
	}
// 	fclose(Thefile);
      }
      // BINARY
      else{
	strcat(filename,".Bin");
	Thefile = fopen(filename,"wb");
	fwrite(&CIJ,sizeof(int),Nc*Nr,Thefile);
// 	fclose(Thefile);
      }
//       printf("dir-%p\n",Thefile);
      fcloseall();
}

void SaveEdges(char filename[]){
  int i,j;
  FILE *Ofile;
  
  Ofile = fopen(filename,"w");
  for(i=0;i<Netwk.Nnodes;i++){
    for(j=0;j<Netwk.Nnodes;j++){
      if(Netwk.CIJ[i*Nc+j]==1){
	fprintf(Ofile,"%d %d\n",i,j);
      }
    }
  }
  fclose(Ofile);
}

void ReadMatrix(char filename[]){
  
  int row,column;
  char cmd[100];
  FILE *fl;
  

  fl = fopen(filename,"r");
  
  for(row=0;row<Netwk.Nnodes;row++){
    for(column=0;column<Netwk.Nnodes;column++){
      fscanf(fl,"%d",&Netwk.CIJ[row*Nc+column]);
    }
  }
  fclose(fl);
  printf("***  Matrix was read succesfully ***\n"); FFLUSH
}

void GetDegreeNodes(int AIJ[]){
  
  int i,j;
 
  // Calculating Out-degree and In-degree
  for(i=0;i<Netwk.Nnodes;i++) // rows
  {
    for(j=0;j<Netwk.Nnodes;j++) // columns
    {
      Netwk.outdegree[i] = Netwk.outdegree[i] + AIJ[i*Nc+j];
      Netwk.indegree[j] = Netwk.indegree[j] + AIJ[i*Nc+j];
    }
  }
}

int GetNedges(int AIJ[]){
  
  int i,j;
  int nedges=0;
  for(i=0;i<Netwk.Nnodes;i++){
    for(j=0;j<Netwk.Nnodes;j++){
      nedges = nedges + AIJ[i*Nc + j];
    }
  }
  return nedges;
}

void GetEdges(int AIJ[], int *ed[NROWS]){
  
  int i,j,ne=0;
  
  for(i=0;i<Nr;i++){
    for(j=0;j<Nc;j++){
      if( AIJ[i*Nc+j] == 1 )
      {
	ed[OUT][ne] = i;
	ed[IN][ne] = j;
	ne++;
      }
    }
  }
}

double Assortativity(int *AIJ, int flag, double *Terms,bool opt ){
  int i;
  double jmean=0.,kmean=0.;
  double sigmaj=0.,sigmak=0.;
  double Minv,rho;
  double sum;
  
  Minv = 1.0/Netwk.Nedges;
  GetEdges(AIJ,GV.edges);
  
  //REMEMBER: j.-> source(OUT)   and k -> target(IN)
  
  //-----------------------------------------
  // (alpha,beta) = (In,Out)
  //-----------------------------------------
  if( flag == 0)
  {
      
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.indegree[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.outdegree[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.indegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.indegree[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.outdegree[ GV.edges[IN][i] ] - kmean)*(Netwk.outdegree[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.indegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.outdegree[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  //-----------------------------------------
  // (alpha,beta) = (Out,In)
  //-----------------------------------------
  else if( flag == 1)
  {
      
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.outdegree[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.indegree[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.outdegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.outdegree[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.indegree[ GV.edges[IN][i] ] - kmean)*(Netwk.indegree[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.outdegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.indegree[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  //-----------------------------------------
  // (alpha,beta) = (In,In)
  //-----------------------------------------
  else if( flag == 2)
  {
      
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.indegree[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.indegree[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.indegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.indegree[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.indegree[ GV.edges[IN][i] ] - kmean)*(Netwk.indegree[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.indegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.indegree[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  //-----------------------------------------
  // (alpha,beta) = (Out,Out)
  //-----------------------------------------
  else if( flag == 3)
  {
      
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.outdegree[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.outdegree[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.outdegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.outdegree[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.outdegree[ GV.edges[IN][i] ] - kmean)*(Netwk.outdegree[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.outdegree[ GV.edges[OUT][i] ] - jmean)*(Netwk.outdegree[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  else{
    printf("You must choose a valid configuration (0,1,2,3)");
  }
  
  if(opt){
    Terms[JMEAN] = jmean;
    Terms[KMEAN] = kmean;
    Terms[SIGMJ] = sigmaj;
    Terms[SIGMK] = sigmak;
  }
  return rho;
}

double SpearmanRho(int *AIJ, int flag, double *Terms,bool opt)
{
  int i;
  double jmean=0.,kmean=0.;
  double sigmaj=0.,sigmak=0.;
  double Minv,rho;
  double sum;
  
  Minv = 1.0/Netwk.Nedges;
  GetEdges(AIJ,GV.edges);
  
  //REMEMBER: j.-> source(OUT)   and k -> target(IN)
  
  //-----------------------------------------
  // (alpha,beta) = (In,Out)
  //-----------------------------------------
  if( flag == 0)
  {
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.inrnk[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.outrnk[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.inrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.inrnk[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.outrnk[ GV.edges[IN][i] ] - kmean)*(Netwk.outrnk[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.inrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.outrnk[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  //-----------------------------------------
  // (alpha,beta) = (Out,In)
  //-----------------------------------------
  else if( flag == 1)
  {
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.outrnk[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.inrnk[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.outrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.outrnk[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.inrnk[ GV.edges[IN][i] ] - kmean)*(Netwk.inrnk[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.outrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.inrnk[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  //-----------------------------------------
  // (alpha,beta) = (In,In)
  //-----------------------------------------
  else if( flag == 2)
  {
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.inrnk[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.inrnk[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.inrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.inrnk[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.inrnk[ GV.edges[IN][i] ] - kmean)*(Netwk.inrnk[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.inrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.inrnk[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  //-----------------------------------------
  // (alpha,beta) = (Out,Out)
  //-----------------------------------------
  else if( flag == 3)
  {
    //Calculating terms
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating Jmean
      jmean = jmean + Netwk.outrnk[ GV.edges[OUT][i] ]; // source
      // Calculating Kmean
      kmean = kmean + Netwk.outrnk[ GV.edges[IN][i] ]; //target
    }
    jmean = jmean*Minv; 
    kmean = kmean*Minv; 
    
    for(i=0;i<Netwk.Nedges;i++){
      // Calculating SigmaJ
      sigmaj = sigmaj + (Netwk.outrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.outrnk[ GV.edges[OUT][i] ] - jmean);
      // Calculating SigmaK
      sigmak = sigmak + (Netwk.outrnk[ GV.edges[IN][i] ] - kmean)*(Netwk.outrnk[ GV.edges[IN][i] ] - kmean);
    }
    sigmaj = sqrt(sigmaj*Minv);
    sigmak = sqrt(sigmak*Minv);
    
    //Calculating assortativity
    for(i=0;i<Netwk.Nedges;i++){
      sum = sum + (Netwk.outrnk[ GV.edges[OUT][i] ] - jmean)*(Netwk.outrnk[ GV.edges[IN][i] ] - kmean);
    }
    rho = Minv*sum/(sigmaj*sigmak);
  }
  else{
    printf("You must choose a valid configuration (0,1,2,3)");
  }
  
  if(opt){
    Terms[JMEAN] = jmean;
    Terms[KMEAN] = kmean;
    Terms[SIGMJ] = sigmaj;
    Terms[SIGMK] = sigmak;
  }
  return rho;
  
}

void Rewire(int *AIJ,int *Ed[NROWS],int EdgesR[4], int *flag){
  
  int n1,n2;
  int a,b,c,d;
  
  n1 = gsl_rng_uniform_int(rand_generator,Netwk.Nedges);
  n2 = gsl_rng_uniform_int(rand_generator,Netwk.Nedges);
  
  // Verifying autoloops
  if( Ed[OUT][n1] == Ed[IN][n2] or Ed[OUT][n2] == Ed[IN][n1]){
    *flag = 0;
  }
  else
  {
    a = Ed[OUT][n1];   b = Ed[IN][n1];
    c = Ed[OUT][n2];   d = Ed[IN][n2];
    
    // Verifiying existing connections
    if( not ( AIJ[a*Nc + d] or AIJ[c*Nc + b]))
    {
      AIJ[a*Nc + d] = 1;   AIJ[a*Nc + b] = 0;
      AIJ[c*Nc + b] = 1;   AIJ[c*Nc + d] = 0;
      
      // Save nodes rewired
      EdgesR[0] = a;   EdgesR[1] = b;
      EdgesR[2] = c;   EdgesR[3] = d;
      *flag = 1;
    }
    else *flag = 0;
  }
}

double DeltaR(double *tm,int edR[4],int flag){
  
  double delta,Delta;
  double ft;

  ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  
  // In-Out
  if(flag == 0){
    delta = (Netwk.indegree[edR[0]]-tm[JMEAN])*(Netwk.outdegree[edR[3]]-tm[KMEAN]) + \
	    (Netwk.indegree[edR[2]]-tm[JMEAN])*(Netwk.outdegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.indegree[edR[0]]-tm[JMEAN])*(Netwk.outdegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.indegree[edR[2]]-tm[JMEAN])*(Netwk.outdegree[edR[3]]-tm[KMEAN]);
  }
  // Out-In
  else if(flag == 1){
    delta = (Netwk.outdegree[edR[0]]-tm[JMEAN])*(Netwk.indegree[edR[3]]-tm[KMEAN]) + \
	    (Netwk.outdegree[edR[2]]-tm[JMEAN])*(Netwk.indegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outdegree[edR[0]]-tm[JMEAN])*(Netwk.indegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outdegree[edR[2]]-tm[JMEAN])*(Netwk.indegree[edR[3]]-tm[KMEAN]);
  }
  // In-In
  else if(flag == 2){
    delta = (Netwk.indegree[edR[0]]-tm[JMEAN])*(Netwk.indegree[edR[3]]-tm[KMEAN]) + \
	    (Netwk.indegree[edR[2]]-tm[JMEAN])*(Netwk.indegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.indegree[edR[0]]-tm[JMEAN])*(Netwk.indegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.indegree[edR[2]]-tm[JMEAN])*(Netwk.indegree[edR[3]]-tm[KMEAN]);
  }
  // Out-Out
  else if(flag == 3){
    delta = (Netwk.outdegree[edR[0]]-tm[JMEAN])*(Netwk.outdegree[edR[3]]-tm[KMEAN]) + \
	    (Netwk.outdegree[edR[2]]-tm[JMEAN])*(Netwk.outdegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outdegree[edR[0]]-tm[JMEAN])*(Netwk.outdegree[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outdegree[edR[2]]-tm[JMEAN])*(Netwk.outdegree[edR[3]]-tm[KMEAN]);
  }
  else{
    printf("Choose a valid configuration\n");
  }
  Delta = delta;//ft*delta;
  
  return Delta;
}

//////////////////////////////////////////////////////////
// ADECUATE FORM of DeltaR for 2swap
//////////////////////////////////////////////////////////
int DeltaR_int(int edR[4],int flag){
  
  double delta,Delta,ft;
  int a,b,c,d;
  
  a = edR[0]; c = edR[2]; 
  b = edR[1]; d = edR[3]; 
  
  // In-Out
  if(flag == 0){
    delta = Netwk.indegree[a]*Netwk.outdegree[d] + Netwk.indegree[c]*Netwk.outdegree[b] - \
	    Netwk.indegree[a]*Netwk.outdegree[b] - Netwk.indegree[c]*Netwk.outdegree[d];
  }
  // Out-In
  else if(flag == 1){
    delta = Netwk.outdegree[a]*Netwk.indegree[d] + Netwk.outdegree[c]*Netwk.indegree[b] - \
	    Netwk.outdegree[a]*Netwk.indegree[b] - Netwk.outdegree[c]*Netwk.indegree[d];
  }
  // In-In
  else if(flag == 2){
    delta = Netwk.indegree[a]*Netwk.indegree[d] + Netwk.indegree[c]*Netwk.indegree[b] - \
	    Netwk.indegree[a]*Netwk.indegree[b] - Netwk.indegree[c]*Netwk.indegree[d];
  }
  // Out-Out
  else if(flag == 3){
    delta = Netwk.outdegree[a]*Netwk.outdegree[d] + Netwk.outdegree[c]*Netwk.outdegree[b] - \
	    Netwk.outdegree[a]*Netwk.outdegree[b] - Netwk.outdegree[c]*Netwk.outdegree[d];
  }
  else{
    printf("Choose a valid configuration\n");
  }
  Delta = delta;//ft*delta;
  return Delta;
}

//////////////////////////////////////////////////////////
// ADECUATE FORM of DeltaR ranked for 2swap
//////////////////////////////////////////////////////////
int DeltaR_ranked_Int(double *tm,int edR[4],int flag){
  
  double delta,Delta;
  double ft;
  int a,b,c,d;

//   ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  
  // Labeling nodes
  a = edR[0]; c = edR[2]; 
  b = edR[1]; d = edR[3]; 
  
  // In-Out
  if(flag == 0){
    delta = Netwk.inrnk[a]*Netwk.outrnk[d] + Netwk.inrnk[c]*Netwk.outrnk[b] - \
	    Netwk.inrnk[a]*Netwk.outrnk[b] - Netwk.inrnk[c]*Netwk.outrnk[d];
  }
  // Out-In
  else if(flag == 1){
    delta = Netwk.outrnk[a]*Netwk.inrnk[d] + Netwk.outrnk[c]*Netwk.inrnk[b] - \
	    Netwk.outrnk[a]*Netwk.inrnk[b] - Netwk.outrnk[c]*Netwk.inrnk[d];
  }
  // In-In
  else if(flag == 2){
    delta = Netwk.inrnk[a]*Netwk.inrnk[d] + Netwk.inrnk[c]*Netwk.inrnk[b] - \
	    Netwk.inrnk[a]*Netwk.inrnk[b] - Netwk.inrnk[c]*Netwk.inrnk[d];
  }
  // Out-Out
  else if(flag == 3){
    delta = Netwk.outrnk[a]*Netwk.outrnk[d] + Netwk.outrnk[c]*Netwk.outrnk[b] - \
	    Netwk.outrnk[a]*Netwk.outrnk[b] - Netwk.outrnk[c]*Netwk.outrnk[d];
  }
  else{
    printf("Choose a valid configuration\n");
  }
  Delta = delta;
  
  return Delta;
}


double DeltaR_ranked(double *tm,int edR[4],int flag){
  
  double delta,Delta;
  double ft;

  ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  
  // In-Out
  if(flag == 0){
    delta = (Netwk.inrnk[edR[0]]-tm[JMEAN])*(Netwk.outrnk[edR[3]]-tm[KMEAN]) + \
	    (Netwk.inrnk[edR[2]]-tm[JMEAN])*(Netwk.outrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.inrnk[edR[0]]-tm[JMEAN])*(Netwk.outrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.inrnk[edR[2]]-tm[JMEAN])*(Netwk.outrnk[edR[3]]-tm[KMEAN]);
  }
  // Out-In
  else if(flag == 1){
    delta = (Netwk.outrnk[edR[0]]-tm[JMEAN])*(Netwk.inrnk[edR[3]]-tm[KMEAN]) + \
	    (Netwk.outrnk[edR[2]]-tm[JMEAN])*(Netwk.inrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outrnk[edR[0]]-tm[JMEAN])*(Netwk.inrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outrnk[edR[2]]-tm[JMEAN])*(Netwk.inrnk[edR[3]]-tm[KMEAN]);
  }
  // In-In
  else if(flag == 2){
    delta = (Netwk.inrnk[edR[0]]-tm[JMEAN])*(Netwk.inrnk[edR[3]]-tm[KMEAN]) + \
	    (Netwk.inrnk[edR[2]]-tm[JMEAN])*(Netwk.inrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.inrnk[edR[0]]-tm[JMEAN])*(Netwk.inrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.inrnk[edR[2]]-tm[JMEAN])*(Netwk.inrnk[edR[3]]-tm[KMEAN]);
  }
  // Out-Out
  else if(flag == 3){
    delta = (Netwk.outrnk[edR[0]]-tm[JMEAN])*(Netwk.outrnk[edR[3]]-tm[KMEAN]) + \
	    (Netwk.outrnk[edR[2]]-tm[JMEAN])*(Netwk.outrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outrnk[edR[0]]-tm[JMEAN])*(Netwk.outrnk[edR[1]]-tm[KMEAN]) - \
	    (Netwk.outrnk[edR[2]]-tm[JMEAN])*(Netwk.outrnk[edR[3]]-tm[KMEAN]);
  }
  else{
    printf("Choose a valid configuration\n");
  }
  Delta = ft*delta;
  
  return Delta;
}



void ThreeSwap(int *AIJ, int *Ed[NROWS],int EdgesR[3][3], int *flag){
  
  int n1,n2,n3;
  int a,b,c,d,e,f;
  int bp,dp,fp;
  float nrandom;
  
  // Choosing 3 stubs
  n1 = gsl_rng_uniform_int(rand_generator,Netwk.Nedges);
  n2 = gsl_rng_uniform_int(rand_generator,Netwk.Nedges);
  n3 = gsl_rng_uniform_int(rand_generator,Netwk.Nedges);
  
  a = Ed[OUT][n1];   b = Ed[IN][n1];
  c = Ed[OUT][n2];   d = Ed[IN][n2];
  e = Ed[OUT][n3];   f = Ed[IN][n3];
  
#ifdef THREETWOSWAP
  int targets[3];
  targets[0] = b; targets[1] = d; targets[2] = f;
  gsl_ran_shuffle(rand_generator,targets,3,sizeof(int));
  bp = targets[0]; dp = targets[1]; fp = targets[2];
#else
  // Verifying if there is repeated target-nodes
  if( b == d or b == f or d == f ){
    *flag = 0;
    return;
  }
  
  // Assigning b', d', f'
  nrandom = gsl_rng_uniform(rand_generator);
  if(nrandom < 0.5){
    bp = d; dp = f; fp = b;
  }else{
    bp = f; dp = b; fp = d;
  }
#endif

  // Avoiding autoloops
  if( a == bp or c == dp or e == fp){
    *flag = 0;
    return;
  }
  else
  {
    // Verifiying existing connections
    if( not ( AIJ[a*Nc + bp] or AIJ[c*Nc + dp] or AIJ[e*Nc + fp]))
    {
      AIJ[a*Nc + bp] = 1;   AIJ[a*Nc + b] = 0;
      AIJ[c*Nc + dp] = 1;   AIJ[c*Nc + d] = 0;
      AIJ[e*Nc + fp] = 1;   AIJ[e*Nc + f] = 0;
      
      // Save nodes rewired
      EdgesR[SOURCE][0] = a;     EdgesR[SOURCE][1] = c;    EdgesR[SOURCE][2] = e;
      EdgesR[TARGET][0] = b;     EdgesR[TARGET][1] = d;    EdgesR[TARGET][2] = f;
      EdgesR[NTARGET][0] = bp;   EdgesR[NTARGET][1] = dp;  EdgesR[NTARGET][2] = fp;
      *flag = 1;
    }
    else{ 
      *flag = 0;
    }
  }
  
}

void prueba(int i){
  if(i>3){
    printf("i>3...\n");
    return; 
  }
  printf("por fuera del if en funcion prueba\n");
}

//////////////////////////////////////////////////////////
// ADECUATE FORM of Dr for 3swap
//////////////////////////////////////////////////////////
int DeltaR3swap_Int(double *tm,int edR[3][3],int flag){
  int delta,Delta;
  int a,b,c,d,e,f,bp,dp,fp;
  double ft;

//   ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  a = edR[SOURCE][0]; c = edR[SOURCE][1]; e = edR[SOURCE][2]; 
  b = edR[TARGET][0]; d = edR[TARGET][1]; f = edR[TARGET][2]; 
  bp = edR[NTARGET][0]; dp = edR[NTARGET][1]; fp = edR[NTARGET][2]; 
  
  // In-Out
  if(flag == 0){
    delta = Netwk.indegree[a]*Netwk.outdegree[bp] + Netwk.indegree[c]*Netwk.outdegree[dp] + Netwk.indegree[e]*Netwk.outdegree[fp] - \
	    Netwk.indegree[a]*Netwk.outdegree[b] - Netwk.indegree[c]*Netwk.outdegree[d] - Netwk.indegree[e]*Netwk.outdegree[f];
  }
  // Out-In
  else if(flag == 1){
    delta = Netwk.outdegree[a]*Netwk.indegree[bp] + Netwk.outdegree[c]*Netwk.indegree[dp] + Netwk.outdegree[e]*Netwk.indegree[fp] - \
	    Netwk.outdegree[a]*Netwk.indegree[b] - Netwk.outdegree[c]*Netwk.indegree[d] - Netwk.outdegree[e]*Netwk.indegree[f];
  }
  // In-In
  else if(flag == 2){
    delta = Netwk.indegree[a]*Netwk.indegree[bp] + Netwk.indegree[c]*Netwk.indegree[dp] + Netwk.indegree[e]*Netwk.indegree[fp] - \
	    Netwk.indegree[a]*Netwk.indegree[b] -  Netwk.indegree[c]*Netwk.indegree[d] -  Netwk.indegree[e]*Netwk.indegree[f];
  }
  // Out-Out
  else if(flag == 3){
    delta = Netwk.outdegree[a]*Netwk.outdegree[bp] + Netwk.outdegree[c]*Netwk.outdegree[dp] + Netwk.outdegree[e]*Netwk.outdegree[fp] - \
	    Netwk.outdegree[a]*Netwk.outdegree[b] -  Netwk.outdegree[c]*Netwk.outdegree[d] -  Netwk.outdegree[e]*Netwk.outdegree[f];
  }
  else{
    printf("Choose a valid configuration\n");
  }
//   Delta = delta;
  
  return delta;
}

double DeltaR3swap(double *tm,int edR[3][3],int flag){
  double delta,Delta;
  int a,b,c,d,e,f,bp,dp,fp;
  double ft;

  ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  a = edR[SOURCE][0]; c = edR[SOURCE][1]; e = edR[SOURCE][2]; 
  b = edR[TARGET][0]; d = edR[TARGET][1]; f = edR[TARGET][2]; 
  bp = edR[NTARGET][0]; dp = edR[NTARGET][1]; fp = edR[NTARGET][2]; 
  
  // In-Out
  if(flag == 0){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.indegree[a]-tm[JMEAN])*(Netwk.outdegree[bp]-tm[KMEAN]) + \
	    (Netwk.indegree[c]-tm[JMEAN])*(Netwk.outdegree[dp]-tm[KMEAN]) + \
	    (Netwk.indegree[e]-tm[JMEAN])*(Netwk.outdegree[fp]-tm[KMEAN])- \
	    (Netwk.indegree[a]-tm[JMEAN])*(Netwk.outdegree[b]-tm[KMEAN]) - \
	    (Netwk.indegree[c]-tm[JMEAN])*(Netwk.outdegree[d]-tm[KMEAN]) - \
	    (Netwk.indegree[e]-tm[JMEAN])*(Netwk.outdegree[f]-tm[KMEAN]);
  }
  // Out-In
  else if(flag == 1){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.outdegree[a]-tm[JMEAN])*(Netwk.indegree[bp]-tm[KMEAN]) + \
	    (Netwk.outdegree[c]-tm[JMEAN])*(Netwk.indegree[dp]-tm[KMEAN]) + \
	    (Netwk.outdegree[e]-tm[JMEAN])*(Netwk.indegree[fp]-tm[KMEAN])- \
	    (Netwk.outdegree[a]-tm[JMEAN])*(Netwk.indegree[b]-tm[KMEAN]) - \
	    (Netwk.outdegree[c]-tm[JMEAN])*(Netwk.indegree[d]-tm[KMEAN]) - \
	    (Netwk.outdegree[e]-tm[JMEAN])*(Netwk.indegree[f]-tm[KMEAN]);
  }
  // In-In
  else if(flag == 2){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.indegree[a]-tm[JMEAN])*(Netwk.indegree[bp]-tm[KMEAN]) + \
	    (Netwk.indegree[c]-tm[JMEAN])*(Netwk.indegree[dp]-tm[KMEAN]) + \
	    (Netwk.indegree[e]-tm[JMEAN])*(Netwk.indegree[fp]-tm[KMEAN])- \
	    (Netwk.indegree[a]-tm[JMEAN])*(Netwk.indegree[b]-tm[KMEAN]) - \
	    (Netwk.indegree[c]-tm[JMEAN])*(Netwk.indegree[d]-tm[KMEAN]) - \
	    (Netwk.indegree[e]-tm[JMEAN])*(Netwk.indegree[f]-tm[KMEAN]);
  }
  // Out-Out
  else if(flag == 3){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.outdegree[a]-tm[JMEAN])*(Netwk.outdegree[bp]-tm[KMEAN]) + \
	    (Netwk.outdegree[c]-tm[JMEAN])*(Netwk.outdegree[dp]-tm[KMEAN]) + \
	    (Netwk.outdegree[e]-tm[JMEAN])*(Netwk.outdegree[fp]-tm[KMEAN])- \
	    (Netwk.outdegree[a]-tm[JMEAN])*(Netwk.outdegree[b]-tm[KMEAN]) - \
	    (Netwk.outdegree[c]-tm[JMEAN])*(Netwk.outdegree[d]-tm[KMEAN]) - \
	    (Netwk.outdegree[e]-tm[JMEAN])*(Netwk.outdegree[f]-tm[KMEAN]);
  }
  else{
    printf("Choose a valid configuration\n");
  }
  Delta = ft*delta;
  
  return Delta;
}

//////////////////////////////////////////////////////////
// ADECUATE FORM of Dr for 3swap ranked (in construction)
//////////////////////////////////////////////////////////
double DeltaR3swap_ranked_Int(double *tm,int edR[3][3],int flag){
  double delta,Delta;
  int a,b,c,d,e,f,bp,dp,fp;
  double ft;
//   ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  a = edR[SOURCE][0]; c = edR[SOURCE][1]; e = edR[SOURCE][2]; 
  b = edR[TARGET][0]; d = edR[TARGET][1]; f = edR[TARGET][2]; 
  bp = edR[NTARGET][0]; dp = edR[NTARGET][1]; fp = edR[NTARGET][2]; 
  
  // In-Out
  if(flag == 0){
    delta = Netwk.inrnk[a]*Netwk.outrnk[bp] + Netwk.inrnk[c]*Netwk.outrnk[dp] + Netwk.inrnk[e]*Netwk.outrnk[fp] - \
	    Netwk.inrnk[a]*Netwk.outrnk[b] - Netwk.inrnk[c]*Netwk.outrnk[d] - Netwk.inrnk[e]*Netwk.outrnk[f];
  }
  // Out-In
  else if(flag == 1){
    delta = Netwk.outrnk[a]*Netwk.inrnk[bp] + Netwk.outrnk[c]*Netwk.inrnk[dp] + Netwk.outrnk[e]*Netwk.inrnk[fp] - \
	    Netwk.outrnk[a]*Netwk.inrnk[b] - Netwk.outrnk[c]*Netwk.inrnk[d] - Netwk.outrnk[e]*Netwk.inrnk[f];
  }
  // In-In
  else if(flag == 2){
    delta = Netwk.inrnk[a]*Netwk.inrnk[bp] + Netwk.inrnk[c]*Netwk.inrnk[dp] + Netwk.inrnk[e]*Netwk.inrnk[fp]- \
	    Netwk.inrnk[a]*Netwk.inrnk[b] - Netwk.inrnk[c]*Netwk.inrnk[d] - Netwk.inrnk[e]*Netwk.inrnk[f];
  }
  // Out-Out
  else if(flag == 3){
    delta = Netwk.outrnk[a]*Netwk.outrnk[bp] + Netwk.outrnk[c]*Netwk.outrnk[dp] + Netwk.outrnk[e]*Netwk.outrnk[fp] - \
	    Netwk.outrnk[a]*Netwk.outrnk[b] - Netwk.outrnk[c]*Netwk.outrnk[d] - Netwk.outrnk[e]*Netwk.outrnk[f];
  }
  else{
    printf("Choose a valid configuration\n");
  }
  
  return delta;
}



double DeltaR3swap_ranked(double *tm,int edR[3][3],int flag){
  double delta,Delta;
  int a,b,c,d,e,f,bp,dp,fp;
  double ft;

  ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
  a = edR[SOURCE][0]; c = edR[SOURCE][1]; e = edR[SOURCE][2]; 
  b = edR[TARGET][0]; d = edR[TARGET][1]; f = edR[TARGET][2]; 
  bp = edR[NTARGET][0]; dp = edR[NTARGET][1]; fp = edR[NTARGET][2]; 
  
  // In-Out
  if(flag == 0){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.inrnk[a]-tm[JMEAN])*(Netwk.outrnk[bp]-tm[KMEAN]) + \
	    (Netwk.inrnk[c]-tm[JMEAN])*(Netwk.outrnk[dp]-tm[KMEAN]) + \
	    (Netwk.inrnk[e]-tm[JMEAN])*(Netwk.outrnk[fp]-tm[KMEAN])- \
	    (Netwk.inrnk[a]-tm[JMEAN])*(Netwk.outrnk[b]-tm[KMEAN]) - \
	    (Netwk.inrnk[c]-tm[JMEAN])*(Netwk.outrnk[d]-tm[KMEAN]) - \
	    (Netwk.inrnk[e]-tm[JMEAN])*(Netwk.outrnk[f]-tm[KMEAN]);
  }
  // Out-In
  else if(flag == 1){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.outrnk[a]-tm[JMEAN])*(Netwk.inrnk[bp]-tm[KMEAN]) + \
	    (Netwk.outrnk[c]-tm[JMEAN])*(Netwk.inrnk[dp]-tm[KMEAN]) + \
	    (Netwk.outrnk[e]-tm[JMEAN])*(Netwk.inrnk[fp]-tm[KMEAN])- \
	    (Netwk.outrnk[a]-tm[JMEAN])*(Netwk.inrnk[b]-tm[KMEAN]) - \
	    (Netwk.outrnk[c]-tm[JMEAN])*(Netwk.inrnk[d]-tm[KMEAN]) - \
	    (Netwk.outrnk[e]-tm[JMEAN])*(Netwk.inrnk[f]-tm[KMEAN]);
  }
  // In-In
  else if(flag == 2){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.inrnk[a]-tm[JMEAN])*(Netwk.inrnk[bp]-tm[KMEAN]) + \
	    (Netwk.inrnk[c]-tm[JMEAN])*(Netwk.inrnk[dp]-tm[KMEAN]) + \
	    (Netwk.inrnk[e]-tm[JMEAN])*(Netwk.inrnk[fp]-tm[KMEAN])- \
	    (Netwk.inrnk[a]-tm[JMEAN])*(Netwk.inrnk[b]-tm[KMEAN]) - \
	    (Netwk.inrnk[c]-tm[JMEAN])*(Netwk.inrnk[d]-tm[KMEAN]) - \
	    (Netwk.inrnk[e]-tm[JMEAN])*(Netwk.inrnk[f]-tm[KMEAN]);
  }
  // Out-Out
  else if(flag == 3){
    ft = 1.0/(Netwk.Nedges*tm[SIGMJ]*tm[SIGMK]);
    delta = (Netwk.outrnk[a]-tm[JMEAN])*(Netwk.outrnk[bp]-tm[KMEAN]) + \
	    (Netwk.outrnk[c]-tm[JMEAN])*(Netwk.outrnk[dp]-tm[KMEAN]) + \
	    (Netwk.outrnk[e]-tm[JMEAN])*(Netwk.outrnk[fp]-tm[KMEAN])- \
	    (Netwk.outrnk[a]-tm[JMEAN])*(Netwk.outrnk[b]-tm[KMEAN]) - \
	    (Netwk.outrnk[c]-tm[JMEAN])*(Netwk.outrnk[d]-tm[KMEAN]) - \
	    (Netwk.outrnk[e]-tm[JMEAN])*(Netwk.outrnk[f]-tm[KMEAN]);
  }
  else{
    printf("Choose a valid configuration\n");
  }
  Delta = ft*delta;
  
  return Delta;
}

//===========================
// DYNAMIC
//===========================
// double FiringRate_nonstochastic(double nuold, double h0, double J, int *AIJ){
//   
// }
