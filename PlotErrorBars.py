#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import scipy.interpolate as itp
import networkx as nx
from mymodules import *

def bootstrap(data, num_samples, statistic, alpha):
  """Returns bootstrap estimate of 100.0*(1-alpha) CI for statistic."""
  n = len(data)
  idx = np.random.randint(0, n, (num_samples, n))
  samples = data[idx]
  stat = np.sort(statistic(samples, 1))
  percent1 = np.percentile(stat,100-alpha)
  percent2 = np.percentile(stat,alpha)
  return percent1,percent2,np.mean(stat)

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv
arr = np.array


#===================================
# INITIAL PARAMETERS
#===================================
DirProfile = Argv[1]
DirOuput = Argv[2]
Nfiles = int(get("ls %sAssort* | wc -l"%DirProfile))
name = DirProfile.split("/")[-2]
Nblocks = 20
Nboostraping = 10000
ys = np.zeros(Nfiles)
idx= 4
Rho = dict()
StatMean = dict()
StatStd = dict()
StatErrBars = dict()
yb2 = dict()
xb2 = dict()

Header =["""=============================================================
     XVALUE      YMEAN        YERROR-MIN         YERROR-MAX 
============================================================="""]



#===================================
#READING FILES
#===================================
Nrew = 20e6
idxb = int(Nrew/Nblocks)
xevaluate = np.arange(0,Nrew,idxb)
for nl in xrange(Nfiles):
  # Reading files
  namefile = DirProfile + "Assort" + name + "_V%d.txt"%nl
  Rho['trial%d'%nl] = np.loadtxt(namefile)

#==========================================
# INTERPOLATING CURVES FOR EACH COMPONENT
#==========================================
modes = ['In-out','Out-In','In-In','Out-Out']
for mode in [0,1,2,3]:
  
  # Initializing variables
  msk = np.ones(Nblocks,dtype='bool')
  yb = dict()
  xb = dict()
  
  # ofr for each trial curve
  for nl in xrange(Nfiles):
    # Interpolate data of trial-nl
    curve_spline = itp.splrep(Rho['trial%d'%nl][:,0],Rho['trial%d'%nl][:,mode+1])
    
    #-------------------------------------------------------------------------------------
    # Set xevaluate less than the last value of rewiring:
    # Each curve don't have the same number of points and in some cases the 
    # maximum value is different.Therefore it is necesary choose an equal number
    # of points
    #-------------------------------------------------------------------------------------
    # maximum value of rewiring (Local case)
    msk_xval = xevaluate < Rho['trial%d'%nl][-1,0] 
    # Update the number of blocks to consider in the problem (Global case)
    msk = msk*msk_xval 
    
    # Evaluating interpolation function in xevaluate values
    xb['nl%d'%nl] = msk_xval
    yb['nl%d'%nl] = itp.splev(xevaluate[msk_xval],curve_spline)
    
    if mode == 3:
      curve_spline2 = itp.splrep(Rho['trial%d'%nl][:,0],Rho['trial%d'%nl][:,mode+1])
      xb2['nl%d'%nl] = msk_xval
      yb2['nl%d'%nl] = itp.splev(xevaluate[msk_xval],curve_spline)
  
  #===================================
  #BOOSTRAPPING
  #===================================
  ybs = [[yb.values()[i][j] for i in xrange(Nfiles)] for j in xrange(1,msk.sum())]
  yerr = np.zeros((len(ybs),2))
  ymean = np.zeros(len(ybs))
  Stderr = np.zeros((len(ybs),2))
  Stdmean = np.zeros(len(ybs))
  
  for nb in xrange(len(ybs)):
    yerr[nb,0],yerr[nb,1],ymean[nb]       =  bootstrap(arr(ybs[nb]),Nboostraping,np.mean,95)
    Stderr[nb,0],Stderr[nb,1],Stdmean[nb] =  bootstrap(arr(ybs[nb]),Nboostraping,np.std,95)
 
  StatMean[modes[mode]] = np.array(zip(xevaluate[msk][1:],ymean, yerr[:,0],yerr[:,1]))
  StatStd[modes[mode]] = np.array(zip(xevaluate[msk][1:],Stdmean, Stderr[:,0],Stderr[:,1]))
  StatErrBars[modes[mode]] = np.array( zip( xevaluate[msk][1:], ymean, ymean - 3*Stdmean, ymean + 3*Stdmean) )
  np.savetxt(DirOuput + "ErrorBarsMean_mode%d.txt"%mode,StatMean[modes[mode]],header=Header[0])
  np.savetxt(DirOuput + "ErrorBarsStd_mode%d.txt"%mode,StatStd[modes[mode]],header=Header[0])
  np.savetxt(DirOuput + "ErrorBars_mode%d.txt"%mode,StatErrBars[modes[mode]],header=Header[0])
  
  

#===================================
# SAVING VALUES
#===================================
