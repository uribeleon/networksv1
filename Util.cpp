#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Allvars.h"
#include "Prototypes.h"


int exec(char *cmd){
	
	FILE *In;
	
	In = popen(cmd,"r");
	
	if (!In) return (1);
	fgets(temp,256,In);
	
	pclose(In);
	return atoi(temp);
}

int Sum(int *vec, int dim){
	int i;
	int sum = 0.0;
	for(i=0;i<dim;i++){
		sum = sum + vec[i];
	}
	return sum;
}
/*
void CumSum(int *AIJ, int flag=0)
{
  
}*/

void ordinal_assign(void){
	int i,j,uplimit[2],lowlimit[2];
	int k=0,kpos=0;
	lowlimit[0] = lowlimit[1] = 0;	
	for(i=0;i<Netwk.Ndegree;i++){

		// Out - Edges
		uplimit[0] = lowlimit[0] + Netwk.outdegree[i];
		Netwk.pos[i] = kpos;
		for(j=lowlimit[0];j<uplimit[0];j++){
			//printf("j: %d   i: %d\n",j,i);

			Netwk.edges[0][j] = i;
			kpos++;
		}
		lowlimit[0] = uplimit[0];

		// In - Edges
		uplimit[1] = lowlimit[1] + Netwk.indegree[i];
		for(j=lowlimit[1];j<uplimit[1];j++){
			Netwk.edges[1][j] = i;	
		}
		lowlimit[1] = uplimit[1];
	}
	printf("***  Edges vector created with exit  ***\n");
// 	printf("Edges:\n");
// 	for(i=0;i<Netwk.Nedges;i++){
// 	  printf("%d %d\n",Netwk.edges[0][i],Netwk.edges[1][i]);
// 	}
}

void InitMatrixCij(void){
	int i,j;
	for(i=0;i<Netwk.Nnodes;i++){
		for(j=0;j<Netwk.Nnodes;j++){
			Netwk.CIJ[i*Nc + j] = 0;
			// printf("Asigning i:%d j:%d C[%d,%d]=%d\n",i,j,i,j,Net.CIJ[i*Nc+j] );
		}
	}
	printf("***  Matrix was successfully initialized  ***\n");
}

void Todump(char filename[]){
	char cmd[100];
	sprintf(cmd,"grep -v \"\%\" %s | grep -v \"^$\" | awk -F\"=\" '{print $2}' > %s.dump",filename,&filename[7]);
	printf("cmd: %s\n",cmd);
	system(cmd);
}

int ReadParameters(char filename[]){
	char cmd[100],filedump[100];
	int i=0;
	FILE *file;
	printf("Reading files...\n");
	file=fopen(filename,"r");
// 	printf("Reading files...\n");
	if(file==NULL){
		printf("File %s not exist\n",filename);
		return(1);
	}
	fclose(file);

	printf("Exec Todump...\n");
	Todump(filename);
	printf("Todump executed...\n");
	sprintf(filedump,"%s.dump",&filename[7]);
	file = fopen(filedump,"r");

	while(getc(file) != EOF){
		fscanf(file,"%d",&prm[i]);
		i++;
	}
	
	fclose(file);
	
	printf("***  The file '%s' has been loaded  ***\n",filename);
	
	sprintf(cmd,"rm -rf %s.dump",&filename[7]);
	system(cmd);
	return(0);
}

void PrintResults(char mode[]){
  int Ndeleted;
  if(strcmp(mode,"basic")==0){
    Ndeleted = Nloops_deleted+Nrepeated_deleted;
    printf("\n===========================================================\n");
    printf("Basic information:\n\n"); 
    printf("Number of nodes:                       %d\n",Netwk.Nnodes);
    printf("Number of initial edges:               %d\n",Netwk.Nedges);
    printf("Number of loops deleted:               %d\n",Nloops_deleted);
    printf("Number of repeated edges deleted:      %d\n",Nrepeated_deleted);
    printf("Number of edges deleted:               %d\n",Ndeleted);
    printf("Number of final edges:                 %d\n",Netwk.Nedges-Ndeleted);
    printf("Percent of edges deleted:              %.2f\%\n",Ndeleted*100.0/Netwk.Nedges);
    printf("===========================================================\n");
  }
  
}

void Transpose(int *A[], int *B[], int dim){
  
  int i,j;
  
  for(i=0;i<dim;i++){
    for(j=0;j<NROWS;j++){
      B[i][j] = A[j][i];
    }
  }
}

void CopyEdges(int *A[NROWS], int *B[NROWS], int dim){
  int i,j;
  
  for(j=0;j<dim;j++){
    for(i=0;i<NROWS;i++){
      B[i][j] = A[i][j];
    }
  }
}


void CopyMatrix(int *A, int *B, int dim){
  int i,j;
  
  for(i=0;i<dim;i++){
    for(j=0;j<dim;j++){
      B[i*dim + j] = A[i*dim + j];
    }
  }
}

bool IsEqual(int *A, int *B,int dim){
  int i,j;
  int counter=0;
  for(i=0;i<dim;i++){
    for(j=0;j<dim;j++){
      if(A[i*dim+j] == B[i*dim+j])
	counter++;
    }
  }
  if(counter==dim*dim){
    printf("Matrix are Equals\n"); FFLUSH
    return 1;
  }
  else{
    printf("Matrix are not Equals\n"); FFLUSH
    return 0;
  }
}

void StateOfProcess(int ActualState, int total, char profile[]){
  
  float percentage;
  
  percentage = (1.0*ActualState)/total*100;
  printf(" Actual State -> Training profile %14s -- %5.2f \%\n",profile,percentage);
  
}

void GreatersArray(int idx_value, int V[], int size, int Vout[])
{
    int i;
    for(i=0;i<size;i++)
    {
        Vout[i] = V[i]>V[idx_value];
    }
    
}

void EqualsArray(int idx_value, int V[], int size, int Vout[])
{
    int i;
    for(i=0;i<size;i++)
    {
        Vout[i] = (V[i]==V[idx_value]);
    }
}

float sum_elmts_vector(int a1[], int size)
{
    int i;
    float sum=0;
    for(i=0;i<size;i++)
        sum = sum + a1[i];
    return sum;
}

void get_averagerank(int V[], int size, double Rnk_Vec[])
{
  int i;
  int vt_greater[size];
  int vt_equal[size];
  float t1,t2;
  
  for(i=0;i<size;i++)
  {
    GreatersArray(i,V,size,vt_greater);
    EqualsArray(i,V,size,vt_equal);
    t1 = sum_elmts_vector(vt_greater,size);
    t2 = sum_elmts_vector(vt_equal,size);
    Rnk_Vec[i] = t1 + 0.5*(1 + t2);
  }
}


template <typename T> int sgn(T val) {
    return (0 < val) - (val < 0);
}

void split_condition(char *StrIni, const char *delimiter, int condition[]){
  char *word;
  int i=0;

  word = strtok(StrIni,delimiter);
  while(word!=NULL){
    condition[i] = atoi(word);
    word = strtok(NULL,delimiter);
    i++;
  }
}

bool evaluate_condition(int delta[4], int cond[4]){
  int sgn_delta[4];
  int i;
  int sum=0;
  for(i=0;i<4;i++){
    sum = sum + fabs( cond[i] - fabs(cond[i])*sgn(delta[i]) );
  }
  
  return !bool(sum);
}

bool evaluate_condition_regulated(double delta[4], int inicond[4], int cond[4], int idx_reg[4]){
  int sgn_delta[4];
  int i;
  int sum=0;
  for(i=0;i<4;i++){
    sum = sum + fabs( cond[i] - fabs(cond[i])*sgn(delta[i]) );
  }
  
  if(!bool(sum)){
    for(i=0;i<4;i++){
      cond[i] = inicond[i]-idx_reg[i]*sgn(delta[i]);
    }
  }
  return !bool(sum);

}

void check_condition(double rho[4], int inicond[4], int cond[4], int idx_reg[4] ){
  int i;
  for(i=0;i<4;i++){
    cond[i] = inicond[i] - idx_reg[i]*sgn(rho[i]);
  }
}

int EvalCondition2(double Delta[4],char *newcond){
  int out;
  int i;
  char str[20];
  char cnd[100] = "Delta[0]<0";
  
  for(i=1;i<4;i++){
    if(Delta[i]>0){
      sprintf(str," and Delta[%d]<0",i);
      strcat(cnd,str);    
    }
    else{
      sprintf(str," and Delta[%d]>0",i);
      strcat(cnd,str);    
    }
  }
  /*
  if(Delta[2]>0){
    strcat(cnd," and Delta[2]<0");    
  }
  else{
    strcat(cnd," and Delta[2]>0");    
  }
  
  if(Delta[3]>0){
    strcat(cnd," and Delta[3]<0");    
  }
  else{
    strcat(cnd," and Delta[3]>0");    
  }
  */
  strcpy(newcond,cnd);
  printf("function: %s\n",cnd);
  
  return 0;
}

int EvalCondition3(double R0[4],double RF[4],char *newcond){
  
  int i;
  int out;
  char str[20];
  char cnd[100] = "Delta[0]<0";
  
  for(i=1;i<4;i++){
    if( RF[i] > R0[i] + 0.01*fabs(R0[i]) ){
      sprintf(str," and Delta[%d]<0",i);
      strcat(cnd,str);    
    }
    if(RF[i] < R0[i] - 0.01*fabs(R0[i])){
      sprintf(str," and Delta[%d]>0",i);
      strcat(cnd,str);    
    }
  }
  /*
  if(Delta[2]>0){
    strcat(cnd," and Delta[2]<0");    
  }
  else{
    strcat(cnd," and Delta[2]>0");    
  }
  
  if(Delta[3]>0){
    strcat(cnd," and Delta[3]<0");    
  }
  else{
    strcat(cnd," and Delta[3]>0");    
  }
  */
  strcpy(newcond,cnd);
//   printf("function: %s\n",cnd);
  
  return 0;
}