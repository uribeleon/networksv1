#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define NF 4

int main(int argc, char *argv[]){

 int i;
 char buff[50];
 FILE *of[NF];

 for(i=0;i<NF;i++){
   sprintf(buff,"archivo%d.txt",i);
   of[i] = fopen(buff,"w");
   fprintf(of[i],"escribo esta linea en el archivo %d\n",i);
   fclose(of[i]);
 }

 return 0;
}
