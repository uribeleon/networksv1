from mymodules import *

Ai = np.loadtxt("./data/MatrixInitial_500.txt")
NR = 300
r = np.zeros(4)
rpearson = np.array([]).reshape(0,4)

#Initial assortativity
for mode in xrange(4):
  r[mode] = Assortativity_Foster(Ai,mode)

rpearson = np.append(rpearson,[r],axis=0)


