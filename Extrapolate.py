import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit

def Func(x,a,b,c,d):
    return (a*x+b)/(c*x+d)

def Sigm(x,a,b,c,d,e):
	return a/(b + np.exp( -c*(x - d) ) ) + e

rw = np.array([])
r0 = np.array([])
nl = []
# for i in xrange(2,3):
#     r,prf = np.loadtxt("./Net100N/Assort_regulated_r0111_R4e6_v%d.txt"%i,usecols=[0,1],unpack=True)
#     rw = np.append(rw,r+(i-1)*4e6)
#     r0 = np.append(r0,prf)
#     nl.append(len(r))

rw,r0 = np.loadtxt("./trash/Net100N/Assort_regulated_r0111_R4e6_v2.txt",usecols=[0,1],unpack=True)
#rw,r0 = np.loadtxt("./Prof3swapN100/_1_0_0_0/Assort_1_0_0_0.txt",usecols=[0,1],unpack=True)

pini = np.random.rand(5)
pval,covar = curve_fit( Sigm, rw, r0, p0 = pini) 

plt.plot(rw,r0,'--',color="blue",lw=2)
plt.grid()

A,B,C,D,E = pval
xs = np.linspace(0,20e6,10000)
plt.plot(xs,Sigm(xs,A,B,C,D,E),'r-')
print A,B,C,D,E

plt.show()
