#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include "Allvars.h"
#include "Prototypes.h"

int main(int argc, char *argv[])
{
  double *J,*ho,*raster,*nu,*MeanFR;
  double jmin,jmax,jsize,jdelta;
  double pc,dT,dJ;
  
  
  char *filename,*fileconf;
  
  int Ntimes;
  int i,j,k;
  
  FILE *ofile;
  
  
  //===================================================================================
  // Initialiting variables
  //===================================================================================
  
  filename = argv[1]; // File with initial matrix
  fileconf = argv[2];
  
  // Allocating general variables
  allocate_variables(filename);
  
  // Reading adjacency matrix
  ReadMatrix(filename);
  
  // Get degrees of each node 
  GetDegreeNodes(Netwk.CIJ);

  // Make copy of Adjacency Matrix
  CopyMatrix(Netwk.CIJ,GV.AIJ,Nc);
  
  // Reading configuration parameter
  read_conf_dynamicfile(&PRM,fileconf);
  
  
  // Calculate array of J
  jsize = (int) (PRM.jmax-PRM.jmin)/PRM.jstep;
  J = (double *)calloc(jsize,sizeof(double));
  for(i=0;i<jsize;i++){
    J[i] = jmin + i*PRM.jstep;
  }
  
  printf("jsize %f\n",jsize);
  //*
  //rprom
  MeanFR = (double *)calloc(jsize,sizeof(double));
  
//   int mysize = Netwk.Nnodes;
//   double *threshold;
//   threshold = (double *)calloc(mysize,sizeof(double));
//   get_rnd_uniform_vector(threshold,mysize);
  
//   for(i=0;i<mysize;i++)
//     printf("Threshold[%d]:%d\n",i,threshold[i]);
  //*
  printf("Calculando dinamica\n");
  DynamicSensibilityOneP(GV.AIJ,PRM, J, jsize, MeanFR);
  
  ofile = fopen("JMatrixC.txt","w");
  for(i=0;i<jsize;i++){
    fprintf(ofile,"%lf %lf\n",J[i],MeanFR[i]);
  }
  fclose(ofile);
  
//   fprintf(stdout,"%lf %lf %lf %d %d %d %d\n",PRM.pc, PRM.Dt, PRM.r0, 
// 	   PRM.Ntimes, PRM.Nr, PRM.NiAct, PRM.ncol);
  
  /*
  // Initializing some variables
  jsize = 1000;
  jmin = 0.0;
  jmax= 40.0;
  dJ = (jmax-jmin)/Jsize;
  
  // Allocating local variables
  raster = (double *)malloc(Nr*Ntimes*sizeof(double));
  nu = (double *)malloc(Nr*sizeof(double));
  J = (double *)malloc((jsize*sizeof(double));
  
  MeanFR = (double *)malloc(jsize*sizeof(double));
  //*/
  return 0;
}