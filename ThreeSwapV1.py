from mymodules import *

Ai = np.loadtxt("./data/AdjacencyMatrix0.txt",dtype="int")
NR = 50000
Eff3swap = 0
r = np.zeros(4)
rold = np.zeros(4)
rpearson = np.array([]).reshape(0,4)
rw = np.array([0])
Terms = np.zeros((4,4))
Delta = np.zeros(4)
DeltaSave = np.array([]).reshape(0,4)
#condition = "Delta[0]>0 and Delta[1]>0 and Delta[2]>0"
condition = "Delta[0]>0"

# Degree of nodes:
InD,OutD = GetDegree(Ai)
Nedges = np.sum(InD)

#Initial assortativity
for mode in xrange(4):
  r[mode],Terms[mode] = Assortativity_Foster(Ai,mode,opt=True)

rold = r.copy()
rpearson = np.append(rpearson,[r],axis=0)
A = Ai.copy()
for n in xrange(1,NR+1):
  A,success, EdgesR =ThreeSwap(A)
  
  if success:
    for mode in xrange(4):
      Delta[mode] = DeltaR3swap(EdgesR,Nedges,InD,OutD,Terms[mode,:],mode)
      #r[mode] = Assortativity_Foster(A,mode)

    #Delta = r - rold
    if eval(condition):
      # Calculating the new assortativity
      for mode in xrange(4):
	#r[mode] = rpearson2[Eff3swap,mode] + Delta2[mode]
	r[mode] = rold[mode] + Delta[mode]
      
      rpearson = np.append(rpearson,[r],axis=0)
      rold = r.copy()
      Eff3swap+=1
      rw=np.append(rw,n)
      
  sys.stdout.write("Progress: %.2f %%\r"%((100.0*n)/NR))
  if n>=NR:
    sys.stdout.write("\n")
  sys.stdout.flush()
  
np.savetxt("./3swap/MatrixFinal100_3swap.txt",A)
np.savetxt("./3swap/Pearson3swap100.txt",rpearson)
print "Neffective: %d"%Eff3swap
plt.figure()
Label=["In-Out","Out-In","In-In","Out-Out"]
Label2 = ["In-Out 2","Out-In 2","In-In 2","Out-Out 2"]
for mode in xrange(4):
  plt.plot(rw,rpearson[:,mode],'-',label=Label[mode])
plt.xlabel("Number of rewiring")
plt.ylabel("Assortativity")
plt.title("With 3swap")
plt.legend(loc="best")
plt.savefig("3swapProbe100.pdf")

