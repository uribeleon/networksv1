#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

Ai = np.loadtxt("./data/MatrixInitial_500.txt")
Din,Dout = GetDegree(Ai)
din = list(np.array(Din,dtype="int"))
dout = list(np.array(Dout,dtype="int"))

GHH = nx.directed_havel_hakimi_graph(din,dout)
AHH = nx.adjacency_matrix(GHH)
np.savetxt("./data/MatrixInitial_500HH.txt",AHH,fmt="%d")