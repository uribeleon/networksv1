#include<stdio.h>

void GreatersArray(int idx_value, int V[], int size, int Vout[])
{
    int i;
    for(i=0;i<size;i++)
    {
        Vout[i] = V[i]>V[idx_value];
    }
    
}

void EqualsArray(int idx_value, int V[], int size, int Vout[])
{
    int i;
    for(i=0;i<size;i++)
    {
        Vout[i] = (V[i]==V[idx_value]);
    }
}

float sum_elmts_vector(int a1[], int size)
{
    int i;
    float sum=0;
    for(i=0;i<size;i++)
        sum = sum + a1[i];
    return sum;
}

void get_averagerank(int V[], int size, float Rnk_Vec[])
{
    int i;
    int vt_greater[size]; // 
    int vt_equal[size];
    float t1,t2;
    
    for(i=0;i<size;i++)
    {
        GreatersArray(i,V,size,vt_greater);
        EqualsArray(i,V,size,vt_equal);
        t1 = sum_elmts_vector(vt_greater,size);
        t2 = sum_elmts_vector(vt_equal,size);
        Rnk_Vec[i] = t1 + 0.5*(1 + t2);
    }
}
int main()
{
    int vec[10] = {4, 2, 4, 5, 2, 3, 1, 17,17,4};
    int j;
    float rank_vector[10];
    
    
    get_averagerank(vec,10,rank_vector);
    //printf("%f\n",sum_elmts_vector(vec,10));
    for(j=0;j<10;j++)
        printf("%.2f,",rank_vector[j]);
    printf("\n");

return 0;
}