import numpy as np
import os,sys

COND = list()
ifile = open("./data/condiciones.txt","r")

for line in ifile:
  COND.append(line.strip())


N = len(COND)

ofile = open("conditions.cpp","w")

ofile.write("""\
int EvalCondition(char *cond, int Delta[4]){

   int i;
   int out;

   if (!strcmp("%s",cond)){
     out = %s;
     return out;
   }
"""%(COND[0],COND[0]))

for i in xrange(1,N):
	ofile.write("""\
   else if (!strcmp("%s",cond)){
     out = %s;
     return out;
   }	
"""%(COND[i],COND[i]))

ofile.write("""\
   else{
     printf("Configuration does not exit");
     return 0;
   }
}
""")
