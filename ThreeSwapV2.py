from mymodules import *

Ai = np.loadtxt("./data/AdjacencyMatrix0.txt",dtype="int")
NR = 500000
Eff3swap = 0
r = np.zeros(4)
r2 = np.zeros(4)
rold = np.zeros(4)
rold2 = np.zeros(4)
rpearson = np.array([]).reshape(0,4)
rpearson2 = np.array([]).reshape(0,4)
rw = np.array([0])
Terms = np.zeros((4,4))
Delta = np.zeros(4)
Delta2 = np.zeros(4)
#DeltaSave = np.array([]).reshape(0,4)
DeltaSave = []
condition = "Delta[0]>0 and Delta[1]>0 and Delta[2]>0 and Delta[3]<0"
#condition = "Delta[0]>0"

# Degree of nodes:
InD,OutD = GetDegree(Ai)
Nedges = np.sum(InD)

#Initial assortativity
for mode in xrange(4):
  r[mode],Terms[mode] = Assortativity_Foster(Ai,mode,opt=True)
  
r2 = r.copy()
rold = r.copy()
rold2 = r.copy()
rpearson = np.append(rpearson,[r],axis=0)
rpearson2 = np.append(rpearson2,[r2],axis=0)
A = Ai.copy()
for n in xrange(1,NR+1):
  Matrix,success, EdgesR =ThreeSwap(A)
  
  if success:
    for mode in xrange(4):
      Delta[mode] = DeltaR3swap(EdgesR,Nedges,InD,OutD,Terms[mode,:],mode)
      #r2[mode] = Assortativity_Foster(Matrix,mode)

    #Delta2 = r2 - rold2
    if eval(condition):
      A = Matrix.copy()
      # Calculating the new assortativity
      for mode in xrange(4):
	r[mode] = rold[mode] + Delta[mode]
	
      #DeltaSave.append([r,r2,Delta[0],Delta2[0],n])
      rpearson = np.append(rpearson,[r],axis=0)
      #rpearson2 = np.append(rpearson2,[r2],axis=0)
      rold = r.copy()
      #rold2 = r2.copy()
      Eff3swap+=1
      rw=np.append(rw,n)
      
  sys.stdout.write("Progress: %.2f %%\r"%((100.0*n)/NR))
  if n>=NR:
    sys.stdout.write("\n")
  sys.stdout.flush()
  
#np.savetxt("./3swap/MatrixFinal100_3swap.txt",A)
#np.savetxt("./3swap/Pearson3swap100.txt",rpearson)
print "Neffective: %d"%Eff3swap
plt.figure()
Label=["In-Out","Out-In","In-In","Out-Out"]
Label2 = ["In-Out 2","Out-In 2","In-In 2","Out-Out 2"]
for mode in xrange(4):
  plt.plot(rw,rpearson[:,mode],'-',label=Label[mode])
plt.xlabel("Number of rewiring")
plt.ylabel("Assortativity")
plt.title("With 3swap delta")
plt.legend(loc="best")
plt.savefig("3swap_N100_R500mil_Delta.pdf")


#plt.figure()
#for mode in xrange(4):
  #plt.plot(rw,rpearson2[:,mode],'-',label=Label2[mode])
#plt.xlabel("Number of rewiring")
#plt.ylabel("Assortativity")
#plt.title("With 3swap formula")
#plt.legend(loc="best")
#plt.savefig("3swap_N100_V2_Formula.pdf")

