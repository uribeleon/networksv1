#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
#filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']

#profiles folder
DirProfiles = Argv[1]

#Reading name of profiles
NAME = np.loadtxt(filenames[0],dtype="|S14")


files = ["%sMaxEigVal-profiles.txt"%DirProfiles,"%sEntropy-values.txt"%DirProfiles]

#"""
# Reading file of eigenvalues
print "="*50
print " Reading file of eigenvalues..."
#Li,Lf = np.loadtxt(files[0],usecols=(1,2),unpack=True)
names = np.loadtxt(files[0], usecols=[0], dtype="|S14")
#EigVals = np.array(zip(names,Li,Lf),dtype=[('names','S14'),('Li','float64'),('Lf','float64')])
#"""

# Reading file of entropies.
print " Reading file of entropies..."
SiE,SfE,SiN,SfN,SiNJ,SfNJ = np.loadtxt(files[1],usecols=range(1,7),unpack=True)
Entropy = np.array(zip(names,SiE,SfE,SiN,SfN,SiNJ,SfNJ),dtype=[('names','S14'),
								('SiE','float64'),('SfE','float64'),
								('SiN','float64'),('SfN','float64'),
								('SiNJ','float64'),('SfNJ','float64')])

#Size = len(names)
Size = len(NAME)



#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                                   PLOTING
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#===============================================
# Plot of eigenvalues
#===============================================
"""
print " Begining with the plots...."
print "----> Plotting eigenvalues ..."
#Sorting data
EigVals.sort(order='Lf')
#mpl.rcParams['xtick.major.pad'] = 10 
fig=plt.figure(figsize=(8,8))
#ax = fig.add_subplot(111)
plt.axes((0,0.5,2,0.5)) 
plt.plot(EigVals['Li'],'go-',label='initial matrix')
plt.plot(EigVals['Lf'],'co-',mec='b',label='final matrix')
plt.title("Maximum eigenvalue of normalized laplacian matrix")
plt.ylabel(r"$\lambda_{max}$",fontsize=15)
#plt.rc('xtick.major', size=4, pad=30)
plt.xticks(range(Size),EigVals['names'],rotation=70,fontsize=10,ha='right')
plt.subplots_adjust(bottom=0.15)
plt.xlim(xmin=-1,xmax=Size+1)
plt.grid(color='k',linestyle='-.',alpha=0.5)
plt.legend(loc='upper left')
plt.savefig(DirProfiles+"PLOT-MaxEigVal.pdf",bbox_inches='tight')
"""

#===============================================
# Plot of SfE
#===============================================
print "----> Plotting Entropy - exact form ..."
Entropy.sort(order='SfE')
index = [ i for i in xrange(len(Entropy['names'])) if Entropy['names'][i] in NAME]
fig = plt.figure(figsize=(14,8))
#plt.axes((0,0.5,2,0.5))
plt.plot(Entropy['SiE'][index],'go--',label="Before")
plt.plot(Entropy['SfE'][index],'co-',mec='b',label="After")
plt.ylabel("Entropy from eigenvalues")
plt.xticks(range(Size),Entropy['names'][index],rotation=70,fontsize=10,ha='right')
plt.subplots_adjust(bottom=0.15)
plt.grid(color='k',linestyle='-.',alpha=0.5)
plt.legend(loc='upper left')
plt.title("SVN ExactForm")
plt.savefig(DirProfiles+"PLOT-SVN-ExactForm.pdf",bbox_inches='tight')
plt.close()


#===============================================
# Plot of SfN
#===============================================
print "----> Plotting Entropy - approximated form ..."
Entropy.sort(order='SfN')
index = [ i for i in xrange(len(Entropy['names'])) if Entropy['names'][i] in NAME]
fig = plt.figure(figsize=(14,8))
#plt.axes((0,0.5,2,0.5))
plt.plot(Entropy['SiN'][index],'go--',label="Before")
plt.plot(Entropy['SfN'][index],'co-',mec='b',label="After")
plt.ylabel("Entropy using approximate form")
plt.xticks(range(Size),Entropy['names'][index],rotation=70,fontsize=10,ha='right')
plt.subplots_adjust(bottom=0.15)
plt.grid(color='k',linestyle='-.',alpha=0.5)
plt.legend(loc='upper left')
plt.title("SVN ApproxForm")
plt.savefig(DirProfiles+"PLOT-SVN-ApproxForm.pdf",bbox_inches='tight')
plt.close()

#===============================================
# Plot of SfNJ
#===============================================
print "----> Plotting Entropy - normal approximated form ..."
Entropy.sort(order='SfNJ')
index = [ i for i in xrange(len(Entropy['names'])) if Entropy['names'][i] in NAME]
fig = plt.figure(figsize=(14,8))
#plt.axes((0,0.5,2,0.5))
plt.plot(Entropy['SiNJ'][index],'go--',label="Before")
plt.plot(Entropy['SfNJ'][index],'co-',mec='b',label="After")
plt.ylabel("Entropy using normalized form")
xx, locs = plt.yticks()
ytick = ['%.5f' % a for a in xx]
plt.yticks(xx,ytick)
plt.xticks(range(Size),Entropy['names'][index],rotation=70,fontsize=10,ha='right')
plt.subplots_adjust(bottom=0.15)
plt.grid(color='k',linestyle='-.',alpha=0.5)
plt.legend(loc='upper left')
plt.title("JVN NormalForm")
plt.savefig(DirProfiles+"PLOT-JVN-NormalForm.pdf",bbox_inches='tight')
plt.close()

print "="*50