#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#==========================
# SOME USEFUL FUNCTIONS
#==========================
def GetRegulatedSecuence(condition):
  nc = len(condition)
  CondReg = list()
  for i in xrange(nc):
    reg = np.array(map(int,condition[i].split()))
    reg = " ".join( (reg**0 - np.abs(reg)).astype('str') )
    CondReg.append(reg)
  return CondReg
    
#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
#filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt','./data/CondRegulated.txt']

#Number of rewirings
Nrewiring = int(Argv[1])

#profiles folder
DirProfiles = Argv[2]

#Initial Matrix path
PathMat = Argv[3]
nameMati = PathMat.split("/")[-1]

#Nprocs = int(argv[3]) 

#Reading name of profiles
NAME = np.loadtxt(filenames[0],dtype="|S14")

#Reading conditions
ifile = open(filenames[2])
COND = ifile.read().splitlines()
ifile.close()

#Reading regulated sequence
ifile2 = open(filenames[3])
REG = ifile2.read().splitlines()
ifile2.close()

# Total number of profiles
Ntotal = len(NAME)

# Verifying if profiledir exist.
system("rm -rf %s"%DirProfiles)
print "Creating each profile folder..."
os.mkdir(DirProfiles)
for i in xrange(Ntotal):
  os.mkdir("%s%s"%(DirProfiles,NAME[i]))  
 
# Creating a copy of matrix into de folder
if not os.path.exists(DirProfiles + nameMati):
  os.system("cp %s %s"%(PathMat,DirProfiles+nameMati))
 
print "*"*70
for i in xrange(Ntotal):
  fileassort = DirProfiles + NAME[i] + "/Assort" + NAME[i] + ".txt"
  filematrix = DirProfiles + NAME[i] + "/Matrix" + NAME[i] + ".txt"
  fileedges = DirProfiles + NAME[i] + "/EdgesRw" + NAME[i] + ".txt"
  #order = "./Three_Swap_regulated.x %s %d '%s' '%s' %s %s %s"%( (DirProfiles + nameMati), Nrewiring, COND[i], REG[i], fileassort, filematrix, fileedges)
  order = "./Three_Swap_rank_reg.x %s %d '%s' '%s' %s %s %s"%( (DirProfiles + nameMati), Nrewiring, COND[i], REG[i], fileassort, filematrix, fileedges)
    
  print "\n--------> Run profile %s  --  progress: %.2f %%"%(NAME[i],(1.0*i)/(Ntotal-1)*100)
  system(order)
  print "\n"+"c"*70
