from mymodules import *

global SOURCE
global TARGET

SOURCE = 0
TARGET = 1

def AverageRank(V):
	Size=len(V)
	rankvec=np.zeros(Size)
	for i in xrange(Size):
		rankvec[i] = np.sum(V>V[i]) + 0.5*( np.sum(V==V[i]) + 1.0 )
	return rankvec

def SpearmanRho(A,mode,rnki,rnko,opt):
	# Determining edges
	Ed = GetList(A)

	# Number of edges
	M = len(Ed)

	# (alpha,beta) : (In,Out)
	if mode==0:
		sigma_a = np.sqrt( 4.*np.sum(rnki[Ed[:,SOURCE]]**2) - M*(M + 1)**2 )
		sigma_b = np.sqrt( 4.*np.sum(rnko[Ed[:,TARGET]]**2) - M*(M + 1)**2 )

		r = (4.0*np.sum( rnki[Ed[:,SOURCE]]*rnko[Ed[:,TARGET]] ) - M*(M+1)**2 )/(sigma_a*sigma_b)
	return r

def SpearmanRho2(A,mode,rnki,rnko,opt):
	
	# Determining edges
	Ed = GetList(A)

	# Number of edges
	M = 1.*len(Ed)

	# (alpha,beta) : (In,Out)
	if mode==0:
		jmean = np.sum(rnki[Ed[:,0]])/M # source
		kmean = np.sum(rnko[Ed[:,1]])/M # target
		sigma_a = np.sqrt( np.sum( (rnki[Ed[:,0]] - jmean)**2 )/M )
		sigma_b = np.sqrt( np.sum( (rnko[Ed[:,1]] - kmean)**2 )/M )
		r = np.sum((rnki[Ed[:,0]] - jmean)*(rnko[Ed[:,1]] - kmean))/(M*sigma_a*sigma_b)
	return r

 #    return r

Ai = np.loadtxt("./3swap/AdjacencyMatrix0.txt",dtype="int")
Din,Dout = GetDegree(Ai)
Rin = AverageRank(Din)
Rout = AverageRank(Dout)
RiS = SpearmanRho2(Ai,0,Rin,Rout,0)
RiP = Assortativity_Foster(Ai,0)
print RiP,RiS

#A1 = np.array([1,2,1,3,3],dtype="int")
#A1 = np.array([4, 2, 4, 5, 2, 3, 1, 17,17,4],dtype="int")
#print AverageRank(A1)