from mymodules import *

Ai = np.loadtxt("./data/MatrixInitial_500.txt",dtype="int")
NR = 10
Eff3swap = 0
r = np.zeros(4)
r2 = np.zeros(4)
Terms = np.zeros((4,4)) 
Delta = np.zeros(4)
Delta2 = np.zeros(4)
rpearson = np.array([]).reshape(0,4)
rpearson2 = np.array([]).reshape(0,4)
DeltaSave = np.array([]).reshape(0,4)
DeltaSave2 = np.array([]).reshape(0,4)
#condition = "Delta[0]>0 and Delta[1]>0 and Delta[2]>0"
condition = "Delta[0]>0"

# Degree of nodes:
InD,OutD = GetDegree(Ai)
Nedges = np.sum(InD)

#Initial assortativity
for mode in xrange(4):
  r[mode],Terms[mode] = Assortativity_Foster(Ai,mode,opt=True)
r2 = r.copy()

rpearson = np.append(rpearson,[r],axis=0)
rpearson2 = np.append(rpearson2,[r2],axis=0)
success = 0
i=0
while not success:
  A,success, EdgesR = ThreeSwap(Ai)
  i+=1
  
print i
print "antes rpearson 1 ---- ",rpearson
print "antes rpearson 2 ---- ",r2 + Delta2
print "Calculando delta por los dos metodos"

for mode in xrange(4):
  Delta2[mode] = DeltaR3swap(EdgesR,Nedges,InD,OutD,Terms[mode,:],mode)
  r[mode] = Assortativity_Foster(A,mode,opt=False)

rpearson = np.append(rpearson,[r],axis=0)
rpearson2 = np.append(rpearson2,[r2 + Delta2],axis=0)
Delta = r-r2
print "Delta 1 --- ",Delta
print "Delta 2 --- ",Delta2
print "Diff  ---  ",Delta-Delta2
print "rpearson 1 ---- ",rpearson[1]
print "rpearson 2 ---- ",rpearson2[1]
