#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

// int exec(char *cmd){
// 	char temp[256];
// 	char *flag;
// 	FILE *In;
// 	
// 	In = popen(cmd,"r");
// 	
// 	if (!In) return (1);
// 	flag = fgets(temp,256,In);
// 	
// 	pclose(In);
// 	return atoi(temp);
// }
char temp[256];

int exec(char *cmd){
	
	FILE *In;
	
	In = popen(cmd,"r");
	
	if (!In) return (1);
	fgets(temp,256,In);
	
	pclose(In);
	return atoi(temp);
}


int main(int argc, char *argv[]){
  
  int i,Nrows;
  double (*r)[4];
  char cmd[256],*filename;
  FILE *ifile,*ofile;
  
 
  filename = argv[1];
  sprintf(cmd,"head ./data/OutPrm.txt");
  Nrows = exec(cmd);
  
  r = (double (*)[4])malloc(Nrows*4*sizeof(double));
  
  ifile = fopen(filename,"rb");
  fread(&r[0][0],sizeof(double),4*Nrows,ifile);
  fclose(ifile);
  
  ofile = fopen("./data/Asort.txt","w");
  for(i=0;i<Nrows;i++){
    fprintf(ofile,"%lf %lf %lf %lf\n",r[i][0],r[i][1],r[i][2],r[i][3]);
  }
  fclose(ofile);
  

  return(0); 
    
}