from mymodules import *

A = np.array([
	[0,1,1,1,1,1],
	[1,0,0,0,0,1],
	[1,1,0,1,0,1],
	[1,1,1,0,1,1],
	[0,1,0,1,0,1],
	[1,1,0,1,1,0]])

Id,Od = GetDegree(A)

M = np.sum(Id)
Ed = GetList(A)
jmean = np.sum(Od[Ed[:,0]])/M # source
kmean = np.sum(Id[Ed[:,1]])/M # target
sigma_a = np.sqrt( np.sum( (Od[Ed[:,0]] - jmean)**2 )/M )
sigma_b = np.sqrt( np.sum( (Id[Ed[:,1]] - kmean)**2 )/M )