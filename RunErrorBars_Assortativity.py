#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
#import networkx as nx
from mymodules import *
import subprocess as sbp

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#==========================
# SOME USEFUL FUNCTIONS
#==========================
def GetRegulatedSecuence(condition):
  nc = len(condition)
  #CondReg = list()
  reg = np.array(map(int,condition.split()))
  reg = " ".join( (reg**0 - np.abs(reg)).astype('str') )
  #for i in xrange(nc):
    #reg = np.array(map(int,condition[i].split()))
    #reg = " ".join( (reg**0 - np.abs(reg)).astype('str') )
    #CondReg.append(reg)
  CondReg = reg[:]
  return CondReg


#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
#filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt','./data/CondRegulated.txt']

#Condition
COND = Argv[1]

#Number of rewirings
Nrewiring = int(Argv[2])

# Output folder
DirProfiles = Argv[3]

#Initial Matrix path
PathMat = Argv[4]
nameMati = PathMat.split("/")[-1]

#Reading regulated sequence
REG = GetRegulatedSecuence(COND)

# Total number of training for one profile
Ntotal = int(Argv[5])

# Type
Type = Argv[6]
Nprocs = 2

NAME = "_" + COND.replace(" ","_")

# delete content of folder "NAME"
if os.path.exists(DirProfiles + NAME):
	system("rm -rf %s"%(DirProfiles + NAME + "/*"))
else:
	#Creating subfolder
	os.mkdir(DirProfiles + NAME)




if(Type=="serial"):
    print(">"*20 + " SERIAL MODE......")
    print "*"*70
    for i in xrange(Ntotal):
      fileassort = DirProfiles + NAME + "/Assort" + NAME + "_V" + str(i) + ".txt"
      filematrix = DirProfiles + NAME + "/Matrix" + NAME + "_V" + str(i) + ".txt"

      #order = "./Three_Swap_regulated.x %s %d '%s' '%s' %s %s"%( PathMat, Nrewiring, COND, REG, fileassort, filematrix)

      #  ===== Three_Swap_rank_reg
      #order = "./Three_Swap_rank_reg.x %s %d '%s' '%s' %s %s"%( PathMat, Nrewiring, COND, REG, fileassort, filematrix)

      #  ===== Two_Swap_ranked
      order = "./Two_Swap_ranked.x %s %d '%s' %s %s"%( PathMat, Nrewiring, COND, fileassort, filematrix)

      sys.stdout.write("\n--------> Run %d  --  progress: %.2f %%\n"%( i,(1.0*i)/(Ntotal-1)*100))
      sys.stdout.flush()
      system(order)
      print "\n"+"c"*70

if(Type == "parallel"):
    Nprocs = int(Argv[7])
    print(">"*20 + " PARALLEL MODE......")
    nblocks = int(Ntotal/Nprocs)

    for block in xrange(nblocks):
      if(block < nblocks - 1):
          filesNum = list( range(block*Nprocs,(block+1)*Nprocs) )
      else:
          filesNum = list( range(block*Nprocs,Ntotal) )
      for i in filesNum:
          p = [1]*len(filesNum)
          fileassort = DirProfiles + NAME + "/Assort" + NAME + "_V" + str(i) + ".txt"
          filematrix = DirProfiles + NAME + "/Matrix" + NAME + "_V" + str(i) + ".txt"
          order = "./Two_Swap_ranked.x %s %d '%s' %s %s "%( PathMat, Nrewiring, COND, fileassort, filematrix)
          p = sbp.Popen(order,shell=True)#system(order)

        #   print fileassort
        #   print filematrix
        #   print order
    #   system("wait")
      p.wait()
      (output, err) = p.communicate()
      print "salida.......",output
      print("\n--------> Block %d  --  progress: %.2f %%\n"%( block,(1.0*block)/(nblocks-1)*100))
    #   sys.stdout.write("\n--------> Block %d  --  progress: %.2f %%\n"%( block,(1.0*block)/(nblocks-1)*100))
    #   sys.stdout.flush()




  #order = "./Three_Swap_regulated.x %s %d '%s' '%s' %s %s"%( PathMat, Nrewiring, COND, REG, fileassort, filematrix)

  #  ===== Three_Swap_rank_reg
  #order = "./Three_Swap_rank_reg.x %s %d '%s' '%s' %s %s"%( PathMat, Nrewiring, COND, REG, fileassort, filematrix)

  #  ===== Two_Swap_ranked



print "\n"+"c"*70
