import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math

#==============================
# MACROS
#==============================
JMEAN,KMEAN,SIGMJ,SIGMK = range(4)
SOURCE,TARGET,NTARGET = range(3)
NA,NC,NE = range(3)
NB,ND,NF = range(3)
NBp,NDp,NFp = range(3)



def Print_parameters(prm):

  Ofile = open("./data/InitialParam.param","w")
  Ofile.write('''\
%%****************************************
%% PARAMETERS FILE
%%****************************************

%%Number of Nodes 
Nn      =      %d

%%Number od conections
NC      =      %d
'''%(int(prm[0]), int(prm[1])))
  Ofile.close()


def GetList(A):
    Ls = np.transpose(np.nonzero(A))
    return Ls
	

#=========================================
# Probability function for neuron activity
# - Stochastic mode
#=========================================
def Firing_Probability(x,h0,J,pc,A):
  N = len(x)
  nu_new = np.zeros(N)
  fc = J/(N*pc)
  nu_new = 1.0/( 1.0 + np.exp( h0 - fc*np.dot(A,x) ) )
  return nu_new

#=========================================
# Probability function for neuron activity
# - Non-stochastic mode
#=========================================
def FiringRate_nonstochastic(nu,h0,J,pc,A):
  N = len(nu)
  nu_new = np.zeros(N)
  fc = J/(N*pc)
  nu_new = 1.0/( 1.0 + np.exp( h0 - fc*np.dot(A,nu) ) )
  return nu_new

#=========================================
# Probability function for neuron activity
# - Non-stochastic mode - Mean Field Limit
#=========================================
def FiringRate_MeanField(nu,h0,J):
  nu_new = 1.0/( 1.0 + np.exp( h0 - J*nu) ) 
  return nu_new


#=========================================
# Von Neumann Entropy for directed graph
#=========================================
def VNEntropy_directed(A,flag=0):
  
  Indeg , Outdeg = GetDegree(A)
  Vol = len(A)
  Elm = GetList(A)
  Sum = 0
  
  for i in range(len(Elm)):
    Sum = Sum + 1.0*Indeg[Elm[i,0]]/(Indeg[Elm[i,1]]*Outdeg[Elm[i,0]]**2)
    if A[Elm[i,0],Elm[i,1]] == A[Elm[i,1],Elm[i,0]]:
      Sum = Sum + 1.0/(Outdeg[Elm[i,0]]*Outdeg[Elm[i,1]])
  if flag==0:
    EntropyVN = 1.0 - 1.0/Vol - Sum/(2.0*Vol*Vol)
  else:
    EntropyVN = 0.5*Sum/Vol
  return EntropyVN

#=======================================================
# Von Neumann Entropy for directed graph - exact form
#=======================================================
def VNEntropy_directed_exact(eigval, size,flag=0):
  Sum=0
  f = 1.0/size
  if flag == 0: #Exact Form
    for i in range(size):
      if eigval[i] >= 1E-7:
	Sum =  Sum + f*eigval[i]*np.log(eigval[i]*f)
    EntropyVN = -Sum
  if flag==1: # Approximate form
    for i in range(size):
      Sum = Sum + f*eigval[i]*(1-eigval[i]*f)
    EntropyVN = Sum
  return EntropyVN

#=========================================
# Asortativity Correct Form - INT
#=========================================
def Assortativity_Foster_int(A,flag,opt=False):
    
    #Calculating degree nodes
    indeg = np.sum(A,axis=0) # Sum over columns
    outdeg = np.sum(A,axis=1) #Sum over rows
    
    #Calculating number of conections
    M =np.sum(indeg)*1.0
    Minv = 1.0/M
    
    # Determining Edges
    Ed = GetList(A)

    # (alpha,beta) : (In-out)
    if flag == 0:
      jmean = np.sum(indeg[Ed[:,0]])*Minv # source
      kmean = np.sum(outdeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (indeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (outdeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      f1 = 1.0/(M*sigma_a*sigma_b)
      f2 = jmean*kmean/(sigma_a*sigma_b)
      
      r = f1 * np.sum( indeg[Ed[:,0]] * outdeg[Ed[:,1]] ) - f2
    # (alpha,beta) : (Out-In)  
    elif flag == 1:
      jmean = np.sum(outdeg[Ed[:,0]])*Minv # source
      kmean = np.sum(indeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (outdeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (indeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      f1 = 1.0/(M*sigma_a*sigma_b)
      f2 = jmean*kmean/(sigma_a*sigma_b)
      
      r = f1 * np.sum( outdeg[Ed[:,0]] * indeg[Ed[:,1]] ) - f2
    # (alpha,beta) : (In-In)  
    elif flag == 2:
      jmean = np.sum(indeg[Ed[:,0]])*Minv # source
      kmean = np.sum(indeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (indeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (indeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      f1 = 1.0/(M*sigma_a*sigma_b)
      f2 = jmean*kmean/(sigma_a*sigma_b)
      
      r = f1 * np.sum( indeg[Ed[:,0]] * indeg[Ed[:,1]] ) - f2
    # (alpha,beta) : (Out-Out)  
    elif flag == 3:
      jmean = np.sum(outdeg[Ed[:,0]])*Minv # source
      kmean = np.sum(outdeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (outdeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (outdeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      f1 = 1.0/(M*sigma_a*sigma_b)
      f2 = jmean*kmean/(sigma_a*sigma_b)
      
      r = f1 * np.sum( outdeg[Ed[:,0]] * outdeg[Ed[:,1]] ) - f2
    else:
      print "You need choose some configuration"
      sys.exit()

    if opt:
      return r, np.array([jmean, kmean, sigma_a, sigma_b])
    else:      
      return r
#=========================================
# Asortativity Foster
#=========================================
def Assortativity_Foster(A,flag,opt=False):
  
    #Calculating degree nodes
    indeg = np.sum(A,axis=0) # Sum over columns
    outdeg = np.sum(A,axis=1) #Sum over rows

    #Calculating number of conections
    M =np.sum(indeg)
    Minv = 1.0/M
    # Determining Edges
    Ed = GetList(A)
    
    #Calculating Jmean and Kmean
    #-------------------------------------------
    
    # (alpha,beta) : (In-out)
    if flag == 0: 
      jmean = np.sum(indeg[Ed[:,0]])*Minv # source
      kmean = np.sum(outdeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (indeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (outdeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      r = np.sum((indeg[Ed[:,0]] - jmean)*(outdeg[Ed[:,1]] - kmean))/(M*sigma_a*sigma_b)
      
    # (alpha,beta) : (Out-In)  
    elif flag==1:
      jmean = np.sum(outdeg[Ed[:,0]])*Minv # source
      kmean = np.sum(indeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (outdeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (indeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      r = np.sum((outdeg[Ed[:,0]] - jmean)*(indeg[Ed[:,1]] - kmean))/(M*sigma_a*sigma_b)
      
    # (alpha,beta) : (In-In)
    elif flag==2:
      jmean = np.sum(indeg[Ed[:,0]])*Minv # source
      kmean = np.sum(indeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (indeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (indeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      r = np.sum((indeg[Ed[:,0]] - jmean)*(indeg[Ed[:,1]] - kmean))/(M*sigma_a*sigma_b)
      
    # (alpha,beta) : (Out-Out)
    elif flag==3:
      jmean = np.sum(outdeg[Ed[:,0]])*Minv # source
      kmean = np.sum(outdeg[Ed[:,1]])*Minv # target
      sigma_a = np.sqrt( np.sum( (outdeg[Ed[:,0]] - jmean)**2 )*Minv )
      sigma_b = np.sqrt( np.sum( (outdeg[Ed[:,1]] - kmean)**2 )*Minv )
      
      r = np.sum((outdeg[Ed[:,0]] - jmean)*(outdeg[Ed[:,1]] - kmean))/(M*sigma_a*sigma_b)
    else:
      print "You need choose some configuration"
      sys.exit()

    if opt:
    	return r, np.array([jmean, kmean, sigma_a, sigma_b])
    else:      
    	return r




#===============================================
# Get rank of each degree
#===============================================
def GetDegreeRank(CIJ):
  pass

#===============================================
# SPEARMAN RHO
#===============================================
def SpearmanRho(CIJ,flag):
  pass

#===============================================
# AVERAGE RANK
#===============================================
#def AverageRank(DegV):
  pass

#===============================================
# STACK ALGORITHM
#===============================================
def StackAlgorithm(DegSeq,Mode,Train):

  #General Parameters
  M = np.sum(DegSeq["In"]) # Number of connections
  Nnodes = len(DegSeq["In"])
  Nmax = Nnodes-1
  C = np.zeros((Nnodes,Nnodes))

  #--------------------------------------
  # Verifying type of correlation
  #--------------------------------------
  if Train>0:
    #****  Sorted from lowest to highest ****
    idOut = np.argsort(DegSeq["Out"])
    idIn = np.argsort(DegSeq["In"]) # In-degree sorted by arguments (id of the nodes)
  else:
    #****  Sorted from highest to lowest ****
    idOut = np.argsort(DegSeq["Out"]) # High output with low input
    idIn = np.argsort(DegSeq["In"])[::-1] # In-degree sorted by arguments (id of the nodes)

  #--------------------------------------
  # Verifying type of profile           
  #--------------------------------------
  if Mode=='OutIn':
    OutS = DegSeq["Out"][idOut].copy()
    InS = DegSeq["In"][idIn].copy() # In-degree sorted
    InS,OutS = Connect_Stacks(C,OutS,InS,idOut,idIn)

    
  elif Mode=="InOut":  
    OutS = DegSeq["Out"][idIn].copy()
    InS = DegSeq["In"][idOut].copy() # In-degree sorted
    print OutS,InS
    InS,OutS = Connect_Stacks(C,OutS,InS,idIn,idOut)

  return C,np.array([InS,OutS])

#===============================================
# FUNTION TO CONNECT NODES IN STACK ALGORTIHM
#===============================================
def Connect_Stacks(C,outS,inS,ID1,ID2):
  Nmax = len(C) - 1

  #Connecting nodes
  for i in xrange(Nmax,-1,-1):
    for j in xrange(Nmax,-1,-1):
      if inS[j] > 0:
        #if C[ idIn[i], idOut[j]] != 1 and idIn[i]!=idOut[j]:
        if ID1[i]!=ID2[j]:
          C[ ID1[i], ID2[j]] = C[ ID1[i], ID2[j]] + 1
          inS[j] = inS[j] - 1
          outS[i] = outS[i] - 1
      else:
        continue
      if outS[i] == 0: break

    # Last node with disconnected stubs
    if i==0 and j==0 and outS[i]!=0:
      InStubs=np.where(inS>0)[0]
      print InStubs
      DiscStubs = np.sum(inS)
      for nstbs in xrange(DiscStubs):
        for k in xrange(len(InStubs),-1,-1):
          if inS[k] > 0:
            C[ ID1[i], ID2[k]] = C[ ID1[i], ID2[k]] + 1
            inS[k] = inS[k]-1
            outS[i] = outS[i]-1
        if outS[i] ==0: break

  return inS,outS
#=========================================================================================================================
# PLOT FUNCTIONS
#=========================================================================================================================

#******************************
# GENERAL PARAMETERS OF PLOT
#******************************
#mpl.rcParams['xtick.labelsize'] = 17
#mpl.rcParams['ytick.labelsize'] = 17
#mpl.rcParams['font.family'] = 'cmr10'
#mpl.rcParams['xtick.minor.size'] = 3
#mpl.rcParams['font.size'] = 17.0

def twoswap(Cij):
  
  C = Cij.copy()
  OutN,InN = np.transpose(GetList(C))
  M = len(OutN)
  EdgeR = np.array([],dtype="int").reshape(0,2)
  flag=0
  
  # Choosing 2 stubs
  n1 = np.random.randint(M)
  n2 = np.random.randint(M)
  
  # Verifying if there is repeated target-nodes
  if InN[n1] == InN[n2]:
    return C,flag,EdgeR
  else:
    a = OutN[n1]; b = InN[n1]
    c = OutN[n2]; d = InN[n2]
    
    # Assigning b', d'
    bp,dp = d,b
    
    # Avoiding autoloops
    if ( a == bp or c == dp ):
      return C,flag,EdgeR
    else:
      #verifying existing connections
      if not (C[a,bp] or C[c,dp]):
	C[a,bp] = 1; C[a,b] = 0
	C[c,dp] = 1; C[c,d] = 0
	flag=1
	EdgeR = np.append(EdgeR,[[a,c]], axis = 0) # source nodes
	EdgeR = np.append(EdgeR,[[b,d]], axis = 0) # initial target nodes
	EdgeR = np.append(EdgeR,[[bp,dp]], axis = 0) # final target nodes

	return C,flag,EdgeR
      else:
	return C,flag,EdgeR

def ThreeSwap(Cij):

  C = Cij.copy()
  OutN,InN = np.transpose(GetList(C))
  M = len(OutN)
  EdgeR = np.array([],dtype="int").reshape(0,3)
  flag=0

  # Choosing 3 stubs
  n1 = np.random.randint(M)
  n2 = np.random.randint(M)
  n3 = np.random.randint(M)
  InId = np.array([InN[n1],InN[n2],InN[n3]])

  # Verifying if there is repeated target-nodes
  if len(np.unique(InId)) < 3:
      return C,flag,EdgeR
  else:

      a = OutN[n1]; b = InN[n1]
      c = OutN[n2]; d = InN[n2]
      e = OutN[n3]; f = InN[n3]

      # Assigning b', d', f'
      nrandom = np.random.rand()
      if nrandom <0.5:
	      bp,dp,fp = d,f,b
      else:
	      bp,dp,fp = f,b,d
      
      # Avoiding autoloops
      if ( a == bp or c == dp or e == fp ):
	      return C,flag,EdgeR
      else:
      
	      #verifying existing connections
	      if not (C[a,bp] or C[c,dp] or C[e,fp]):
		      C[a,bp] = 1; C[a,b] = 0
		      C[c,dp] = 1; C[c,d] = 0
		      C[e,fp] = 1; C[e,f] = 0
		      flag=1
		      EdgeR = np.append(EdgeR,[[a,c,e]], axis = 0) # source nodes
		      EdgeR = np.append(EdgeR,[[b,d,f]], axis = 0) # initial target nodes
		      EdgeR = np.append(EdgeR,[[bp,dp,fp]], axis = 0) # final target nodes

		      return C,flag,EdgeR
	      else:
		      return C,flag,EdgeR

def ThreeTwoSwap(Cij):

  C = Cij.copy()
  C2 = Cij.copy()
  OutN,InN = np.transpose(GetList(C))
  M = len(OutN)
  EdgeR = np.array([],dtype="int").reshape(0,3)
  flag=0

  # Choosing 3 stubs
  n1 = np.random.randint(M)
  n2 = np.random.randint(M)
  n3 = np.random.randint(M)
  InId = np.array([InN[n1],InN[n2],InN[n3]])

  a = OutN[n1]; b = InN[n1]
  c = OutN[n2]; d = InN[n2]
  e = OutN[n3]; f = InN[n3]

  # In-nodes
  InNodes = np.array([b, d, f])

  # Assigning b', d', f'
  np.random.shuffle(InNodes)
  bp,dp,fp = InNodes
  
  
  # Avoiding autoloops
  if ( (a == bp or c == dp or e == fp) ):
    return C,flag,EdgeR
  else:
    #verifying existing connections
    if not (C[a,bp] or C[c,dp] or C[e,fp]):
      C[a,bp] = 1; C[a,b] = 0
      C[c,dp] = 1; C[c,d] = 0
      C[e,fp] = 1; C[e,f] = 0
      flag=1
      EdgeR = np.append(EdgeR,[[a,c,e]], axis = 0) # source nodes
      EdgeR = np.append(EdgeR,[[b,d,f]], axis = 0) # initial target nodes
      EdgeR = np.append(EdgeR,[[bp,dp,fp]], axis = 0) # final target nodes

      return C,flag,EdgeR
    else:
      return C,flag,EdgeR

def DeltaR3swap(EdgesR,E,indeg,outdeg,tm,flag):

  #ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
  # sum over 3 choiced edges

  # In-Out
  if flag == 0:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
		    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) + \
		    (indeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NFp]] - tm[KMEAN]) - \
		    (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
		    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,ND]] - tm[KMEAN]) - \
		    (indeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,NF]] - tm[KMEAN])
  # Out-In
  elif flag == 1:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
		    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) + \
		    (outdeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NFp]] - tm[KMEAN]) - \
		    (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
		    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[TARGET,ND]] - tm[KMEAN]) - \
		    (outdeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(indeg[EdgesR[TARGET,NF]] - tm[KMEAN])
  # In-In
  elif  flag == 2:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
		    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) + \
		    (indeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NFp]] - tm[KMEAN]) - \
		    (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
		    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[TARGET,ND]] - tm[KMEAN]) - \
		    (indeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(indeg[EdgesR[TARGET,NF]] - tm[KMEAN])
  # Out-Out
  elif flag == 3:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
		    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) + \
		    (outdeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NFp]] - tm[KMEAN]) - \
		    (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
		    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,ND]] - tm[KMEAN]) - \
		    (outdeg[EdgesR[SOURCE,NE]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,NF]] - tm[KMEAN])
  else:
	  print("Choose a valid configuration")

  return ft*delta


def DeltaR2swap(EdgesR,E,indeg,outdeg,tm,flag):
  #ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
  # sum over 3 choiced edges

  # In-Out
  if flag == 0:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
	    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) - \
	    (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
	    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,ND]] - tm[KMEAN])
  # Out-In
  elif flag == 1:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
	    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) - \
	    (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
	    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[TARGET,ND]] - tm[KMEAN])
  # In-In
  elif  flag == 2:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
	    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) - \
	    (indeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(indeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
	    (indeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(indeg[EdgesR[TARGET,ND]] - tm[KMEAN])
  # Out-Out
  elif flag == 3:
    ft = 1.0/(E*tm[SIGMJ]*tm[SIGMK])
    delta = (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NBp]] - tm[KMEAN]) + \
	    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[NTARGET,NDp]] - tm[KMEAN]) - \
	    (outdeg[EdgesR[SOURCE,NA]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,NB]] - tm[KMEAN]) - \
	    (outdeg[EdgesR[SOURCE,NC]] - tm[JMEAN])*(outdeg[EdgesR[TARGET,ND]] - tm[KMEAN])
  else:
    print("Choose a valid configuration")

  return ft*delta


def SubPlots_Transient(Nactives,Size,ts,dtime,J0,PATHD,NAME,Mean1,Sigma1,Mean2,Sigma2,CL):
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # TRANSIENT
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    fig=plt.figure(figsize=(16,8))
    #ax=fig.add_subplot(111)
    plt.subplot(2,1,1)
    plt.plot(100*Nactives/Size,'-',lw=1.6,color = "red",label="$J=%.2f$"%J0)
    #plt.semilogx(Nactives,'-',lw=1.2,color = CL["Salmon 2"],label="J=%.2f"%J0)
    plt.xlabel("Time/timebin",fontsize=16)
    plt.ylabel("Percentage of Neurons activated",fontsize=14 )
    plt.title("Profile %s"%NAME,fontsize=19)
    plt.xlim(xmax=ts + 2*dtime + ts)
    if len(np.where(100.0/Size*Nactives[ts+dtime:]>20)[0])>1: # greater than 20%
      plt.yticks(np.linspace(0,100,11))
    plt.xticks(np.linspace(0,ts+2*dtime+ts,21))
    #plt.xticks(np.arange(0,ts+2*dtime,250))
  
    plt.axvline(x=ts-1, color ='k', ls="--", lw=2)
    plt.axvline(x=ts-1+dtime, color ='k', ls="--", lw=2)
    plt.axvline(x=ts-1 + 2*dtime,color ='k', ls="--",lw=2)
    
    
    leg = plt.legend(loc="best",fontsize=14)
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)
    plt.grid(which='both')
    #ax.xaxis.set_major_locator(MultipleLocator(250))
    #ax.xaxis.set_minor_locator(MultipleLocator(50))
  
    #-------------------------------
    # Stimulated zone
    #-------------------------------
    plt.subplot(2,2,4)
    dsigma=4
    #plt.axes([0.6,.42,0.25,0.25])
    #plt.plot(100.0*Nactives/Size,'-',lw=2,color = CL["Salmon 2"],label="J=%.2f"%J0)
    plt.plot(100.0*Nactives/Size,'-',lw=2,color = 'red',label="J=%.2f"%J0)
    plt.grid(which='both')
    Xfill = np.linspace(ts-1 +dtime,ts-1+2*dtime,dtime)
    
    #determinig if network is in HFS(High firing rate state)
    if len(np.where(100.0/Size*Nactives[ts+dtime:]>20)[0])>1:
      # Zone between the mean
      Ymin = (Mean1 - dsigma*Sigma1)*100.0/Size*np.ones(len(Xfill))
      Ymax = (Mean1 + dsigma*Sigma1)*100.0/Size*np.ones(len(Xfill))
      plt.fill_between(Xfill,Ymin,Ymax,alpha=0.6,color=CL["Tan"])    
      idmax = np.where(100.0/Size*Nactives[ts+dtime:]>10)[0][0] # greater than 5%
      plt.xlim(xmin = ts+dtime,xmax=ts-1+dtime+idmax+1)
      plt.axhline(y=Mean1*100/Size,color='k',ls='--')
      plt.ylim(ymax=100.0/500*Nactives[ts-1+dtime+idmax+1],ymin=0.0)
    else:
      # Zone between the mean
      Ymin = (Mean2 - dsigma*Sigma2)*100.0/Size*np.ones(len(Xfill))
      Ymax = (Mean2 + dsigma*Sigma2)*100.0/Size*np.ones(len(Xfill))
      plt.fill_between(Xfill,Ymin,Ymax,alpha=0.6,color=CL["Tan"])    
      ymaxfr = np.ceil(np.max(100.0/Size*Nactives[ts+dtime:ts+2*dtime]))
      MaxY = ymaxfr+1 if ymaxfr>Ymax[0] else Ymax[0]+1
      plt.ylim(ymax=MaxY,ymin=0.0)
      plt.xlim(xmin = ts+dtime,xmax=ts-1+2*dtime+1)
      plt.axhline(y=Mean2*100/Size,color='k',ls='--')
    
    #-------------------------------
    # Spontaneous zone
    #-------------------------------
    plt.subplot(2,2,3)
    #plt.axes([0.2,.42,0.25,0.25])
    plt.plot(100.0*Nactives/Size,'-',lw=2,color = "red",label="J=%.2f"%J0)
    plt.ylabel("With %d$\sigma$"%dsigma,fontsize=16)
    plt.grid(which='both')
  
    # Zone between the mean
    Xfill = np.linspace(ts-1,ts-1+dtime,dtime)
    Ymin = (Mean1 - dsigma*Sigma1)*100.0/Size*np.ones(len(Xfill))
    Ymax = (Mean1 + dsigma*Sigma1)*100.0/Size*np.ones(len(Xfill))
    plt.fill_between(Xfill,Ymin,Ymax,alpha=0.6,color=CL["Tan"])
    plt.axhline(y=Mean1*100/Size,color='k',ls='--')
  
    plt.xlim(xmax = ts+dtime,xmin=ts)
    #determining ymax
    ymaxfr = np.ceil(np.max(100.0/Size*Nactives[ts:ts+dtime]))
    MaxY = ymaxfr+1 if ymaxfr>Ymax[0] else Ymax[0]+1
    plt.ylim(ymax=MaxY,ymin=0.0)
    plt.savefig(PATHD + NAME + "/Sens_transient_sp%s.pdf"%NAME)
    plt.close()



def GetDegree(CIJ):
  indeg = np.sum(CIJ,axis=0) # columns
  outdeg = np.sum(CIJ,axis=1) # rows
  return indeg,outdeg


def Neffective(CIJ,xold):
  Xbool = xold.astype(bool)
  xcolumns=np.sum(CIJ[Xbool,:],axis=0)-1
  xcolumns[np.where(xcolumns==-1)[0]]=0
  f = np.sum(xcolumns)
  xeffective = np.sum(np.sum(CIJ[Xbool,:],axis=1))-f
  return xeffective
    
#*********************
# PROGRESS BAR
#*********************
def progress(width,percent,msg):
    marks = math.floor(width * (percent /100.0))
    spaces = math.floor(width - marks)
    loader = msg + '[' + ('=' * int(marks)) + (' ' * int(spaces)) + ']'
    sys.stdout.write("%s %d%%\r" % (loader, percent))
    if percent >= 100:
        sys.stdout.write("\n")
    sys.stdout.flush()
