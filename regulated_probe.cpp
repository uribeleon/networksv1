#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <time.h>

template <typename T> int sgn(T val) {
    return (0 < val) - (val < 0);
}

void split_condition(char *StrIni, const char *delimiter, int condition[]){
	char *word;
	int i=0;

	word = strtok(StrIni,delimiter);
	while(word!=NULL){
		condition[i] = atoi(word);
		word = strtok(NULL,delimiter);
		i++;
	}
}

bool evaluate_condition(double delta[4], int cond[4]){
	int sgn_delta[4];
	int i;
	int sum=0;
	for(i=0;i<4;i++){
		sum = sum + fabs( cond[i] - fabs(cond[i])*sgn(delta[i]) );
	}
	return !bool(sum);
}

bool evaluate_condition_regulated(double delta[4], int inicond[4], int cond[4], int idx_reg[4]){
	int sgn_delta[4];
	int i;
	int sum=0;
	for(i=0;i<4;i++){
		sum = sum + fabs( cond[i] - fabs(cond[i])*sgn(delta[i]) );
	}
	
	if(!bool(sum)){
		for(i=0;i<4;i++){
			cond[i] = inicond[i]-idx_reg[i]*sgn(delta[i]);
		}
	}
	return !bool(sum);

}

int main(int argc, char *argv[]){
//*
	char *cond;
	int cond_cpy[4];
	int idx_regcond[4];
	int condition[4];
	int i;
	int j=0;
	int z=0,k=0;
	double r[4];
	bool result;
	

	cond = argv[1];
	split_condition(argv[2]," ",idx_regcond);

	printf("\ninitial condition(string): %s\n",cond);
	split_condition(cond, " ", condition);
	memcpy(cond_cpy,condition,sizeof(condition));
	for(i=0;i<4;i++) printf("%d ",condition[i] );
	printf("\n***-> integers\n");
	
	//*
	srand(time(NULL));
	for(k=0;k<10;k++){
			
		for(z=0;z<4;z++){
			r[z] = (2.0*rand())/RAND_MAX - 1.0;			
		}//*/
		result = evaluate_condition_regulated(r,condition,cond_cpy,idx_regcond);
		//printf("%d %d\n",result,k );
		

		//if(result){
			printf(" condition fulfilled:  %d\n", result );
			for(i=0;i<4;i++) printf("%lf sign: %d\n", r[i],sgn( r[i]));
			for(i=0;i<4;i++) printf("%d ",cond_cpy[i] );
			printf("\n\n");
			// k++;
		//}
	}
	return 0;
 

}