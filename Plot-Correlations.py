#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
#import matplotlib as mpl
#mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
from mymodules import *
import matplotlib as mpl
mpl.style.use('classic')

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

# DirName = Argv[1]
# profile = Argv[2]
fileini = Argv[1]
fileEnd = Argv[2]
pathdir = Argv[3]

Ai = np.loadtxt(fileini)
Af = np.loadtxt(fileEnd)

Edgesi = GetList(Ai)
Edgesf = GetList(Af)
din,dout = GetDegree(Ai)

Corri = np.column_stack((din[Edgesi[:,0]],dout[Edgesi[:,1]]))
Corrf = np.column_stack((din[Edgesf[:,0]],dout[Edgesf[:,1]]))


plt.figure()
plt.plot(Corrf[:,1],Corrf[:,0], 'o',color='#ffc700', ms=3, mec='#ffa500',label='Final Matrix')#color=(.38,.80,.98), ms=1, mec=(0.03, 0.62, 0.87),mew=0.5)
# plt.scatter(Corrf[:,1],Corrf[:,0], marker = 'o', s=3, c='#0d99b0')#color=(.38,.80,.98), ms=1, mec=(0.03, 0.62, 0.87),mew=0.5)
plt.xlabel("Out degree",fontsize=19)
plt.ylabel("In degree",fontsize=19)
plt.title("Profile 1000")
plt.xticks(size=19)
plt.yticks(size=19)
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig("%sFinalCorrelation.pdf"%(pathdir))


plt.figure()
# plt.scatter(Corri[:,1],Corri[:,0],marker = 'o',s=3,c='#0d99b0')#color=(.38,.80,.98), ms=1, mec=(0.03, 0.62, 0.87))
plt.plot(Corri[:,1],Corri[:,0],'o',color='#ffc700', ms=3, mec='#ffa500',label='Initial Matrix')
plt.xlabel("Out degree",fontsize=12)
plt.ylabel("In degree",fontsize=12)
plt.title("Profile 1000")
plt.xticks(size=12)
plt.yticks(size=12)
plt.grid()
plt.legend()
plt.tight_layout()
# plt.setp(ax1.get_xticklabels(), visible=False)

plt.tight_layout()
# plt.savefig("%sCorrelations.pdf"%(pathdir))
plt.savefig("%sInitialCorrelation.pdf"%pathdir)


# SUBPLOTS
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plt.figure(figsize=(8,8))
#
# ax2 = plt.subplot(212)
# plt.subplot(2,1,2)
# plt.plot(Corrf[:,1],Corrf[:,0], 'o',color='#ffc700', ms=3, mec='#ffa500',label='Final Matrix')#color=(.38,.80,.98), ms=1, mec=(0.03, 0.62, 0.87),mew=0.5)
# # plt.scatter(Corrf[:,1],Corrf[:,0], marker = 'o', s=3, c='#0d99b0')#color=(.38,.80,.98), ms=1, mec=(0.03, 0.62, 0.87),mew=0.5)
# plt.xlabel("Out degree",fontsize=12)
# plt.ylabel("In degree",fontsize=12)
# # plt.title("Profile 1000")
# plt.xticks(size=12)
# plt.yticks(size=12)
# plt.grid()
# plt.legend()
#
# ax1 = plt.subplot(211,sharex=ax2)
# # plt.scatter(Corri[:,1],Corri[:,0],marker = 'o',s=3,c='#0d99b0')#color=(.38,.80,.98), ms=1, mec=(0.03, 0.62, 0.87))
# plt.plot(Corri[:,1],Corri[:,0],'o',color='#ffc700', ms=3, mec='#ffa500',label='Initial Matrix')
# # plt.xlabel("Out degree",fontsize=12)
# plt.ylabel("In degree",fontsize=12)
# plt.title("Profile 1000")
# plt.xticks(size=12)
# plt.yticks(size=12)
# plt.grid()
# plt.legend()
# plt.setp(ax1.get_xticklabels(), visible=False)
#
# plt.tight_layout()
# plt.savefig("%sCorrelations.pdf"%(pathdir))

# plt.savefig("%sInitialCorrelation.pdf"%pathdir)
#plt.show()

# plt.figure()
# plt.savefig("%sFinalCorrelation.pdf"%(pathdir))
#plt.show()
