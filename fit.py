import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
from mymodules import *

def sigmoid(x,a,b,c):
  return c/(1.0 + np.exp( -(x-a)/b ) )

#==============
# SHORCUTS
#==============
system=os.system
argv = sys.argv


N=30
#"""
for i in xrange(N):
  order = 'python Dynamic-TheoreticalCurves.py FR-STOCHASTIC-ONER -s -p ./data/ -if ./AllProfiles/MatrixInitial.txt -v 0'
  out=system(order)
  if not out:
    system("mv ./data/MFRS_Minitial_h0.txt ./data/MFRS_Minitial%d.txt"%i)
    print "name changed in %d"%i
#"""

J,r = np.loadtxt("./data/MFRS_Minitial0.txt",unpack=True)
Size=len(r)
rs = np.zeros_like(r)
rs = rs + r
for i in xrange(1,N):
  r = np.loadtxt("./data/MFRS_Minitial%d.txt"%i,usecols=[1])
  rs = rs + r

rs = rs/N

plt.semilogy(J,rs,'-')
plt.show()
"""
J,R = np.loadtxt("AllProfiles/MFRS_Minitial_h0.txt",unpack=True)

a=30.0
b=0.424
c=95.0
guess = [a, b, c]
params, params_covariance = optimize.curve_fit(sigmoid, J, R, guess)
xr = np.arange(0,60,0.01)
y = sigmoid(xr,params[0],params[1],params[2])


N=1000
y2 = np.zeros((len(J),N))
for i in xrange(N):
  #y2[:,i] = R + 0.1*np.random.normal(size=len(J))
  y2[:,i] = R + 0.001*(1.0+ 2.0*np.random.rand(len(J)))

y3 = np.sum(y2,axis=1)/N
plt.figure(figsize=(16,8))
plt.plot(xr,y,'r-',label='Ajuste',lw=3.0)
plt.plot(J,y3,'-',lw=2,label='Datos promediados',color = '#B8860B',alpha=0.5)
#plt.semilogy(xr,y,'-')
#plt.semilogy(J,y3,'-',lw=0.1)
plt.ylim(ymax=110,ymin=0.0)
plt.xlabel("J",fontsize=15)
plt.ylabel("Firing Rate r(Hz)",fontsize=15)
leg = plt.legend(loc='best',shadow=True)
# set the linewidth of each legend object
for legobj in leg.legendHandles:
  legobj.set_linewidth(3.0)
#plt.plot(J,R,'r-')
plt.grid()
plt.savefig("AllProfiles/MFRS_Minitial_h0_averaged.pdf")
plt.show()
#"""



