#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
from mymodules import *
from Colors import *
import commands as cm
import scipy as sp
import scipy.linalg as la
import scipy.optimize as so
import scipy.stats as st
import argparse
import textwrap

from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GENERAL PARAMETERS OF PLOT
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mpl.rcParams['xtick.labelsize'] = 17
mpl.rcParams['ytick.labelsize'] = 17
mpl.rcParams['font.family'] = 'Freeserif' #cmr10
#mpl.rcParams['xtick.minor.size'] = 3
mpl.rcParams['font.size'] = 15.0



Ai = np.loadtxt("./Profiles/MatrixInitial.txt")

din,dout = GetDegree(Ai)
MaxD = din.max() if din.max()>dout.max() else dout.max()
Bins = np.arange(0.0,MaxD+1)

plt.figure(figsize=(12,10))

plt.subplot(2,1,1)
Nc,B,pt = plt.hist(din,Bins,histtype='stepfilled',label="In-Degree",color='red',alpha=0.4)
plt.legend()
plt.ylim(ymax=Nc.max()*1.1)
plt.xticks(np.arange(0,MaxD+5,5))
plt.title("In Degree Distribution")
plt.ylabel("Number of nodes")
plt.xlabel("Degree")
plt.tight_layout()

plt.subplot(2,1,2)
Nc,B,pt = plt.hist(dout,Bins,histtype='stepfilled',label="Out-Degree",color='Blue',alpha=0.4)
plt.legend()
plt.ylim(ymax=Nc.max()*1.1)
plt.xticks(np.arange(0,MaxD+5,5))
plt.title("Out Degree Distribution")
plt.ylabel("Number of nodes")
plt.xlabel("Degree")

plt.tight_layout()
plt.savefig("./Profiles/DegreeDist.pdf")