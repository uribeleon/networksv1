#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#=================================================================================================================
# initial files
#=================================================================================================================
#filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']
DirProfiles = Argv[1]
fileini = DirProfiles + "MatrixInitial.txt"
Ai = np.loadtxt(fileini)
Size = len(Ai)

folders = np.loadtxt(filenames[0],dtype=str)
profiles = np.loadtxt(filenames[2],dtype=str)
Nprofiles = len(folders)

#==================================================================================================
# GET AVERAGE SHORTEST PATH LENGTH FOR EACH PROFILE
#==================================================================================================
AvgShortPath = np.zeros(Nprofiles)

print"-> Creating initial graph with networkx..."
# Graph digraph-type in networkx
Di = nx.DiGraph(Ai)
# Get initial average short path
AvgSPL_random = nx.average_shortest_path_length(Di)*np.ones(Nprofiles)


## CALCULATION FOR EACH PROFILE
#for i in xrange(len(folders)):
  #Dir = DirProfiles + folders[i]
  #File = Dir + "/Matrix" + folders[i] + ".txt"
  #Af = np.loadtxt(File)
  #Df = nx.DiGraph(Af)
  #AvgShortPath[i]  = nx.average_shortest_path_length(Df)
  
##-----------------------------------------------------------------------------
## Saving file 
##-----------------------------------------------------------------------------
#print "-> Saving data..."
#data = np.array(zip(folders,AvgSPL_random,AvgShortPath),dtype=[('folders','S14'),
							       #('AvgSPL_random','float64'),
							       #('AvgShortPath','float64')])

#np.savetxt(DirProfiles + 'AvgShortPath-profiles.txt', data, fmt=["%15s"] + ["%15.10f",]*2)

#system("sort -n -k 2 %s -o %s"%(DirProfiles + 'AvgShortPath-profiles.txt',DirProfiles + 'AvgShortPath-Profiles-sorted.txt'))


