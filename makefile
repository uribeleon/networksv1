#===========================================================================
#  NEURAL NETWORK
#  Code to study complex networks
#
#  By: Cesar Alfredo Uribe Leon
#      alfredo.uribe@udea.edu.co
#      2015
#===========================================================================


#####################
# OPTIONS
#####################

# For testing the code
#OPT += -DPROBE

# To Create the adjacency matrix
#OPT += -DCREATEMATRIX

# To do rewiring
OPT += -DREWIRING

# Train one profile
OPT += -DONEPROFILE

# Save edges rewired
#OPT += -DSAVE_REW_HISTORY

# Train one profile
#OPT += -DSELECTEDPROFILES

# Train one profile
#OPT += -DSTATPROBE

# Train with tree-two swap
#OPT += -DTHREETWOSWAP


#-------------------------------------------------------
CC = g++

OPTIONS = $(OPT)

GSLPATH = /usr/local/include/

LFLAGS = -lm -L$(GSLPATH) -lgsl -lgslcblas 

CPPFLAGS = -g -c -Wall -Wextra  ${OPTIONS} -I$(GSLPATH) -I.

OBJ = main.o Allvars.o Util.o Allocation.o Network.o
OBJ2 = Allvars.o Util.o Allocation.o Network.o DPRewiring.o
OBJ3 = Allvars.o Util.o Allocation.o Network.o Three_Swap.o
OBJ4 = Allvars.o Util.o Allocation.o Network.o Three_Swap_regulated.o
OBJ5 = Allvars.o Util.o Allocation.o Network.o Two_Swap.o
OBJ6 = Allvars.o Util.o Allocation.o Network.o Three_Swap_ranked.o
OBJ7 = Allvars.o Util.o Allocation.o Network.o Three_Swap_rank_reg.o
OBJ8 = Allvars.o Util.o Allocation.o Network.o DynamicFunctions.o DynamicNetwork.o
OBJ9 = Allvars.o Util.o Allocation.o Network.o Two_Swap_ranked.o
OBJTEST = Allvars.o Util.o Allocation.o Network.o CalculateRho.o

INCL = Allvars.h Prototypes.h makefile

EXEC1 = main.x 
EXEC2 = DPRewiring.x
EXEC3 = Three_Swap.x
EXEC4 = Three_Swap_regulated.x
EXEC5 = Two_Swap.x
EXEC6 = Three_Swap_ranked.x
EXEC7 = Three_Swap_rank_reg.x
EXEC8 = DynamicNetwork.x
EXEC9 = Two_Swap_ranked.x
EXTEST = CalculateRho.x


###################
# BASIC RULES
###################

compile:${EXEC1}
	@echo "\nCOMPILING..." $^
	@echo "options" ${OPT}

compile-rewiring: ${EXEC2}

compile-3swap: ${EXEC3}

compile-3swap-regulated: ${EXEC4}

compile-2swap: ${EXEC5}

compile-3swap-ranked: ${EXEC6}

compile-3swap-rank-reg: ${EXEC7}

compile-TestAssort: ${EXTEST}

compile-Dynamic: ${EXEC8}

compile-2swap-ranked: ${EXEC9}

${EXEC1}:${OBJ}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ} ${LFLAGS} -o ${EXEC1}

${EXEC2}:${OBJ2}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ2} ${LFLAGS} -o ${EXEC2}

${EXEC3}:${OBJ3}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ3} ${LFLAGS} -o ${EXEC3}
	
${EXEC4}:${OBJ4}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ4} ${LFLAGS} -o ${EXEC4}
	
${EXEC5}:${OBJ5}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ5} ${LFLAGS} -o ${EXEC5}

${EXEC6}:${OBJ6}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ6} ${LFLAGS} -o ${EXEC6}

${EXEC7}:${OBJ7}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ7} ${LFLAGS} -o ${EXEC7}
	
${EXEC8}:${OBJ8}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ8} ${LFLAGS} -o ${EXEC8}

${EXEC9}:${OBJ9}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJ9} ${LFLAGS} -o ${EXEC9}
	
${EXTEST}:${OBJTEST}
	@echo "\n ---- Compiling ---- \n" $@
	$(CC) ${OBJTEST} ${LFLAGS} -o ${EXTEST}
	
${OBJ}:$(INCL)


# %.o:%.cpp
# 	@echo "\n ---- Creating .o files ----"
# 	$(CC) $< $(CFLAGS) -o $@


###################
# RUN RULES
###################
run:
	./${EXEC1}

runRewiring:
	./${EXEC2}

init:
	python python_codes/GeneratingNodesDegree.py

rewire:
	#python Rewiring.py
	python python_codes/MakeRewiring.py

viewplots:
	qpdfview ./Plots/*.pdf &

###########################
# CLEAN RULES
###########################

clean:
	@echo "Cleaning..."
	rm -rf *.out *.o *.x
	#rm -rf $(PROGS:.out=)
	#rm python_codes/*.pyc python_codes/*~
	#rm -rf Plots/*.pdf
	#rm -rf Plots/*
	#rm -rf *.dat
