/*
 * 
 * 
 * USAGE: 
 * 	For one profile       ->    ./DPRewiring.out ./data/condiciones_selected.txt 900000 "Delta[0]>0"
 * 			      ->     /DPRewiring.out 1000000 "Delta[0]>0 and Delta[3]<0" ./Probe-Profiles/Asortati.probe.txt ./Probe-Profiles/Matrix.txt
 * 	For several profiles  ->    ./DPRewiring.out 1000 ./data/condiciones_selected.txt ./data/names_selected.txt ./profiles/
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include "Allvars.h"
#include "Prototypes.h"
#include "conditions.cpp"


int main(int argc, char *argv[]){
  int i,j,training,nr;
  int EdgeR[3][3];
  int flag=0, ncond, mode, Nrewirings, EffRewirings=0;
  int *cij , *rw;
  int  resp;
  int a,b,c,d,e,f,bp,dp,fp;
  int condition[4];
  int cond_cpy[4];
  int idx_reg[4];
  int Delta[4];
  
  char cmd[100],*filename;
  char COND[80][256],NAMECOND[80][30],*cond,*cond2;
  char *pos;
  char *ofileassort;
  char *ofilematrix;
  
  double r,(*Rho)[4], terms[4], TMS[4][4];
//   double Delta[4];
  double rho[4];
  bool ans;
  double ft[4];
  
  FILE *ifile;
  FILE *ofAsort[NPROF],*ofMat,*ofPrm,*OFASORT;
  FILE *OFREW;
  
  //*
  //===================================================================================
  // Initialiting variables
  //===================================================================================
  
  filename =argv[1]; // File with initial matrix
  
  // Allocating general variables
  allocate_variables(filename);
//   allocate_variables("./data/MatrixInitial_500.txt");
  
  // Reading adjacency matrix
  ReadMatrix(filename);

  // Get number of conections
  Netwk.Nedges = GetNedges(Netwk.CIJ);
  
  // Allocating edge arrays
  for(i=0;i<NROWS;i++){
    Netwk.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); // 2 rows
    GV.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); 
  }

  // Get degrees of each node 
  GetDegreeNodes(Netwk.CIJ);

  // Get edges of network
  GetEdges(Netwk.CIJ,Netwk.edges);
  
  // Make copy of Edges
  CopyEdges(Netwk.edges,GV.edges,Netwk.Nedges);
  
  // Make copy of Adjacency Matrix
  CopyMatrix(Netwk.CIJ,GV.AIJ,Nc);
  
  // Determining the average rank of degrees
  get_averagerank(Netwk.indegree,Netwk.Nnodes, Netwk.inrnk);
  get_averagerank(Netwk.outdegree,Netwk.Nnodes, Netwk.outrnk);
 
  
//   Calculating initial assortativity
  for(mode=0;mode<4;mode++){
//     rho[mode] = SpearmanRho(Netwk.CIJ,mode,&TMS[mode][0],true);
    rho[mode] = Assortativity(Netwk.CIJ,mode,&TMS[mode][0],true);
//     ft[mode] = 1.0/(Netwk.Nedges*TMS[mode][SIGMJ]*TMS[mode][SIGMK]);
    printf("mode %d: %.12lf \n",mode,rho[mode]);
  }
  free_memory();
  //*/
}