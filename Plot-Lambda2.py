#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
#filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']

#profiles folder
DirProfiles = Argv[1]

#Reading name of profiles
NAME = np.loadtxt(filenames[0],dtype="|S14")


files = ["%sLambda2-Profiles-sorted.txt"%DirProfiles]

#"""
# Reading file of eigenvalues
print "="*50
print " Reading file of eigenvalues..."
Li,Lf = np.loadtxt(files[0],usecols=(1,2),unpack=True)
names = np.loadtxt(files[0], usecols=[0], dtype="|S14")
#EigVals = np.array(zip(names,Li,Lf),dtype=[('names','S14'),('Li','float64'),('Lf','float64')])
#"""


Size = len(NAME)



#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                                   PLOTING
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#===============================================
# Plot of eigenvalues
#===============================================
print " Begining with the plots...."
print "----> Plotting eigenvalues ..."
fig = plt.figure(figsize=(14,8))
#plt.axes((0,0.5,2,0.5))
plt.plot(Li,'go--',label="Before")
plt.plot(Lf,'co-',mec='b',label="After")
plt.ylabel("$\lambda_2$ of normalized laplacian matrix")
plt.xticks(range(Size),names,rotation=70,fontsize=10,ha='right')
plt.subplots_adjust(bottom=0.15)
plt.grid(color='k',linestyle='-.',alpha=0.5)
plt.legend(loc='upper left')
#plt.title("")
plt.savefig(DirProfiles+"PLOT-Lambda2.pdf",bbox_inches='tight')
plt.close()

print "="*50