import matplotlib.pyplot as plt
import numpy as np
import sys,os
import commands as cm

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv


FileIn = Argv[2] 
MatrixOut = Argv[3]
AssortOut = Argv[4]
Type = Argv[5]
Nrewirings = int(Argv[1])
condition = "Delta[0]>0"
#condition = "Delta[0]>0 and Delta[1]>0 and Delta[2]>0 and Delta[3]<0"
condition = "Delta[1]<0 and Delta[2]<0 and Delta[3]<0"


#===================================================================================
# COMPILATION OPTIONS
#===================================================================================
if Type == "3swap":
  cmd1 = "sed -i '/OPT += -DTHREETWOSWAP/c\#OPT += -DTHREETWOSWAP' makefile"
  #cmd2 = "sed -i -e 's/#OPT += -DTHREETWOSWAP/#OPT += -DTHREETWOSWAP/g' makefile"
  system(cmd1)
  #system(cmd2)
  system("make compile-3swap > output.log")
  system("clear")

if Type == "32swap":
  cmd = "sed -i '/OPT += -DTHREETWOSWAP/c\OPT += -DTHREETWOSWAP' makefile"
  system(cmd)
  system("make compile-3swap > output.log")
  system("clear")


#===================================================================================
# RUN CPP CODE
#===================================================================================
cmd = "./Three_Swap.x '%s' %d '%s' '%s' '%s' "%(FileIn,Nrewirings,condition,AssortOut,MatrixOut)
system(cmd)

#===================================================================================
# PLOT FINAL TRAININING
#===================================================================================
#Vals = np.loadtxt(AssortOut)
#rw = Vals[:,0]
#Rho = Vals[:,1:]

#Label = ["In-out","Out-In","In-In","Out-Out"]
#for mode in xrange(4):
  #plt.plot(rw,Rho[:,mode],'-',label=Label[mode])
#plt.xlabel("Number of rewirings")
#plt.ylabel("Assortativity")
#plt.title("with " + Type)
#plt.grid()
#plt.legend(loc="best")
##plt.show()


##===================================================================================
## PLOT ALL TRAININING
##===================================================================================
#Vals = np.loadtxt(AssortOut[:-5]+".txt")
#rw1 = Vals[:,0]
#Rho1 = Vals[:,1:]
#rw1 = rw[-1] + rw1
#for mode in xrange(4):
  #plt.plot(rw,Rho[:,mode],'-',label=Label[mode])
#for mode in xrange(4):
  #plt.plot(rw1,Rho1[:,mode],'-',label=Label[mode])
#plt.xlabel("Number of rewirings")
#plt.ylabel("Assortativity")
#plt.title("with " + Type)
#plt.grid()
#plt.legend(loc="best")
#plt.show()