from mymodules import *

Ai = np.loadtxt("./data/MatrixInitial_500.txt",dtype="int")
NR = 500000
Eff3swap = 0
r = np.zeros(4)
r2 = np.zeros(4)
rpearson = np.array([]).reshape(0,4)
rpearson2 = np.array([]).reshape(0,4)
rw = np.array([0])
rw2=rw.copy()
Terms = np.zeros((4,4)) 
Delta2 = np.zeros(4)
DeltaSave = np.array([]).reshape(0,4)
DeltaSave2 = np.array([]).reshape(0,4)
#condition = "Delta[0]>0 and Delta[1]>0 and Delta[2]>0"
condition = "Delta[0]>0"

# Degree of nodes:
InD,OutD = GetDegree(Ai)
Nedges = np.sum(InD)

#Initial assortativity
for mode in xrange(4):
  r[mode],Terms[mode] = Assortativity_Foster(Ai,mode,opt=True)
r2 = r.copy()

rpearson = np.append(rpearson,[r],axis=0)
rpearson2 = np.append(rpearson2,[r2],axis=0)
rw = np.append(rw,0)
rw2 = np.append(rw2,0)

#A,success, EdgesR = ThreeSwap(Ai)


for n in xrange(1,NR+1):
  A,success, EdgesR =ThreeSwap(Ai)
  if success:
    
    for mode in xrange(4):
      Delta2[mode] = DeltaR3swap(EdgesR,Nedges,InD,OutD,Terms[mode,:],mode)
      r[mode] = Assortativity_Foster(A,mode)
    
    Delta = r-rpearson[Eff3swap]
    DeltaSave = np.append(DeltaSave,[Delta],axis=0)
    DeltaSave2 = np.append(DeltaSave2,[Delta2],axis=0)
    if eval(condition) or "Delta2[0]>0":
      
      # Calculating the new assortativity
      for mode in xrange(4):
	r2[mode] = rpearson2[Eff3swap,mode] + Delta2[mode]
	
      rpearson = np.append(rpearson,[r],axis=0)
      rpearson2 = np.append(rpearson2,[r2],axis=0)
      Eff3swap+=1
      rw=np.append(rw,n)
      rw2=np.append(rw2,n)
  
  if (rpearson[Eff3swap,0]>1 or rpearson[Eff3swap,1]>1 or \
      rpearson[Eff3swap,2]>1 or rpearson[Eff3swap,3]>1):
    print "assortativity is greater than 1 "
    break
  
  sys.stdout.write("Progress: %.2f %%\r"%((100.0*n)/NR))
  if n>=NR:
    sys.stdout.write("\n")
  sys.stdout.flush()
  
np.savetxt("./3swap/MatrixFinal_3swap.txt",A)
np.savetxt("./3swap/Pearson3swap.txt",rpearson)
np.savetxt("./3swap/Pearson3swap.txt",rpearson2)
np.savetxt("./3swap/Deltas1.txt",DeltaSave)
np.savetxt("./3swap/Deltas2.txt",DeltaSave2)
print "Neffective: %d"%Eff3swap
plt.figure()
Label=["In-Out","Out-In","In-In","Out-Out"]
Label2 = ["In-Out 2","Out-In 2","In-In 2","Out-Out 2"]
for mode in xrange(4):
  plt.plot(rw,rpearson[:,mode],'-',label=Label[mode])
plt.xlabel("Number of rewiring")
plt.ylabel("Assortativity")
plt.title("With 3swap")
plt.legend(loc="best")
plt.savefig("3swapProbe.pdf")
