/*
 * 
 * 
 * USAGE: 
 * 	For one profile       ->    ./DPRewiring.out ./data/condiciones_selected.txt 900000 "Delta[0]>0"
 * 			      ->     /DPRewiring.out 1000000 "Delta[0]>0 and Delta[3]<0" ./Probe-Profiles/Asortati.probe.txt ./Probe-Profiles/Matrix.txt
 * 	For several profiles  ->    ./DPRewiring.out 1000 ./data/condiciones_selected.txt ./data/names_selected.txt ./profiles/
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include "Allvars.h"
#include "Prototypes.h"
#include "conditions.cpp"


int main(int argc, char *argv[]){
  int i,j,training,nr;
  int EdgeR[3][3];
  int flag=0, ncond, mode, Nrewirings, EffRewirings=0;
  int *cij , *rw;
  int  resp;
  int a,b,c,d,e,f,bp,dp,fp;
  int condition[4];
  int Delta[4];
//   int (*DeltaS)[4];
  
  char cmd[100],*filename;
  char COND[80][256],NAMECOND[80][30],*cond;
  char *pos;
  char *ofileassort;
  char *ofilematrix;
  
  double r,(*Rho)[4], terms[4], TMS[4][4];
//   double  Delta[4];
  double rho[4];
  bool ans;
  double ft[4];
  
  FILE *ifile;
  FILE *ofAsort[NPROF],*ofMat,*ofPrm,*OFASORT;
  
  //*
  //===================================================================================
  // Initialiting variables
  //===================================================================================
  
  filename =argv[1]; // File with initial matrix
  
  // Allocating general variables
  allocate_variables(filename);
//   allocate_variables("./data/MatrixInitial_500.txt");
  
  // Reading adjacency matrix
  ReadMatrix(filename);

  // Get number of conections
  Netwk.Nedges = GetNedges(Netwk.CIJ);
  
  // Allocating edge arrays
  for(i=0;i<NROWS;i++){
    Netwk.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); // 2 rows
    GV.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); 
  }
  

  // Get degrees of each node 
  GetDegreeNodes(Netwk.CIJ);

  // Get edges of network
  GetEdges(Netwk.CIJ,Netwk.edges);
  
  // Make copy of Edges
  CopyEdges(Netwk.edges,GV.edges,Netwk.Nedges);
  
  // Make copy of Adjacency Matrix
  CopyMatrix(Netwk.CIJ,GV.AIJ,Nc);
  
 
  //===================================================================================
  // Starting with Training
  //===================================================================================
  
  // Get number of rewirings
  Nrewirings = atoi(argv[2]);
  
  // Get condition
  cond = argv[3];
  split_condition(argv[3]," ",condition);
  
  //Get ofile of assortativity
  ofileassort = argv[4];
  
  //Get ofile of final Matrix
  ofilematrix = argv[5];
  
  // Allocating assortativity arrays
  Rho = (double (*)[4])malloc(Nrewirings*4*sizeof(double));
  cij = (int *)malloc(Nc*Nc*sizeof(int));
  rw = (int *)malloc(Nrewirings*sizeof(int));
//   DeltaS = (int (*)[4])malloc(Nrewirings*4*sizeof(int));
  printf("Initial variables allocated\n");
  
  //Calculating initial assortativity
  for(mode=0;mode<4;mode++){
    Rho[0][mode] = Assortativity(Netwk.CIJ,mode,&TMS[mode][0],true);
    ft[mode] = 1.0/(Netwk.Nedges*TMS[mode][SIGMJ]*TMS[mode][SIGMK]);
//     printf("%f \n",Rho[0][mode]);
  }
  
  printf("Initial r calculated\n");
  
  // 1. Doing rewiring for an specific condition
  for(nr=1;nr<Nrewirings;nr++){
    
    ThreeSwap(GV.AIJ, GV.edges,EdgeR,&flag);
    // 2. If Rewire was succesfull -> flag=1
    if(flag){

      // Calculating DeltaR
      for(mode=0;mode<4;mode++){
	Delta[mode] = DeltaR3swap_Int(&TMS[mode][0],EdgeR,mode);
      }
//       
      
      // 3. Verifiying if rewiring increment assortativity
//       if( EvalCondition(cond,Delta) ){
      if(evaluate_condition(Delta,condition)){
// 	CopyMatrix(GV.CPIJ,GV.AIJ,Nc)
	EffRewirings++;
	
	//Calculating new asortativity
	for(mode=0;mode<4;mode++){
	  Rho[EffRewirings][mode] = Rho[EffRewirings-1][mode] + ft[mode]*Delta[mode];
// 	  DeltaS[EffRewirings][mode] = Delta[mode];
// 	  Rho[EffRewirings][mode] = rho[mode];
	}
	// Get edges of network
	GetEdges(GV.AIJ,GV.edges);

	rw[EffRewirings] = nr;
      }
      else{
	a = EdgeR[SOURCE][0]; c = EdgeR[SOURCE][1]; e = EdgeR[SOURCE][2]; 
	b = EdgeR[TARGET][0]; d = EdgeR[TARGET][1]; f = EdgeR[TARGET][2]; 
	bp = EdgeR[NTARGET][0]; dp = EdgeR[NTARGET][1]; fp = EdgeR[NTARGET][2];
	
	// Restoring adjacency matrix
	GV.AIJ[a*Nc + b] = 1;   GV.AIJ[a*Nc + bp] = 0;
	GV.AIJ[c*Nc + d] = 1;   GV.AIJ[c*Nc + dp] = 0;
	GV.AIJ[e*Nc + f] = 1;   GV.AIJ[e*Nc + fp] = 0;
	
      }
    }
  }
  
  //Saving files
  OFASORT = fopen(ofileassort,"w");
  for(i=0;i<EffRewirings;i++)
    fprintf(OFASORT,"%d %lf %lf %lf %lf\n",rw[i],Rho[i][0],Rho[i][1],Rho[i][2],Rho[i][3]);
//   fwrite(&Rho[0][0],sizeof(double),4*EffRewirings,ofAsort);
  fclose(OFASORT);
  
  //Deltas
//   FILE *DELTAS;
//   DELTAS = fopen("./trash/Test3swap/Delta.txt","w");
//   for(i=0;i<EffRewirings;i++)
//     fprintf(DELTAS,"%d %d %d %d %d\n",rw[i],DeltaS[i][0],DeltaS[i][1],DeltaS[i][2],DeltaS[i][3]);
// //   fwrite(&Rho[0][0],sizeof(double),4*EffRewirings,ofAsort);
//   fclose(DELTAS);
  
  SaveMatrix(GV.AIJ,ofilematrix);

  printf("Terminado\n");
  //*/
}