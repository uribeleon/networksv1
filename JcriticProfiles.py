#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
from mymodules import *
from Colors import *
import commands as cm
import scipy as sp
import scipy.linalg as la
import scipy.optimize as so
import scipy.stats as st
import scipy.misc as ms
import argparse
import textwrap


import matplotlib as mpl
mpl.use('PDF')
mpl.style.use('classic')
from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter
from matplotlib.ticker import MultipleLocator, AutoMinorLocator, FormatStrFormatter

#***************************
# DEFINING PARSER 
#***************************
parser=argparse.ArgumentParser(
  prog='JcriticProfiles.py',
  formatter_class=argparse.RawDescriptionHelpFormatter,
  description=textwrap.dedent('''\
**********************************************************************************************************************
  Breve descripcion
    '''),epilog='''
**********************************************************************************************************************
  ''')

#***************************
# PARSER ARGUMENTS OPTIONS
#***************************
#parser.add_argument('option', action="store")
parser.add_argument('-if',dest='ifile',action="store",default=0,help="path to initial file",type=str)
parser.add_argument('-p',dest='pathDir',action="store",default=0,help="path of parent folder",type=str)
args=parser.parse_args()

#OPTION = args.option
PATHD = args.pathDir

#=======================
# SIGMOID FUNCTION
#=======================
def sigmoid(x,*Args):
  a,b,c,d = Args
  return c/(1.0 + np.exp( -b*(x-a) ) ) + d

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
argv = sys.argv


# Some parameter files
filenames = ['./data/names_selected.txt',
	     './data/condiciones_selected.txt',
	     './data/combinaciones_selected.txt']
filenames = ['./data/names.txt',
	     './data/condiciones.txt',
	     './data/combinaciones.txt']


#////////////////////////////////////////
# READING CONDITIONS OF EACH PROFILES
#////////////////////////////////////////
  
ParentDir = args.pathDir
NAME = list()
ifile2 = open(filenames[0],"r")

for line in ifile2:
  NAME.append(line.strip())
  
Nl = len(NAME)
Size = int(get("awk 'END {print NR}' %s"%(PATHD+"MatrixInitial.txt")))
Jcritic = np.zeros(Nl)
Sigma = np.zeros(Nl)
LABELFILE = """\
###########################################################
 PROFILE      JCRITIC       SIGMA       JcrO
###########################################################"""

#////////////////////////////////////////
# FITTING FOR INITIAL PROFILE
#////////////////////////////////////////

# Initial Guess
a=30.0   #10.0 	 
a=30.0   #10.0   
b=0.424   #2.0    
c=95.0   #100.0  
d=0.1   # 0.001 
guess = (a, b, c, d)

# Reading values of Firing rate for each profile
Jo,Ro = np.loadtxt("%sMFRS_Minitial_h0.txt"%(PATHD),unpack=True)


# Making fit of the data
params, params_covariance = so.curve_fit(sigmoid, Jo, Ro, guess)
a,b,c,d = params

Nsgm = 3 # delta in sigma
Nst = 8 #delta in sigma for stimulated study
Sigma0 = np.sqrt(2*np.pi**2/(6*b**2))
Joc = a - Nsgm*Sigma0


#////////////////////////////////////////
# FITTING FOR EACH PROFILE
#////////////////////////////////////////
for n in xrange(Nl):
  
  print "Profile --> %s     progress: %.2f %%"%(NAME[n],(100.0*(n+1))/(Nl))
  if NAME[n] != "_0_0_0_1":
    # Reading values of Firing rate for each profile
    J,R = np.loadtxt("%s%s/MFRS_vs_J-h0.dat"%(PATHD,NAME[n]),unpack=True)

    # Initial Guess
    a=30.0   #10.0   
    b=0.424   #2.0    
    c=95.0   #100.0  
    d=0.1   # 0.001 
    guess = (a, b, c, d)

    # Making fit of the data
    params, params_covariance = so.curve_fit(sigmoid, J, R, guess)
    a,b,c,d = params
    ARGS = (a,b,c,d)

    # calculating the theoretical curve with the results of fit
    xr = np.arange(0,40,0.01)
    y = sigmoid(xr,*ARGS)

    #calculating the numerical detivate of theoretical curve
    Yderiv = ms.derivative(sigmoid,xr,dx=1e-6,args=(a,b,c,d))
    #idx = np.where(Yderiv>0.1)
    #jcrit = xr[idx[0]]
    
    Sigma[n] = np.sqrt(2*np.pi**2/(6*b**2))
    Jcritic[n] = a - Nsgm*Sigma[n]

    #'''
    plt.figure()
    plt.plot(J,R,'-',color=CL["Goldenrod 1"],lw=2,label="Firing Rate")
    plt.plot(xr,y,'-',color=CL["Indianred"],lw=2,label="Fit",alpha=0.7)
    plt.axvline(x=Jcritic[n]+Nsgm*Sigma[n],ymin=0,ymax=100,color="b",lw=2,ls="--")
    plt.axvspan(xmin=a-Nsgm*Sigma[n],xmax=a+Nsgm*Sigma[n],ymax=100,color="b",alpha=0.2)
    #------------------------------------------------------------------------------------------------
    # annotation of jcritics
    plt.annotate("$J_{critic}$",xy=(Jcritic[n],1),xytext=(Jcritic[n]-Nst*Sigma[n],15),
                 xycoords='data',textcoords='data',size='18',horizontalalignment='right',
                 arrowprops=dict(arrowstyle="fancy", #linestyle="dashed",
                            color="0.4",
                            shrinkB=5,
                            connectionstyle="arc3,rad=-0.5",
                            fc=(1,0.4,0.4),
                            ec='r'
                            ),)
    # annotation of 15sigma
    plt.annotate("",xy=(Jcritic[n],50),xytext=(Jcritic[n]-Nst*Sigma[n],50),xycoords='data',textcoords='data',size='10',
                 arrowprops=dict(arrowstyle="|-|",lw=2))
    plt.annotate("",xy=(Jcritic[n],50),xytext=(Jcritic[n]-Nst*Sigma[n],50),xycoords='data',textcoords='data',size='18',
                 arrowprops=dict(arrowstyle="<->"))
    plt.text(Jcritic[n]-(Nst/2.)*Sigma[n], 54, '-$%d\sigma$'%Nst,fontsize='18',horizontalalignment='center')
    # annotation of 3sigma
    plt.annotate("",xy=(a,50),xytext=(a+Nsgm*Sigma[n],50),xycoords='data',textcoords='data',size='10',
                 arrowprops=dict(arrowstyle="|-|",ec='r',lw=2))
    plt.text(a+4*Sigma[n], 54, '$%d\sigma$'%Nsgm,fontsize='18',horizontalalignment='center')
    #------------------------------------------------------------------------------------------------
    plt.xlim(xmax=40)
    plt.xlabel("$J$",fontsize="15")
    plt.ylabel("% of active neurons",fontsize="15")
    plt.title("Profile: %s"%NAME[n])
    plt.xticks(range(0,45,5))
    plt.grid()
    plt.legend(loc="best")
    plt.savefig("%s%s/Sigmoid.pdf"%(PATHD,NAME[n]))
    #plt.close()
    plt.clf()

    #plt.figure()
    plt.plot(xr,Yderiv,'r-',lw =2)
    plt.axvline(x=Jcritic[n],ymin=0,ymax=100,color="b",lw=2,ls="--")
    plt.axvspan(xmin=a-Nsgm*Sigma[n],xmax=a+Nsgm*Sigma[n],ymax=100,color="b",alpha=0.3)
    plt.xlim(xmax=40)
    plt.xticks(range(0,45,5))
    plt.xlabel("J")
    plt.ylabel("Derivate of Sigmoid function")
    plt.title("Profile: %s"%NAME[n])
    plt.grid()
    plt.savefig("%s%s/SigmoidDeriv.pdf"%(PATHD,NAME[n]))
    plt.clf()
    #plt.close()
    #'''

# Save values in a table
J0C = np.ones(Nl)*Joc
JcProfiles = np.array(zip(NAME,Jcritic,Sigma,J0C),dtype=[('NAME','S14'),('Jcritic','float64'),('Sigma','float64'),('J0C','float64')])
np.savetxt("%sJcritics.txt"%PATHD,JcProfiles,fmt="%12s %12f %12f %12f",header=LABELFILE)

#--------------------------------------
# PLOT JCRITIC vs PROFILES (ORDERED)
#--------------------------------------
JcProfiles.sort(order='Jcritic')
index = [ i for i in xrange(len(JcProfiles['NAME'])) if JcProfiles['NAME'][i] in NAME]

#majorLocator = MultipleLocator(20) #step between ticks
minorLocator = MultipleLocator(5) #number of minor ticks between ticks

fig = plt.figure(figsize=(10,6))
ax=fig.add_subplot(111)
plt.plot(range(1,Nl),JcProfiles['Jcritic'][index[1:]],'go--')
plt.axhline(y=Joc,xmax=Nl-1,label="$J_{0,critic} = %.3f$"%Joc)
plt.ylabel("Jcritic")
plt.xticks(range(Nl),JcProfiles['NAME'][index],rotation=70,fontsize=8,ha='right')
##plt.axes().yaxis.set_minor_locator(minorLocator)
ax.yaxis.set_minor_locator(AutoMinorLocator(5))
plt.grid(which="major",color='k',linestyle=':',lw=0.1,alpha=0.5)
#plt.minorticks_on()
plt.grid(which="minor",axis='y',color='k',linestyle=':',lw=0.1,alpha=0.5)
plt.legend(loc="best")
plt.savefig("%sJcriticVsProfiles.pdf"%PATHD,bbox_inches='tight')
#plt.close()

  #ofile.write("%10s %12f %12f\n"%(NAME[n],Jcritic[n],))
