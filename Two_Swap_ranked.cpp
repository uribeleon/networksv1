/*
 *
 *
 * USAGE:
 * 	For one profile       ->    ./DPRewiring.out ./data/condiciones_selected.txt 900000 "Delta[0]>0"
 * 			      ->     /DPRewiring.out inifile 1000000 "Delta[0]>0 and Delta[3]<0" ./Probe-Profiles/Asortati.probe.txt ./Probe-Profiles/Matrix.txt
 * 	For several profiles  ->    ./DPRewiring.out 1000 ./data/condiciones_selected.txt ./data/names_selected.txt ./profiles/
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include "Allvars.h"
#include "Prototypes.h"
#include "conditions.cpp"

int main(int argc, char *argv[]){

  int i,j,training,nr;
  int EdgeR[4];
  int flag=0, ncond, mode, Nrewirings, EffRewirings=0;
  int condition[4];
  int Delta[4];
  int (*DeltaS)[4];

  char cmd[100],*filename1,*filename2,*namedir,*inifile;
  char COND[80][256],NAMECOND[80][30],*cond;
  char *pos;

  double r,(*Rho)[4], terms[4], TMS[4][4];
  double rho[4];
  double ft[4];
//   double Delta[4];
  bool ans;

  int *cij , *rw;
  int  resp;


  FILE *ifile;
  FILE *ofAsort[NPROF],*ofMat,*ofPrm,*OFASORT;


  //===================================================================================
  // Initialiting variables
  //===================================================================================

  inifile = argv[1];  // File with initial matrix
  printf("%s\n",inifile);

  // Allocating general variables
  allocate_variables(inifile);

  // Reading adjacency matrix
  ReadMatrix(inifile);

  // Get number of conections
  Netwk.Nedges = GetNedges(Netwk.CIJ);

  // Allocating edge arrays
  for(i=0;i<NROWS;i++){
    Netwk.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); // 2 rows
    GV.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int));
  }


  // Get degrees of each node
  GetDegreeNodes(Netwk.CIJ);

  // Get edges of network
  GetEdges(Netwk.CIJ,Netwk.edges);

  // Make copy of Edges
//   CopyEdges(Netwk.edges,GV.edges,Netwk.Nedges);

  // Make copy of Adjacency Matrix
  CopyMatrix(Netwk.CIJ,GV.AIJ,Nc);

  GetEdges(GV.AIJ,GV.edges);
  //===================================================================================
  // Starting with Rewiring
  //===================================================================================

//-----------------------------------------------------------------
// TRAINING ONE PROFILE
//-----------------------------------------------------------------
#ifdef ONEPROFILE

  char *ofileassort;
  char *ofilematrix;

  // Get number of rewirings
  Nrewirings = atoi(argv[2]);

  // Get condition
  cond = argv[3];
  split_condition(argv[3]," ",condition);

  //Get ofile of assortativity
  ofileassort = argv[4];

  //Get ofile of final Matrix
  ofilematrix = argv[5];

  // Allocating assortativity arrays
  Rho = (double (*)[4])malloc(Nrewirings*4*sizeof(double));
  DeltaS = (int (*)[4])malloc(Nrewirings*4*sizeof(int));
  cij = (int *)malloc(Nc*Nc*sizeof(int));
  rw = (int *)malloc(Nrewirings*sizeof(int));

  // Determining the average rank of degrees
  get_averagerank(Netwk.indegree,Netwk.Nnodes, Netwk.inrnk);
  get_averagerank(Netwk.outdegree,Netwk.Nnodes, Netwk.outrnk);

#if(1)
  //Calculating initial assortativity
  for(mode=0;mode<4;mode++){
//     Rho[0][mode] = Assortativity(Netwk.CIJ,mode,&TMS[mode][0],true);
    Rho[0][mode] = SpearmanRho(Netwk.CIJ,mode,&TMS[mode][0],true);
    ft[mode] = 1.0/(Netwk.Nedges*TMS[mode][SIGMJ]*TMS[mode][SIGMK]);
    printf("%f \n",Rho[0][mode]);
  }


  // 1. Doing rewiring for an specific condition
  for(nr=1;nr<Nrewirings;nr++){


    Rewire(GV.AIJ,GV.edges,EdgeR,&flag);

    // 2. If Rewire was succesfull -> flag=1
    if(flag){

      // Calculating DeltaR
      for(mode=0;mode<4;mode++){
// 	       Delta[mode] = DeltaR_int(EdgeR,mode);
// 	Delta[mode] = DeltaR(&TMS[mode][0],EdgeR,mode);
//  	rho[mode] = Assortativity(GV.AIJ,mode,&TMS[mode][0],false);
//  	Delta[mode] = rho[mode] - Rho[EffRewirings][mode];
	Delta[mode] = DeltaR_ranked_Int(&TMS[mode][0],EdgeR,mode);
      }
#if(1)
      // 3. Verifiying if rewiring increment assortativity
//       if( EvalCondition(cond,Delta) ){
//       printf("%d\n",evaluate_condition(Delta,condition));
      if(evaluate_condition(Delta,condition)){
          EffRewirings++;
          //Calculating new asortativity
          for(mode=0;mode<4;mode++){
              Rho[EffRewirings][mode] = Rho[EffRewirings-1][mode] + ft[mode]*Delta[mode];
              DeltaS[EffRewirings][mode] = Delta[mode];
              //Rho[EffRewirings][mode] = rho[mode];
          }
          // Get edges of network
          GetEdges(GV.AIJ,GV.edges);
          rw[EffRewirings] = nr;
      }
      else{
          // Restoring adjacency matrix
          GV.AIJ[EdgeR[0]*Nc + EdgeR[1]] = 1;   GV.AIJ[EdgeR[0]*Nc + EdgeR[3]] = 0;
          GV.AIJ[EdgeR[2]*Nc + EdgeR[3]] = 1;   GV.AIJ[EdgeR[2]*Nc + EdgeR[1]] = 0;
      }

#endif
    }
  }

  //Saving files
  //======================================================================================

  OFASORT = fopen(ofileassort,"w");
  for(i=0;i<EffRewirings;i++)
    fprintf(OFASORT,"%d %lf %lf %lf %lf\n",rw[i],Rho[i][0],Rho[i][1],Rho[i][2],Rho[i][3]);
//   fwrite(&Rho[0][0],sizeof(double),4*EffRewirings,ofAsort);
  fclose(OFASORT);

  //Deltas
//   FILE *DELTAS;
//   DELTAS = fopen("./trash/Test2swap/Delta.txt","w");
//   for(i=0;i<EffRewirings;i++)
//     fprintf(DELTAS,"%d %d %d %d %d\n",rw[i],DeltaS[i][0],DeltaS[i][1],DeltaS[i][2],DeltaS[i][3]);
// //   fwrite(&Rho[0][0],sizeof(double),4*EffRewirings,ofAsort);
//   fclose(DELTAS);

  // Saving matrix
  SaveMatrix(GV.AIJ,ofilematrix);
  /*ofPrm = fopen("./data/OutPrm.txt","w");
  fprintf(ofPrm,"%d\n",EffRewirings);
  fclose(ofPrm)*/;
#endif
#endif

  free(rw);
  free(Rho);
  free_memory();

//   for(i=0;i<NROWS;i++){
//     free(Netwk.edges[i]);
//   }

//   for(i=0;i<NROWS;i++){
//     free(GV.edges[i]);
//   }


}
