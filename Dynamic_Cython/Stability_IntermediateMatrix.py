import sys,os
import commands as cm
import stability as stb
import matplotlib.pyplot as plt
import numpy as np
import time
from Colors import *
from Functions_Stability import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv = sys.argv

PRM = {"EdgesFile":Argv[1],
       "IniMatrix":Argv[2],
       "Nmatrices": int(Argv[3])}

STBPRM = {"pc":0.1,
	  "Dt":10E-3,
	  "Ntimes": 50,
	  "Nr": 30,
	  "r0":1.0,
	  "NiActv":5,
	  "ncol":10,
	  "ofname":Argv[4],
	  "filename":" "}


# Reading Edges Edges rewired 
rw,a,c,e,b,d,f,bp,dp,fp = np.loadtxt(PRM["EdgesFile"],unpack=True,dtype="int")

# Get number of effective rewirings
Nrew = len(a)
#EffRew = int(get(" wc -l ../Prof3swapN100/_-1_1_-1_1/EdgesRewired.txt | awk '{print $1}' "))

# Get delta of each blocks
Nblocks = int(Nrew/PRM['Nmatrices'])


# Reading Initial Matrix
Ai = np.loadtxt(PRM['IniMatrix'],dtype=np.int)
Af = Ai.copy()

#Defining Initial variables
J = np.arange(0,18,0.1)
Rprom = np.zeros_like(J)
Jc = []
Sgm = []
Rw = []

# Initial Guess to fit
ai=10.0
bi=2.0
ci=100.0
di= 0.001
guess = (ai,bi,ci,di)

stb.DynamicStabilityOneP(Af,STBPRM,J,Rprom)
output = GetJcritic(J,Rprom,guess)
Jc.append(output[0])
Sgm.append(output[1])


CountMtrx = 0
for i in xrange(Nrew):
  
  # Reconstructing the intermediate matrices
  Af[ a[i] , bp[i] ] = 1;   Af[ a[i] , b[i] ] = 0;
  Af[ c[i] , dp[i] ] = 1;   Af[ c[i] , d[i] ] = 0;
  Af[ e[i] , fp[i] ] = 1;   Af[ e[i] , f[i] ] = 0;
  
  # Verifying location to get the matrix and
  # study the stability
  if i%Nblocks == 0 and i>0:
    CountMtrx += 1
    stb.DynamicStabilityOneP(Af,STBPRM,J,Rprom)
    output = GetJcritic(J,Rprom,guess)
    Jc.append(output[0])
    Sgm.append(output[1])
    Rw.append(rw[i])
    print "Process: ", (100.0*CountMtrx)/PRM['Nmatrices']


LABELFILE = """\
###########################################################
 No REWIRING      JCRITIC       SIGMA
###########################################################"""  

JcMtrxIntrm = np.array(zip(Rw,Jc,Sgm),dtype=[('rw','int32'),('Jc','float64'),('Sgm','float64')])
np.savetxt("%s/Jcritics_MtxIntrm.txt"%STBPRM['ofname'],JcMtrxIntrm,fmt="%12d %12f %12f",header=LABELFILE)  
  


