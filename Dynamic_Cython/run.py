import sys,os
import commands as cm
import matplotlib as mpl
mpl.use('PDF')
import stability as stb
import matplotlib.pyplot as plt
import numpy as np
from Colors import *
import time

mpl.style.use('classic')

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv = sys.argv


it=1
OPTION = Argv[it]

#===============================================================================================
# TRAINING ONE PROFILE
#===============================================================================================
if OPTION == "ONEP":
  Parameters = {"pc":0.1,
		"Dt":10E-3,
		"Ntimes": 200,
		"Nr": 30,
		"r0":1.0,
		"NiActv":5,
		"ncol":10,
		"filename":sys.argv[it+1],
		"ofname":sys.argv[it+2],
		"savefile":True,
		"showplot":False,
        "saveplot":True}


  J = np.arange(0,45,0.2)
  Rprom = np.zeros_like(J)
  start_time = time.time()
  AI = np.loadtxt(Parameters["filename"],dtype=np.int)
  #Rprom = stb.DynamicStabilityOneP(Parameters,J)
  stb.DynamicStabilityOneP(AI,Parameters,J,Rprom)
  #Rprom = pystb.Stability(Parameters,J)
  print("--- %s seconds ---" % (time.time() - start_time))

  if Parameters["savefile"]:
    np.savetxt("%s/MFRS_vs_J-h0.dat"%(Parameters["ofname"]),np.array([J,Rprom]).T,fmt="%10f")
  if Parameters["showplot"]:
    plt.figure()
    #plt.semilogy(Ji,Ri,'r-',label='Initial: $r_0 = %d$ Hz'%r0[i])
    plt.semilogy(J,Rprom,'-',color=CL['Darkgoldenrod 1'],label = 'final: $r_0 = %d$ Hz'%Parameters["r0"])
    plt.ylim(ymax=150)
    plt.grid(which='both')
    plt.xlabel("J",fontsize=15)
    plt.ylabel("Firing Rate r(Hz)",fontsize=15)
    plt.show()
  if Parameters["saveplot"]:
      ParentDir = Parameters["filename"].split("/")
      NAME = ParentDir[-2].replace("_"," ")
      ParentDir = "/".join(ParentDir[:-2]) + "/"
      # Loading firing rate of random matrix ---------------------
      Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)
      #*************************************
      # Plot stability curves
      #*************************************
      plt.figure()
      plt.semilogy(Ji,Ri,'r-',label='Initial: $r_0 = %d$ Hz'%1.0)
      plt.semilogy(J,Rprom,'-',color=CL['Darkgoldenrod 1'],label = 'final: $r_0 = %d$ Hz'%1.0)
      plt.ylim(ymax=150)
      plt.grid(which='both')
      plt.xlabel("J",fontsize=15)
      plt.ylabel("% of active neurons",fontsize=15)

      leg = plt.legend(loc='best',shadow=True)
      # set the linewidth of each legend object
      for legobj in leg.legendHandles:
          legobj.set_linewidth(2.0)

      plt.title("profile %s"%NAME)
      plt.savefig("%s/MFRS_vs_J-h%d.pdf"%(Parameters["ofname"],0))
      plt.close()

#stb.main(sys.argv[1])

#===============================================================================================
# TRAINING ALL PROFILES
#===============================================================================================
if OPTION == "ALLP":

  #////////////////////////////////////////
  # DEFINING INITIAL PARAMETER
  #////////////////////////////////////////
  r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
  idx = 0

  STBPRM = {"pc":0.1,
	    "Dt":10E-3,
	    "Ntimes": 200,
	    "Nr": 20,
	    "r0":r0[idx],
	    "NiActv":5,
	    "ncol":10}

  GRPRM = {"ParentDir": Argv[it+1],"savefile":True,"showplot":False}

  J = np.arange(0,45,0.2)

  filenames = ['../data/names.txt',
	     '../data/condiciones.txt',
	     '../data/combinaciones.txt']



  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////

  ParentDir = GRPRM["ParentDir"]
  NAME = list()
  ifile2 = open(filenames[0],"r")

  for line in ifile2:
    NAME.append(line.strip())

  Nl = len(NAME)
  Size = int(get("awk 'END {print NR}' %s"%(ParentDir+"MatrixInitial.txt")))

  #//////////////////////////////////////////////
  # Reading mean firing rate for initial matrix
  #//////////////////////////////////////////////
  Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)

  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  for n in xrange(Nl):
    ifname = ParentDir + NAME[n] + "/Matrix" + NAME[n] + ".txt"
    ofname = ParentDir + NAME[n]
    Af = np.loadtxt(ifname,dtype="int")
    rprom = np.zeros_like(J)
    sys.stdout.write("Dynamic profile --> %s     progress: %.2f %%"%(NAME[n],(100.0*(n+1))/(Nl)))
    sys.stdout.flush()

    #-----------------------------------
    # Determining stability curve
    #-----------------------------------
    stb.DynamicStabilityOneP(Af,STBPRM,J,rprom)

    np.savetxt("%s/MFRS_vs_J-h%d.dat"%(ofname,idx),np.array([J,rprom]).T,fmt="%10f")

    #*************************************
    # Plot stability curves
    #*************************************
    plt.figure()
    plt.semilogy(Ji,Ri,'r-',label='Initial: $r_0 = %d$ Hz'%r0[idx])
    plt.semilogy(J,rprom,'-',color=CL['Darkgoldenrod 1'],label = 'final: $r_0 = %d$ Hz'%r0[idx])
    plt.ylim(ymax=150)
    plt.grid(which='both')
    plt.xlabel("J",fontsize=15)
    plt.ylabel("Firing Rate r(Hz)",fontsize=15)

    leg = plt.legend(loc='best',shadow=True)
    # set the linewidth of each legend object
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)

    plt.title("profile %s"%NAME[n].replace("_"," "))
    plt.savefig("%s/MFRS_vs_J-h%d.pdf"%(ofname,idx))
    plt.close()

#===============================================================================================
# TRAINING SELECTED PROFILES
#===============================================================================================
if OPTION == "SELECTP":

  #////////////////////////////////////////
  # DEFINING INITIAL PARAMETER
  #////////////////////////////////////////
  r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
  idx = 0

  STBPRM = {"pc":0.1,
	    "Dt":10E-3,
	    "Ntimes": 200,
	    "Nr": 5,
	    "r0":r0[idx],
	    "NiActv":5,
	    "ncol":10}

  GRPRM = {"ParentDir": Argv[it+1],"savefile":True,"showplot":False}

  J = np.arange(0,45,0.2)


  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////

  ParentDir = GRPRM["ParentDir"]

  Nfiles = int(get("ls %sAssort* | wc -l"%ParentDir))
  name = ParentDir.split("/")[-2]

  #//////////////////////////////////////////////
  # Reading mean firing rate for initial matrix
  #//////////////////////////////////////////////
  #Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)

  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  for n in xrange(Nfiles):

    ifname = ParentDir + "Matrix" + name + "_V%d.txt"%n
    ofname = ParentDir
    Af = np.loadtxt(ifname,dtype="int")
    rprom = np.zeros_like(J)
    sys.stdout.write("Dynamic profile --> %s     progress: %.2f %%"%(n,(100.0*(n+1))/(Nfiles)))
    sys.stdout.flush()

    #-----------------------------------
    # Determining stability curve
    #-----------------------------------
    stb.DynamicStabilityOneP(Af,STBPRM,J,rprom)

    np.savetxt("%s/MFRS_vs_J-h%d_V%d.dat"%(ofname,idx,n),np.array([J,rprom]).T,fmt="%10f")

    """
    #*************************************
    # Plot stability curves
    #*************************************
    plt.figure()
    plt.semilogy(Ji,Ri,'r-',label='Initial: $r_0 = %d$ Hz'%r0[idx])
    plt.semilogy(J,rprom,'-',color=CL['Darkgoldenrod 1'],label = 'final: $r_0 = %d$ Hz'%r0[idx])
    plt.ylim(ymax=150)
    plt.grid(which='both')
    plt.xlabel("J",fontsize=15)
    plt.ylabel("Firing Rate r(Hz)",fontsize=15)

    leg = plt.legend(loc='best',shadow=True)
    # set the linewidth of each legend object
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)

    plt.title("profile %s"%NAME[n].replace("_"," "))
    plt.savefig("%s/MFRS_vs_J-h%d.pdf"%(ofname,idx))
    plt.close()
    """
