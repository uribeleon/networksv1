import sys,os
import commands as cm
import matplotlib as mpl
#mpl.use('PDF')
import sensibility as stb
import matplotlib.pyplot as plt
import numpy as np
from Colors import *
from scipy import stats
import time
from dynmod import *

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv = sys.argv
arr = np.array

#==============
# FUNCTIONS
#==============
def GetDegree(CIJ):
  indeg = np.sum(CIJ,axis=0) # columns
  outdeg = np.sum(CIJ,axis=1) # rows
  return indeg,outdeg


it=1
OPTION = Argv[1]

#===============================================================================================
# TRAINING ONE PROFILE
#===============================================================================================
if OPTION == "ONEP":
  Parameters = {"pc":0.1,
		"Dt":10E-3,
		"Nr": 1,
        "Nrep":20,
		"r0":1.0,
		"NiActv":5,
		"Nstim": 4,
		"ncol":10,
		"Tstb":30,
		"Tsp":100,
		"Tst":100,
        "J0": 13,#21.590752000000002,# FOR PROFILE -1 0 0 0  .... 15.065261999999999,#8.194968
		"filename":sys.argv[2],
		"ofname":sys.argv[3],
		"savefile":True,
		"showplot":True,
        "saveplot":False}

  # parameter for boostrapping
  PrmBst = {"nsample":100000,
        "alpha":95,
        "do-boostrp":True}

  # CONDITION TO SAVE OR SHOW PLOT
  if Parameters["saveplot"]:
      mpl.use('PDF')

  ntimes = Parameters['Tstb'] + Parameters['Tsp'] + Parameters['Tst']
  time = np.arange(0,ntimes)
  J0 = Parameters['J0']
  # start_time = time.time()
  AI = np.loadtxt(Parameters["filename"],dtype=np.int)
  nactives = np.zeros(ntimes)
  Size = len(AI)
  #NAME =  Parameters['ofname'].split("/")[-2]
  NAME = 'Probe'
  # Neurons stimulated
  din,dout = GetDegree(AI)
  idx = np.argsort(dout)
  IDX = idx[-Parameters['Nstim']:] #for neurons with highest dout
  nrs_nonstimulated = np.ones(len(AI),dtype="bool")
  nrs_nonstimulated[IDX] = False
  # nrs_nonstimulated = np.ones(len(AI),dtype="bool")
  # nrs_nonstimulated[:6] = False

  Tsp_min = Parameters['Tstb']
  Tsp_max = Tsp_min + Parameters['Tsp']
  Tst_min = Tsp_max

  # Variables for boostrapping ----------------------------
  meansp_arr = np.zeros(Parameters['Nrep'])
  meanst_arr = np.zeros(Parameters['Nrep'])

  #  %%%%%%%%%%%%%%%%%%%%%%%%%% LOOP FOR TO CREATE SAMPLE NEEDED FOR BOOSTSTRAP METHOD  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  for irep in xrange(Parameters['Nrep']):
      nactives = np.zeros(ntimes)
      stb.DynamicSensibilityOneP(AI,Parameters,J0,nrs_nonstimulated,nactives) #23.374943

      #Rprom = pystb.Stability(Parameters,J)
      nactives_spontaneous = nactives[Tsp_min:Tsp_max]
      nactives_stimulated = nactives[Tsp_max:]
      # print("--- %s seconds ---" % (time.time() - start_time))

      # Plotting the histogram -----------------------------------------------------------------------------------------
      NcountSp = np.histogram(nactives_spontaneous,bins=range(len(AI)))[0]
      NcountSt = np.histogram(nactives_spontaneous,bins=range(len(AI)))[0]
      #NcountSp = np.histogram(nactives_spontaneous,bins=100)[0]
      #NcountSt = np.histogram(nactives_stimulated,bins=100)[0]
    #   NcountSp = plt.hist(nactives_spontaneous,bins=range(len(AI)),histtype='stepfilled',label='spontaneous',alpha=0.4,normed=True, color='g')[0]
    #   NcountSt = plt.hist(nactives_stimulated,bins=range(len(AI)),histtype='stepfilled',label='stimulated',alpha=0.4,normed=True , color='b')[0]

      # Determining the xmax -------------------------
      argXmaxSp = np.argwhere(NcountSp>0)[-1,-1]
      argXmaxSt = np.argwhere(NcountSt>0)[-1,-1]
      argxmax = argXmaxSp + 4 if argXmaxSp > argXmaxSt else argXmaxSt + 4
      # Determining the xmin -------------------------
      argXminSp = np.argwhere(NcountSp>0)[0,0]
      argXminSt = np.argwhere(NcountSt>0)[0,0]
      argxmin = argXminSp if argXminSp < argXminSt else argXminSt
      argxmin = argxmin - 4 if argxmin - 4>=0 else 0

      # Fit to gaussian distribution -----------------------------------------------------------------------------------

      # Determining the zone where the number of active nodes is less than the 30% of total nodes
      nspontaneous = nactives_spontaneous[ nactives_spontaneous < 0.3*Size ]
      nstimulated = nactives_stimulated[ nactives_stimulated < 0.3*Size ]
      try:
          xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2
      except ValueError:
          nspontaneous = nactives_spontaneous.copy()
          nstimulated = nactives_stimulated.copy()
          xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2

      xsp = np.linspace(argxmin, xmax, len(nspontaneous)) # spontaneous
      xst = np.linspace(argxmin, xmax, len(nstimulated)) # stimulated
      # xst = np.linspace(argxmin, argxmax, len(nactives_stimulated)) # stimulated
      mean_sp, sigma_sp = stats.norm.fit(nspontaneous) # get mean and standard deviation
      mean_st, sigma_st = stats.norm.fit(nstimulated) # get mean and standard deviation
      # mean_st, sigma_st = stats.norm.fit(nactives_stimulated) # get mean and standard deviation
      meansp_arr[irep] = mean_sp
      meanst_arr[irep] = mean_st



      pdfSp = stats.norm.pdf(xsp, mean_sp, sigma_sp) # get theoretical values in our interval
      pdfSt = stats.norm.pdf(xst, mean_st, sigma_st) # get theoretical values in our interval


      #plt.plot(,np.max(pdfSt),'o',color='black')
      #print np.mean(pdfSt),np.max(pdfSt)


  # END FOR LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  # Plotting the fit -----------------------------------------------------------------------------------------
  plt.plot(xsp, pdfSp,'-',color='g',lw=1.3)
  #plt.plot(xsp[np.argmax(pdfSp)],np.max(pdfSp),'o',color='black')
  plt.plot(xst, pdfSt,'-',color='b',lw=1.3)

  # MAKING BOOSTRAPPING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  # >>> spontaneous
  meansp_err_min, meansp_err_max, meansp_err = bootstrap(meansp_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
  stdsp_err_min, stdsp_err_max, stdsp_err = bootstrap(meansp_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

  # >>> stimulated
  meanst_err_min, meanst_err_max, meanst_err = bootstrap(meanst_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
  stdst_err_min, stdst_err_max, stdst_err = bootstrap(meanst_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

  # >>> Differences between means
  diffmean_err_min,diffmean_err_max,diffmean_err = bootstrap(meanst_arr-meansp_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
  diffstd_err_min,diffstd_err_max,diffstd_err = bootstrap(meanst_arr-meansp_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])



  # Plotting the histogram
  NcountSp = plt.hist(nactives_spontaneous,bins=range(len(AI)),histtype='stepfilled',label='spontaneous',alpha=0.4,normed=True, color='g')[0]
  NcountSt = plt.hist(nactives_stimulated,bins=range(len(AI)),histtype='stepfilled',label='stimulated',alpha=0.4,normed=True , color='b')[0]

  # Plotting error BAR
  plt.errorbar(meansp_err,np.max(pdfSp)*1.1,xerr=3*stdsp_err,fmt='o',ms=10,mfc='k',mec='k',
                                            elinewidth=3,ecolor='k',capthick=3,capsize=5)
  # error = np.row_stack((meansp_err - meansp_err_min,meansp_err_max - meansp_err ))
  #print meansp_err,error
  # plt.errorbar(meansp_err,np.max(pdfSp)*1.2,xerr=error,fmt='o',ms=5,mfc='r',mec='k',
                                            # elinewidth=3,ecolor='k',capthick=3,capsize=5)
  plt.errorbar(meanst_err,np.max(pdfSt)*1.1,xerr=3*stdst_err,fmt='o',ms=10,mfc='k',mec='k',
                                            elinewidth=3,ecolor='k',capthick=3,capsize=5)
  #plt.errorbar(np.mean(meansp_arr),np.max(pdfSp)*1.2,xerr=3*np.std(meansp_arr),fmt='o',ms=10,mfc='g',
#                                          elinewidth=3,ecolor='g',capthick=3,capsize=5)
  #plt.errorbar(np.mean(meanst_arr),np.max(pdfSt)*1.2,xerr=3*np.std(meanst_arr),fmt='o',ms=10,mfc='b',
#                                          elinewidth=3,ecolor='b',capthick=3,capsize=5)
  #plt.plot(meansp_err,np.max(pdfSp)*1.1,'ro')

  plt.xlim(xmax=xmax,xmin=argxmin)
  plt.grid(which='both')
  plt.xlabel("Active Neurons",fontsize=15)
  plt.ylabel("Counts",fontsize=15)
  plt.title("profile %s"%NAME.replace("_"," "))
  leg = plt.legend(loc='best',shadow=True)


  print "meansp meanst  sigma_sp sigma_st "
  print "%10f %10f %10f %10f"  %(mean_sp,mean_st,sigma_sp,sigma_st)
  # plt.show()


  if Parameters["savefile"]:
      np.savetxt("%s/NneuronsMean_spontaneous.dat"%Parameters["ofname"],np.array([time[Tsp_min:Tsp_max],nactives_spontaneous]).T,fmt="%10f")
      np.savetxt("%s/NneuronsMean_stimulated.dat"%Parameters["ofname"],np.array([time[Tsp_max:],nactives_stimulated]).T,fmt="%10f")
    # np.savetxt("%s/MFRS_vs_J-h0.dat"%(Parameters["ofname"]),np.array([J,Rprom]).T,fmt="%10f")
  if Parameters["saveplot"]:
    pathplot = sys.argv[it+3]
    plt.savefig(Parameters['ofname'] + pathplot)

  plt.show()
#stb.main(sys.argv[1])



#===============================================================================================
# TRAINING ALL PROFILES
#===============================================================================================
if OPTION == "ALLP":

  #////////////////////////////////////////
  # DEFINING INITIAL PARAMETER
  #////////////////////////////////////////
  r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
  idx = 0
  DsgmJ = 15 #Delta SigmaJ

  SENSPRM = {"pc":0.1,
		"Dt":10E-3,
		"Nr": 20,
        "Nrep":10,
		"r0":1.0,
		"NiActv":5,
		"Nstim": 5,
		"ncol":10,
		"Tstb":50,
		"Tsp":200,
		"Tst": 200}


  GRPRM = {"ParentDir": Argv[it+1],"savefile":True,"showplot":False}

  # J = np.arange(0,45,0.2)

  filenames = ['../data/names.txt',
	     '../data/condiciones.txt',
	     '../data/combinaciones.txt']

  #////////////////////////////////////////
  # INITIALIZING IMPORTANT VARIABLES
  #////////////////////////////////////////
  Tsp_min = SENSPRM['Tstb']
  Tsp_max = Tsp_min + SENSPRM['Tsp']
  Tst_min = Tsp_max
  ntimes = SENSPRM['Tstb'] + SENSPRM['Tsp'] + SENSPRM['Tst']
  time = np.arange(0,ntimes)
  # nrs_nonstimulated = np.ones(len(AI),dtype="bool")
  # nrs_nonstimulated[:6] = False

  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////
  ParentDir = GRPRM["ParentDir"]
  # NAME = list()
  # ifile2 = open(filenames[0],"r")
  #
  # for line in ifile2:
  #   NAME.append(line.strip())

  Size = int(get("awk 'END {print NR}' %s"%(ParentDir+"MatrixInitial.txt")))

  #-------------------------------------------------
  # Get the index of neurons with:
  #  * higher out degree
  #  * lower out degree
  #  * stimulated neurons
  #-------------------------------------------------
  Ai = np.loadtxt(ParentDir+"MatrixInitial.txt")
  din,dout = GetDegree(Ai)
  idx = np.argsort(dout)
  IDX = idx[-SENSPRM['Nstim']:] #for neurons with highest dout
  nrs_nonstimulated = np.ones(len(Ai),dtype="bool")
  nrs_nonstimulated[IDX] = False


  #//////////////////////////////////////////////
  # Reading file with jcritics
  #//////////////////////////////////////////////
  NAME = np.loadtxt("%sJcritics.txt"%ParentDir,usecols=[0],dtype="|S14")
  JC,SIGMAJ = np.loadtxt("%sJcritics.txt"%ParentDir,usecols=[1,2],unpack=True)
  SizeJC = len(JC)
  Nl = len(NAME)

  # Initialiting variables to save statistic
  mean_sp = np.zeros(Nl)
  mean_st = np.zeros(Nl)
  sigma_sp = np.zeros(Nl)
  sigma_st = np.zeros(Nl)

  # Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)

  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  for n in xrange(Nl):
    ifname = ParentDir + NAME[n] + "/Matrix" + NAME[n] + ".txt"
    ofname = ParentDir + NAME[n]
    Af = np.loadtxt(ifname,dtype="int")
    # rprom = np.zeros_like(J)
    sys.stdout.write("Dynamic profile --> %s     progress: %.2f %%\n"%(NAME[n],(100.0*(n+1))/(Nl)))
    sys.stdout.flush()

    #-----------------------------------
    # Initializing some needed variables
    #-----------------------------------
    nactives = np.zeros(ntimes)
    J0 = JC[n] - DsgmJ*SIGMAJ[n]

    #-----------------------------------
    # Determining stability curve
    #-----------------------------------
    stb.DynamicSensibilityOneP(Af,SENSPRM,J0,nrs_nonstimulated,nactives)

    nactives_spontaneous = nactives[Tsp_min:Tsp_max]
    nactives_stimulated = nactives[Tsp_max:]

    # np.savetxt("%s/MFRS_vs_J-h%d.dat"%(ofname,idx),np.array([time,nactives]).T,fmt="%10f")
    np.savetxt("%s/NneuronsMean_spontaneous.dat"%(ofname),np.array([time[Tsp_min:Tsp_max],nactives_spontaneous]).T,fmt="%10f")
    np.savetxt("%s/NneuronsMean_stimulated.dat"%(ofname),np.array([time[Tsp_max:],nactives_stimulated]).T,fmt="%10f")

    #*************************************
    # Plot histogram curves
    #*************************************

    Bins = np.arange(Size)
    plt.figure()
    NcountSp = plt.hist(nactives_spontaneous,Bins,histtype='stepfilled',label="spontaneous",alpha=0.4,normed=True , color = 'g')[0]
    NcountSt = plt.hist(nactives_stimulated,Bins,histtype='stepfilled',label="Stimulated",alpha=0.4,normed=True , color = 'b')[0]

    # Determining the xmax -------------------------
    argXmaxSp = np.argwhere(NcountSp>0)[-1,-1]
    argXmaxSt = np.argwhere(NcountSt>0)[-1,-1]
    argxmax = argXmaxSp + 4 if argXmaxSp > argXmaxSt else argXmaxSt + 4
    # Determining the xmin -------------------------
    argXminSp = np.argwhere(NcountSp>0)[0,0]
    argXminSt = np.argwhere(NcountSt>0)[0,0]
    argxmin = argXminSp if argXminSp < argXminSt else argXminSt
    argxmin = argxmin - 4 if argxmin - 4>=0 else 0


    #*****************************************************************************************************************
    # Fit to gaussian distribution
    #*****************************************************************************************************************
    # -----------------------------------------------------------------------------------
    # # Determining the zone where the number of active nodes is less than the 30% of total nodes
    nspontaneous = nactives_spontaneous[ nactives_spontaneous < 0.3*Size ]
    nstimulated = nactives_stimulated[ nactives_stimulated < 0.3*Size ]
    try:
        xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2
    except ValueError:
        nspontaneous = nactives_spontaneous.copy()
        nstimulated = nactives_stimulated.copy()
        xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2


    xsp = np.linspace(argxmin, xmax, len(nspontaneous)) # spontaneous
    xst = np.linspace(argxmin, xmax, len(nstimulated)) # stimulated
    mean_sp[n], sigma_sp[n] = stats.norm.fit(nspontaneous) # get mean and standard deviation
    mean_st[n], sigma_st[n] = stats.norm.fit(nstimulated) # get mean and standard deviation
    pdfSp = stats.norm.pdf(xsp, mean_sp[n], sigma_sp[n]) # get theoretical values in our interval
    pdfSt = stats.norm.pdf(xst, mean_st[n], sigma_st[n]) # get theoretical values in our interval
    plt.plot(xsp, pdfSp,'g-',lw=2)
    plt.plot(xst, pdfSt,'b-',lw=2)

    plt.xlim(xmax=xmax,xmin=argxmin)

    plt.grid(which='both')
    plt.xlabel("Active Neurons",fontsize=15)
    plt.ylabel("Counts ",fontsize=15)

    leg = plt.legend(loc='best',shadow=True)

    plt.title("profile %s"%NAME[n].replace("_"," "))
    plt.savefig("%s/Sensitivity_histo.pdf"%(ofname))
    plt.close()

  # Saving data in array -------------------------------------------------------------------------
  STAT = np.array(zip(NAME,JC-DsgmJ*SIGMAJ,mean_sp,mean_st,sigma_sp,sigma_st),dtype=[\
    ('name','S14'),('J0','f8'),('meanSp','f8'),('meanSt','f8'),('sigmaSp','f8'),('sigmaSt','f8')])
  np.savetxt("%sSens-statistic.txt"%(ParentDir),STAT,fmt="%14s %10f %10f %10f %10f %10f",\
  header="# PROFILE   J0   meanSp   meanSt  sigmaSp  sigmaSt")
#===============================================================================================
# TRAINING SELECTED PROFILES
#===============================================================================================
if OPTION == "SELECTP":

  #////////////////////////////////////////
  # DEFINING INITIAL PARAMETER
  #////////////////////////////////////////
  r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
  idx = 0

  STBPRM = {"pc":0.1,
	    "Dt":10E-3,
	    "Ntimes": 200,
	    "Nr": 5,
	    "r0":r0[idx],
	    "NiActv":5,
	    "ncol":10}

  GRPRM = {"ParentDir": Argv[it+1],"savefile":True,"showplot":False}

  J = np.arange(0,45,0.2)


  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////

  ParentDir = GRPRM["ParentDir"]

  Nfiles = int(get("ls %sAssort* | wc -l"%ParentDir))
  name = ParentDir.split("/")[-2]

  #//////////////////////////////////////////////
  # Reading mean firing rate for initial matrix
  #//////////////////////////////////////////////
  #Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)

  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  for n in xrange(Nfiles):

    ifname = ParentDir + "Matrix" + name + "_V%d.txt"%n
    ofname = ParentDir
    Af = np.loadtxt(ifname,dtype="int")
    rprom = np.zeros_like(J)
    sys.stdout.write("Dynamic profile --> %s     progress: %.2f %%"%(n,(100.0*(n+1))/(Nfiles)))
    sys.stdout.flush()

    #-----------------------------------
    # Determining stability curve
    #-----------------------------------
    stb.DynamicStabilityOneP(Af,STBPRM,J,rprom)

    np.savetxt("%s/MFRS_vs_J-h%d_V%d.dat"%(ofname,idx,n),np.array([J,rprom]).T,fmt="%10f")

    """
    #*************************************
    # Plot stability curves
    #*************************************
    plt.figure()
    plt.semilogy(Ji,Ri,'r-',label='Initial: $r_0 = %d$ Hz'%r0[idx])
    plt.semilogy(J,rprom,'-',color=CL['Darkgoldenrod 1'],label = 'final: $r_0 = %d$ Hz'%r0[idx])
    plt.ylim(ymax=150)
    plt.grid(which='both')
    plt.xlabel("J",fontsize=15)
    plt.ylabel("Firing Rate r(Hz)",fontsize=15)

    leg = plt.legend(loc='best',shadow=True)
    # set the linewidth of each legend object
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)

    plt.title("profile %s"%NAME[n].replace("_"," "))
    plt.savefig("%s/MFRS_vs_J-h%d.pdf"%(ofname,idx))
    plt.close()
    """
