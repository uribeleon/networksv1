import numpy as np
import matplotlib.pyplot as plt
import os,sys
import math



#=========================================
# Probability function for neuron activity
# - Stochastic mode
#=========================================
def Firing_Probability(x,h0,J,pc,A):
  N = len(x)
  nu_new = np.zeros(N)
  fc = J/(N*pc)
  nu_new = 1.0/( 1.0 + np.exp( h0 - fc*np.dot(A,x) ) )
  return nu_new

def Stability(Params,J):

  # Filename of the Adjacency matrix
  fileini = Params["filename"]
  
  # Probability of conection
  pc = Params["pc"]
  
  # Size of timebin(DT)
  Dt = Params["Dt"]
  
  # baseline firing rate
  r0 = Params["r0"]
  
  # Size of the rasterplot
  Ntimes = Params["Ntimes"]
  
  # Number of repetitions
  Nr = Params["Nr"]
  
  # Number of Initial active neurons
  NiAct = Params["NiActv"]
  
  # Number of columns to calculate firing rate
  ncol = Params["ncol"]

  # Baseline probability of firing.
  h0 = np.log(1.0/(r0*Dt)-1)
  
  
  # Reading Adjacency matrix
  Ai = np.loadtxt(fileini,dtype=np.int)
  Size = len(Ai)
  
  # Initial random activity of neurons
  xi = np.zeros(Size,dtype=np.int)
  loc = np.random.randint(Size,size=NiAct)
  xi[loc] = 1
  rasterplot = np.zeros((Size,Ntimes))
  rasterplot[:,0] = xi.copy()
  r = np.zeros_like(J)
  rprom = np.zeros_like(J)


  for nr in xrange(Nr):
    for j in range(len(J)):
      x = xi.copy()
      for t in xrange(1,Ntimes):
	# Calculating the firing probability
	nu = Firing_Probability(x,h0,J[j],pc,Ai)
	
	# Generating uniform random number to determine if
	# neurons spike or not
	Threshold = np.random.rand(Size)
	Spike = nu >= Threshold
	
	#Final State
	#-------------------------------------
	x=Spike.astype(int)
	rasterplot[:,t] = x.copy()
	#nu = nuN.copy()

      r[j] = np.sum(rasterplot[:,-ncol:])/(Size*ncol*Dt)
    rprom = rprom + r
    
  rprom = rprom/Nr  
  
  return rprom
    