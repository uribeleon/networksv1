from cython.view cimport array as cvarray
import numpy as np
from libc.math cimport exp,sqrt,pow,log

import os,sys

#==============
# SHORCUTS
#==============
system=os.system
argv = sys.argv


#=========================================
# Probability function for neuron activity
# - Stochastic mode
#=========================================
def Firing_Probability(x,h0,J,pc,A):
  N = len(x)
  nu_new = np.zeros(N)
  fc = J/(N*pc)
  nu_new = 1.0/( 1.0 + np.exp( h0 - fc*np.dot(A,x) ) )
  return nu_new

#=========================================
# CYTHON
# Probability function for neuron activity
# - Stochastic mode 
#=========================================
def FiringProb(long [::1] X, double h0, double J, double pc, long [:,::1] A):
  
  cdef N = len(X)
  cdef double [::1] Nu = np.zeros(N)
  cdef int i,j
  cdef double Sum
  cdef double fc = J/(N*pc)
  
  for i in xrange(N):
    Sum = 0.0
    for j in xrange(N):
      Sum += A[i,j]*X[j] 
    
    Nu[i] = 1.0/( 1.0 + exp( h0 - fc*Sum ))
    
  return Nu

#=========================================
# CYTHON
# Dynamic stability for one profile
# - Stochastic mode 
#=========================================
cpdef void DynamicStabilityOneP(A, #Inital Matrix
				Params, # Dict with basic params 
			 double[::1] J,  # Overall coupling strength
			 double[::1] rprom):  
			 
  
  #*********************************************************************
  #                    GENERAL DECLARATIONS
  #*********************************************************************
  cdef:
    int i,j,k,t=0
    int row,column
  
  #*********************************************************************
  #                    ASSIGMENT INITIAL PARAMETERS
  #*********************************************************************
  
  # Filename of the Adjacency matrix
  #fileini = Params["filename"]
  
  # Probability of conection
  cdef double pc = Params["pc"]
  
  # Size of timebin(DT)
  cdef double Dt = Params["Dt"]
  
  # baseline firing rate
  cdef double r0 = Params["r0"]
  
  # Size of the rasterplot
  cdef long Ntimes = Params["Ntimes"]
  
  # Number of repetitions
  cdef long Nr = Params["Nr"]
  
  # Number of Initial active neurons
  cdef long NiAct = Params["NiActv"]
  
  # Number of columns to calculate firing rate
  cdef long ncol = Params["ncol"]
  
  
  
  #*********************************************************************
  #     DEFINING, CALCULATING AND READING SOME PARAMETERS/VARIABLES
  #*********************************************************************
    
  # Baseline probability of firing.
  cdef double h0 = log(1.0/(r0*Dt)-1)
  
  
  # Reading Adjacency matrix
  cdef long [:,::1] Ai = A #np.loadtxt(fileini,dtype=np.int)
  cdef unsigned int Size = len(Ai)
  
  # Initial random activity of neurons
  xin = np.zeros(Size,dtype=np.int)
  loc = np.random.randint(Size,size=NiAct)
  xin[loc] = 1
  cdef long [::1] xi = xin[:] #making a copy to cython array
  
  # Initialiting array of rasterplot
  cdef long [:,::1] rasterplot = np.zeros((Size,Ntimes),dtype=np.int)
  for i in range(Size):
    rasterplot[i,0] = xi[i]
    
  # Defining array of firing rate
  cdef double [::1] FrRate = np.zeros(len(J))
  #cdef double [::1] rprom = np.zeros(len(J))
    
  
  
  #*********************************************************************
  #                 CALCULATING THE RASTERPLOT
  #*********************************************************************
  cdef double [::1] Threshold
  cdef double [::1] nu
  cdef long [::1] x = xi
  cdef double sumFR
  cdef double ft = 1.0/Nr
  
  for nr in range(Nr): # For of repetitions
    for j in range(len(J)):
      for t in range(1,Ntimes):
    
	# Calculating the firing probability
        nu = FiringProb(x,h0,J[j],pc,Ai)
	#nu = Firing_Probability(x,h0,J[j],pc,Ai)

	# Generating uniform random number to determine if
	# neurons spike or not
        Threshold = np.random.rand(Size)
	#Spike = nu >= Threshold
	
        for k in range(Size):
	  #Final State
          if nu[k] >= Threshold[k]:
            x[k] = 1
          else:
            x[k] = 0
	  # Save the activity in rasterplot   
          rasterplot[k,t] = x[k]
      
      #rprom = rprom + r
      
      #-------------------------------------------
      # Calculating the Firing Rate
      #-------------------------------------------
      sumFR = 0.0
      for row in range(Size):
        for column in range(-ncol,0):
          sumFR += rasterplot[row,column] 
      #FrRate[j] = sumFR/(Size*ncol*Dt)
      rprom[j] += ft*sumFR/(Size*ncol*Dt)
      
 
  #return rprom

