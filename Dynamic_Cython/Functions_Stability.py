import numpy as np
#import matplotlib as mpl
#mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
#SCIPY
import scipy as sp
import scipy.linalg as la
import scipy.optimize as so
import scipy.stats as st
import scipy.misc as ms


#=======================
# SIGMOID FUNCTION
#=======================
def sigmoid(x,*Args):
  a,b,c,d = Args
  return c/(1.0 + np.exp( -b*(x-a) ) ) + d


def GetJcritic(J,R,guess):
  
  #////////////////////////////////////////
  # FITTING FOR INITIAL PROFILE
  #////////////////////////////////////////
  
  # Making fit of the data
  params, params_covariance = so.curve_fit(sigmoid, J, R, guess)
  a,b,c,d = params
  ARGS = (a,b,c,d)
  
  # calculating the theoretical curve with the results of fit
  xr = np.arange(0,20,0.01)
  y = sigmoid(xr,*ARGS)
  
  #print ARGS
  ##plt.semilogy(xr,y,'b-')
  ##plt.semilogy(J,R,'r-')
  #plt.plot(xr,y,'b-')
  #plt.plot(J,R,'r-')
  #plt.ylim(ymax=120)  
  #plt.grid()
  #plt.show()

  #calculating the numerical detivate of theoretical curve
  Yderiv = ms.derivative(sigmoid,xr,dx=1e-6,args=(a,b,c,d))
  #idx = np.where(Yderiv>0.1)
  #jcrit = xr[idx[0]]
  Sigma = np.sqrt(2*np.pi**2/(6*b**2))
  Jcritic = a
  
  return Jcritic,Sigma
