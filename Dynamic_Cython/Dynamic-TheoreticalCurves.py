#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
from mymodules import *
from Colors import *
import commands as cm
import scipy.linalg as la
import scipy.optimize as so
import argparse
import textwrap

from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter

#********************************************
# CREATING HELP
#********************************************
parser=argparse.ArgumentParser(
  prog='Dynamic-TheoricCurves.py',
  formatter_class=argparse.RawDescriptionHelpFormatter,
  description=textwrap.dedent('''\
**********************************************************************************************************************
  This program plots the theoretical curves for dynamics of networks
    
  USAGE: python Dynamic-TheoreticalCurves.py <OPTIONS> <ARGS>
    
  where OPTIONS are:
    
   * ROOT  --> Plot the curves of root in the mean field limit
      
   * MFR-MF-ONER --> Plot the mean firing rate vs J for a specific baseline firing rate for
		     a infinite-size network
	>> EXAMPLE: python Dynamic-TheoricCurves.py MFR-MF-ONER # (where # is 0,1,2,3 for h0[#])
	    
   * MFR-MF --> Plot the mean firing rate vs J for several baseline firing rate for
		a infinite-size network
      
   * MFR-NONSTOCHASTIC-ONER --> Plot the mean firing rate vs J in the mean firing rate limit
				with a finite-size network and a specific baseline firing rate
				  
	>> EXAMPLE: python Dynamic-TheoricCurves.py MFR-NONSTOCHASTIC-ONER -v <#>
	    
   * MFR-NONSTOCHASTIC  --> Plot the mean firing rate vs J in the mean firing rate limit
			    with a finite-size network and several baseline firing rate
		
   * FR-STOCHASTIC-ONER   --> Plot the mean firing rate vs J using stochastic form 
			      with a real finite-size network and a specific baseline firing rate
				 
	>> EXAMPLE: python Dynamic-TheoricCurves.py FR-STOCHASTIC-ONER -v 1
	>> EXAMPLE: python Dynamic-TheoreticalCurves.py FR-STOCHASTIC-ONER -s -p ./AllProfiles/ -if ./AllProfiles/MatrixInitial.txt -v 0
				 
   * FR-STOCHASTIC  --> Plot the mean firing rate vs J using stochastic form with a finite-size network
			and several baselines firing rate
			   
   * FR-STOCHASTIC-ONER-ONEP
			   
   * FR-STOCHASTIC-ONER-ALLP  --> Plot the mean firing rate vs J using the stochastic form with a 
				  finite-size network, a specific baseline firing rate for all
				  assortativity profiles
    '''),epilog='''
**********************************************************************************************************************
  ''')


def Function(x,*Args):
  J,H0 = Args
  f = x - 1.0/(1 + np.exp(H0 - J*x))
  return f


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
argv = sys.argv

#***************************
# PARSER ARGUMENTS OPTIONS
#***************************
parser.add_argument('option', action="store")
parser.add_argument('-if',dest='ifile',action="store",default=0,help="path to initial file",type=str)
parser.add_argument('-of',dest='ofile',action="store",default=0,help="path to final file",type=str)
#parser.add_argument('ifile',action="store",help="path to initial file")
parser.add_argument('-v',dest='value', action="store",default=0,help='firing rate index',type=int)
parser.add_argument('-p',dest='pathDir',action="store",default=0,help="path of parent folder",type=str)
parser.add_argument('-s',dest='savefile',action="store_true",default=False,help="save data in a text file")
parser.add_argument('-subplot',dest='zoom',action="store_true",default=False,help="plot a subplot zoomed of transient plot")
#parser.add_argument('-s',dest='savefile',action="store_true",help="save data in a text file")
#parser.add_argument('value', action="store",default=0,type=int)
args=parser.parse_args()

OPTION = args.option

#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
filenames = ['./data/names.txt',
	     './data/condiciones.txt',
	     './data/combinaciones.txt']

if OPTION != "FR-STOCHASTIC-ONER-ALLP":
  # Reading initial adjacency matrix
  #fileini = "./data/MatrixInitial.txt"
  fileini = args.ifile
  #fileini = "./data/Matrix2000N_0001_1e6r.txt"
  Ai = np.loadtxt(fileini)
  Size = len(Ai)




#==================================================================================================
# STUDY THE DYNAMICS OF NETWORK
#==================================================================================================

pc = 0.1 # conection probability
r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
Dt = 10E-3 # 10ms
h0 = np.log(1.0/(r0*Dt)-1)
J = np.arange(0.0, 45,0.1)
#nu = 0.1*np.random.rand(Size)

if OPTION == "ROOT":
  
  #-------------------------------------------------------------------
  #  Calculating the roots in the mean field
  #-------------------------------------------------------------------
  dx = 0.01
  x = np.arange(0.0,1.0+dx,dx)

  for k in xrange(len(h0)):
    points = list()

    for j in J:
      xold = x[0]
      ARGS = (j,h0[k])
      roots = list()

      for x0 in x:
	if np.sign(Function(x0,*ARGS))!=np.sign(Function(xold,*ARGS)):
	  roots.append(so.brentq(Function,x0,xold,args=ARGS))
	xold = x0

      for i in xrange(len(roots)):
	points.append([j,roots[i]])
    points = np.array(points)
    points = points[np.argsort(points[:,1])]
    plt.plot(points[:,0],points[:,1],'-',linewidth = 2,label = "$r_0 : $ %d Hz"%(r0[k]))
    
  plt.legend(loc='best',shadow=True)
  plt.yticks(np.arange(0.0,1.2,0.1))  
  plt.ylim(ymax=1.1)
  plt.xlabel("J")
  plt.grid()
  plt.ylabel("roots $\\nu$ (mean field)")
  plt.savefig("./AllProfiles/RootsMeanField.pdf")
  plt.close()
 
elif OPTION == "MFR-MF-ONER":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate - Mean Fiel Limit (Infinite-size Network)
  #-------------------------------------------------------------------------------
  Ntimes = 400
  Size = 1
  rasterplot = np.zeros((Size,1),dtype=float)
  nui = 0.001*np.random.rand(Size)
  rasterplot[:,0] = nui.copy()
  r = np.zeros_like(J)

  i = args.value
  for j in range(len(J)):
    #nu = np.zeros(Size)
    nu = nui.copy()
    #nu[loc] = np.random.rand(len(loc))*0.1
    #nu = 0.1*np.random.rand(Size)
    #rasterplot[:,0] = nu.copy()
    for t in xrange(1,Ntimes):
      # Calculating the firing probability
      #nuN = FiringRate_nonstochastic(nu,h0[0],j,pc,A)
      nu = FiringRate_MeanField(nu,h0[i],J[j])
      rasterplot = nu.copy()
      #nu = nuN.copy()

    r[j] = np.sum(rasterplot)/(Size*Dt)

    
  plt.semilogy(J,r,'-',label = '$r_0 = %d$ Hz'%r0[i])

  plt.ylim(ymax=150)
  plt.grid(which='both')
  plt.xlabel("J",fontsize=15)
  plt.ylabel("Mean Firing Rate r(Hz)",fontsize=15)
  plt.legend(loc='best',shadow=True)
  plt.savefig("./AllProfiles/MeanFRvsJ-h%d.pdf"%i)

elif OPTION == "MFR-MF":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate - Mean Fiel Limit (Infinite-size Network)
  #-------------------------------------------------------------------------------
  Ntimes = 20
  Size = 1
  rasterplot = np.zeros((Size,Ntimes),dtype=float)
  nui = 0.01*np.random.rand(Size)
  rasterplot[:,0] = nui.copy()
  r = np.zeros_like(J)


  for i in range(len(h0)):
    for j in range(len(J)):
      nu = nui.copy()
      for t in xrange(1,Ntimes):
	# Calculating the firing probability
	nu = FiringRate_MeanField(nu,h0[i],J[j])
	rasterplot[:,t] = nu.copy()

      r[j] = np.sum(rasterplot[:,-10])/(Size*Dt)

  # ------ Plotting ------  
    plt.semilogy(J,r,'-',label = '$r_0 = %d$ Hz'%r0[i])

  plt.ylim(ymax=150)
  plt.grid(which='both')
  plt.xlabel("J",fontsize=15)
  plt.ylabel("Mean Firing Rate r(Hz)",fontsize=15)
  plt.legend(loc='best',shadow=True)
  plt.savefig("./AllProfiles/MeanFRvsJ.pdf")
  

elif OPTION == "MFR-NONSTOCHASTIC-ONER": 
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate - Mean Fiel Limit and non-stochastic
  #-------------------------------------------------------------------------------
  Ntimes = 10
  rasterplot = np.zeros((Size,1),dtype=float)
  nui = 0.01*np.random.rand(Size)
  rasterplot[:,0] = nui.copy()
  r = np.zeros_like(J)

  i = args.value
  for j in range(len(J)):
    #nu = np.zeros(Size)
    nu = nui.copy()
    #nu[loc] = np.random.rand(len(loc))*0.1
    #nu = 0.1*np.random.rand(Size)
    #rasterplot[:,0] = nu.copy()
    for t in xrange(1,Ntimes):
      # Calculating the firing probability
      nu = FiringRate_nonstochastic(nu,h0[i],J[j],pc,Ai)
      #nu = FiringRate_MeanField(nu,h0[i],J[j])
      rasterplot = nu.copy()
      #nu = nuN.copy()

    r[j] = np.sum(rasterplot[:])/(Size*Dt)
    
  plt.semilogy(J,r,'-',label = '$r_0 = %d$ Hz'%r0[i])

  plt.ylim(ymax=150)
  plt.grid(which='both')
  plt.xlabel("J",fontsize=15)
  plt.ylabel("Mean Firing Rate r(Hz)",fontsize=15)
  plt.legend(loc='best',shadow=True)
  plt.savefig("./AllProfiles/MFR_NonStochastic_vs_J-h%d.pdf"%i)
  
elif OPTION == "MFR-NONSTOCHASTIC":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate - Mean Fiel Limit and non-stochastic
  #-------------------------------------------------------------------------------
  Ntimes = 10
  rasterplot = np.zeros((Size,1),dtype=float)
  nui = 0.01*np.random.rand(Size)
  rasterplot = nui.copy()
  r = np.zeros_like(J)

  for i in range(len(h0)):
    for j in range(len(J)):
      nu = nui.copy()
      for t in xrange(1,Ntimes):
	# Calculating the firing probability
	nu = FiringRate_nonstochastic(nu,h0[i],J[j],pc,Ai)
	rasterplot = nu.copy()

      r[j] = np.sum(rasterplot)/(Size*Dt)
      
    plt.semilogy(J,r,'-',label = '$r_0 = %d$ Hz'%r0[i])

  plt.ylim(ymax=150)
  plt.grid(which='both')
  plt.xlabel("J",fontsize=15)
  plt.ylabel("Mean Firing Rate r(Hz)",fontsize=15)
  plt.legend(loc='best',shadow=True)
  plt.title("r vs J - Non stochastic mode in Mean Field")
  plt.savefig("./AllProfiles/MFR_NonStochastic_vs_J.pdf")

elif OPTION == "FR-STOCHASTIC-ONER":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate - Mean Fiel Limit and non-stochastic
  #-------------------------------------------------------------------------------
  Ntimes = 400
  Nr = 30
  rasterplot = np.zeros((Size,Ntimes),dtype=float)
  xi = np.zeros(Size)
  loc = np.random.randint(Size,size=10)
  xi[loc] = 1.0
  rasterplot[:,0] = xi.copy()
  r = np.zeros_like(J)
  rprom = np.zeros_like(J)
  ncol = 10
  
  i = args.value
  for nr in xrange(Nr):
    sys.stdout.write("====>  Repeated Proccess percentage: %d  %5.2f%%\r"%(nr,(100.0*nr)/Nr))
    sys.stdout.flush()
    for j in range(len(J)):
      x = xi.copy()
      for t in xrange(1,Ntimes):
	# Calculating the firing probability
	nu = Firing_Probability(x,h0[i],J[j],pc,Ai)
	
	# Generating uniform random number to determine if
	# neurons spike or not
	Threshold = np.random.rand(Size)
	Spike = nu >= Threshold
	
	#Final State
	#-------------------------------------
	x=Spike.astype(int)
	rasterplot[:,t] = x.copy()
	#nu = nuN.copy()

      r[j] = np.sum(rasterplot[:,-ncol:])/(Size*ncol*Dt)
    rprom = rprom + r
    
  rprom = rprom/Nr  
  if args.savefile:
    parentdir = args.pathDir
    TABLE = np.array([J,r])
    np.savetxt("%s/MFRS_Minitial_h%d.txt"%(parentdir,i),TABLE.T)
    
  plt.semilogy(J,r,'-',label = '$r_0 = %d$ Hz'%r0[i])

  plt.ylim(ymax=150)
  plt.grid(which='both')
  plt.xlabel("J",fontsize=15)
  plt.ylabel("Firing Rate r(Hz)",fontsize=15)
  plt.legend(loc='best',shadow=True)
  plt.savefig("%s/FR_Stochastic_vs_J-h%d.pdf"%(parentdir,i))
  
elif OPTION == "FR-STOCHASTIC-ONER-ALLP":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate - ALl profiles and stochastic form
  #-------------------------------------------------------------------------------
    
  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////
  
  ParentDir = args.pathDir
  NAME = list()
  ifile2 = open(filenames[0],"r")

  for line in ifile2:
    NAME.append(line.strip())
    
  Nl = len(NAME)
  Size = int(get("awk 'END {print NR}' %s"%(ParentDir+"MatrixInitial.txt")))
  
  #//////////////////////////////////////////////
  # Reading mean firing rate for initial matrix
  #//////////////////////////////////////////////
  Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,args.value),unpack = True)
  
  #"""
  #/////////////////////////////////////////
  # Some initial parameter for dynamics
  #/////////////////////////////////////////
  
  Ntimes = 400
  Nrepeat = 50
  rasterplot = np.zeros((Size,Ntimes),dtype=float)
  xi = np.zeros(Size)
  loc = np.random.randint(Size,size=25)
  xi[loc] = 1.0
  rasterplot[:,0] = xi.copy()
  r = np.zeros_like(J)
  rprom = np.zeros_like(J)
  ncol = 10
  
  
  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  for n in xrange(Nl):
  
    ifname = "./" + ParentDir + NAME[n] + "/Matrix" + NAME[n] + ".txt"
    ofname = "./" + ParentDir + NAME[n]
    Af = np.loadtxt(ifname)
    rprom = np.zeros_like(J)
    
    #"""
    print "Dynamic profile --> %s     progress: %.2f %%"%(NAME[n],(100.0*(n+1))/(Nl))
    i = args.value
    for nr in xrange(Nrepeat):
      sys.stdout.write("====>  Repeated Proccess percentage: %d  %5.2f%%\r"%(nr,(100.0*nr)/Nrepeat))
      sys.stdout.flush()
      
      for j in xrange(len(J)):
	x = xi.copy()
	for t in xrange(1,Ntimes):
	  # Calculating the firing probability
	  nu = Firing_Probability(x,h0[i],J[j],pc,Af)
	
	  # Generating uniform random number to determine if
	  # neurons spike or not
	  Threshold = np.random.rand(Size)
	  Spike = nu >= Threshold
	
	  #Final State
	  #-------------------------------------
	  x=Spike.astype(int)
	  rasterplot[:,t] = x.copy()
	  #nu = nuN.copy()

	r[j] = np.sum(rasterplot[:,-ncol:])/(Size*ncol*Dt)
      rprom = rprom + r
    
    rprom = rprom/Nrepeat
    np.savetxt("%s/MFRS_vs_J-h%d.dat"%(ofname,i),np.array([J,rprom]).T,fmt="%10f")
    
    plt.figure()
    plt.semilogy(Ji,Ri,'r-',label='Initial: $r_0 = %d$ Hz'%r0[i])
    plt.semilogy(J,rprom,'-',color=CL['Darkgoldenrod 1'],label = 'final: $r_0 = %d$ Hz'%r0[i])
    plt.ylim(ymax=150)
    plt.grid(which='both')
    plt.xlabel("J",fontsize=15)
    plt.ylabel("Firing Rate r(Hz)",fontsize=15)
    
    leg = plt.legend(loc='best',shadow=True)
    # set the linewidth of each legend object
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)
      
    plt.title("profile %s"%NAME[n].replace("_"," "))
    plt.savefig("%s/MFRS_vs_J-h%d.pdf"%(ofname,i))
    plt.close()
  #"""
elif OPTION == "TRANSIENT":
  
  #-------------------------------------------------------------------------------
  #  Calculating the percentage of active neuron for a Ntimes time - stochastic
  #-------------------------------------------------------------------------------
  Ntimes = 600
  Nn = int(Size*0.2)
  
  rasterplot = np.zeros((Size,Ntimes),dtype=float)
  xi = np.zeros(Size)
  loc = range(Nn)#np.random.randint(500,size=25)
  J = np.arange(0,20,1)
  r = np.zeros_like(J)
  Nactives = np.zeros((Ntimes,len(J)))
  
  xi[loc] = 1.0
  rasterplot[:,0] = xi.copy()
  Nactives[0,:] = (100.0*np.sum(xi))/Size
  ncol = Ntimes
  cl = []
  
  i = args.value
  
  #---------------------------------------
  # Some parameter of plot
  #---------------------------------------
  #fig=plt.figure()
  #ax=plt.subplot(111)
  colormap = plt.cm.gist_ncar
  plt.gca().set_color_cycle([colormap(ii) for ii in np.linspace(0, 0.9, len(J))])
  
  
  for j in range(len(J)):
    x = xi.copy()


    for t in xrange(1,Ntimes):
      # Calculating the firing probability
      nu = Firing_Probability(x,h0[i],J[j],pc,Ai)
      
      # Generating uniform random number to determine if
      # neurons spike or not
      Threshold = np.random.rand(Size)
      Spike = nu >= Threshold
      
      #Final State
      #-------------------------------------
      x=Spike.astype(int)
      Nactives[t,j] = (100.0*np.sum(x))/Size
      rasterplot[:,t] = x.copy()
	#nu = nuN.copy()

    #cl.append(clr[np.random.randint(len(clr))])
    plt.plot(Nactives[:,j],'-',label = '$J = %.2f$'%J[j])
  #plt.ylim(ymax=150)
  plt.grid(which='both')
  plt.xlabel("Time/Ntimes ",fontsize=15)
  plt.ylabel("Percentage of active neurons",fontsize=15)
  
  # Shrink current axis by 20%
  #box = ax.get_position()
  #ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
  
  #Linewidth in legend
  leg = plt.legend(loc='upper right',shadow=True,fontsize=8,bbox_to_anchor=(1.18, 1.01))
  for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)
  
  if args.zoom:
    # Zoom within plot
    plt.axes([0.44,.42,0.43,0.43])
    colormap = plt.cm.gist_ncar
    plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, len(J))]) 
    for l in range(len(J)):
      plt.plot(Nactives[:,l],'-',linewidth = 1.2)
    plt.xlim(xmax = 30)
    plt.grid(which='both')
    
  plt.savefig("./Profiles/Transients/Transient.pdf",bbox_extra_artists=(leg,), bbox_inches='tight')
  #"""
else:
  print "No se hace nada..."
