import sys,os
import commands as cm
import matplotlib as mpl
mpl.use('PDF')
import sensibility as stb
import matplotlib.pyplot as plt
import numpy as np
from Colors import *
from scipy import stats
import time
from dynmod import *

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv = sys.argv
arr = np.array

#==============
# FUNCTIONS
#==============
def GetDegree(CIJ):
  indeg = np.sum(CIJ,axis=0) # columns
  outdeg = np.sum(CIJ,axis=1) # rows
  return indeg,outdeg


it=1
OPTION = Argv[1]

#===============================================================================================
# TRAINING ALL PROFILES
#===============================================================================================
if OPTION == "ALLP":


  #////////////////////////////////////////
  # DEFINING INITIAL PARAMETER
  #////////////////////////////////////////
  r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
  idx = 0
  DsgmJ = 15 #Delta SigmaJ
  PERCENT = 0.4

  SENSPRM = {"pc":0.1,
		"Dt":10E-3,
		"Nr": 1,
        "Nrep":30,
		"r0":1.0,
		"NiActv":10,
		"Nstim": 5,
		"ncol":10,
		"Tstb":50,
		"Tsp":200,
		"Tst": 200}

  # General parameters
  GRPRM = {"ParentDir": Argv[it+1],"savefile":True,"showplot":False}

  # Parameter for boostrapping
  PrmBst = {"nsample":1000,
            "alpha":95,
            "do-boostrp":True}

  # J = np.arange(0,45,0.2)

  filenames = ['../data/names.txt',
	     '../data/condiciones.txt',
	     '../data/combinaciones.txt']

  #////////////////////////////////////////
  # INITIALIZING IMPORTANT VARIABLES
  #////////////////////////////////////////
  Tsp_min = SENSPRM['Tstb']
  Tsp_max = Tsp_min + SENSPRM['Tsp']
  Tst_min = Tsp_max
  ntimes = SENSPRM['Tstb'] + SENSPRM['Tsp'] + SENSPRM['Tst']
  time = np.arange(0,ntimes)
  # nrs_nonstimulated = np.ones(len(AI),dtype="bool")
  # nrs_nonstimulated[:6] = False

  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////
  ParentDir = GRPRM["ParentDir"]
  # NAME = list()
  # ifile2 = open(filenames[0],"r")
  #
  # for line in ifile2:
  #   NAME.append(line.strip())

  Size = int(get("awk 'END {print NR}' %s"%(ParentDir+"MatrixInitial.txt")))

  #-------------------------------------------------
  # Get the index of neurons with:
  #  * higher out degree
  #  * lower out degree
  #  * stimulated neurons
  #-------------------------------------------------
  Ai = np.loadtxt(ParentDir+"MatrixInitial.txt")
  din,dout = GetDegree(Ai)
  idx = np.argsort(dout)
  IDX = idx[-SENSPRM['Nstim']:] #for neurons with highest dout
  nrs_nonstimulated = np.ones(len(Ai),dtype="bool")
  nrs_nonstimulated[IDX] = False


  #//////////////////////////////////////////////
  # Reading file with jcritics
  #//////////////////////////////////////////////
  NAME = np.loadtxt("%sJcritics2.txt"%ParentDir,usecols=[0],dtype="|S14")
  JC,SIGMAJ = np.loadtxt("%sJcritics2.txt"%ParentDir,usecols=[1,2],unpack=True)
  SizeJC = len(JC)
  Nl = len(NAME)

  # Initialiting variables to save statistic
  mean_sp = np.zeros(Nl)
  mean_st = np.zeros(Nl)
  sigma_sp = np.zeros(Nl)
  sigma_st = np.zeros(Nl)

  # Variables for boostrapping ----------------------------
  meansp_arr = np.zeros(SENSPRM['Nrep'])
  meanst_arr = np.zeros(SENSPRM['Nrep'])

  #.............................................................
  # VARIABLES THAT CONTAIN STATISTIC OF BOOTSTRAPPING
  # >>> spontaneous
  meansp_err = np.zeros(Nl)
  # >>> stimulated
  meanst_err = np.zeros(Nl)
  # >>> standar deviation spontaneous
  stdsp_err = np.zeros(Nl)
  # >>> standar deviation spontaneous
  stdst_err = np.zeros(Nl)
  # >>> diff between means of final results
  DiffMeanBS = np.zeros(Nl)
  # >>> Mean of meandiff of each iterations
  MeanDiffMean = np.zeros(Nl)
  # >>> Error of DiffMean_bp
  DiffMeanBS_err = np.zeros(Nl)
  # >>> Error of MeanDiffMean
  MeanDiffMean_err = np.zeros(Nl)


  #.............................................................




  # Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)

  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  for n in xrange(Nl):#(Nl):
    ifname = ParentDir + NAME[n] + "/Matrix" + NAME[n] + ".txt"
    ofname = ParentDir + NAME[n]
    Af = np.loadtxt(ifname,dtype="int")
    # rprom = np.zeros_like(J)
    sys.stdout.write("Dynamic profile --> %s     progress: %.2f %%\n"%(NAME[n],(100.0*(n+1))/(Nl)))
    sys.stdout.flush()

    #-----------------------------------
    # Initializing some needed variables
    #-----------------------------------
    #nactives = np.zeros(ntimes)
    J0 = JC[n] - PERCENT*JC[n]#JC[n] - DsgmJ*SIGMAJ[n]

    # LOOP FOR TO CREATE SAMPLE NEEDED FOR BOOSTSTRAP METHOD  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for irep in xrange(SENSPRM['Nrep']):
        nactives = np.zeros(ntimes)

        #-----------------------------------
        # Determining stability curve
        #-----------------------------------
        stb.DynamicSensibilityOneP(Af,SENSPRM,J0,nrs_nonstimulated,nactives)

        nactives_spontaneous = nactives[Tsp_min:Tsp_max]
        nactives_stimulated = nactives[Tsp_max:]

        # Counting number of actives neurons -----------------------------------------------------------------------------------------
        NcountSp = np.histogram(nactives_spontaneous,bins=range(Size))[0]
        NcountSt = np.histogram(nactives_stimulated,bins=range(Size))[0]

        # Determining the xmax -------------------------
        argXmaxSp = np.argwhere(NcountSp>0)[-1,-1]
        argXmaxSt = np.argwhere(NcountSt>0)[-1,-1]
        argxmax = argXmaxSp + 4 if argXmaxSp > argXmaxSt else argXmaxSt + 4
        # Determining the xmin -------------------------
        argXminSp = np.argwhere(NcountSp>0)[0,0]
        argXminSt = np.argwhere(NcountSt>0)[0,0]
        argxmin = argXminSp if argXminSp < argXminSt else argXminSt
        argxmin = argxmin - 4 if argxmin - 4>=0 else 0

        #*****************************************************************************************************************
        # Fit to gaussian distribution
        #*****************************************************************************************************************
        # -----------------------------------------------------------------------------------
        # # Determining the zone where the number of active nodes is less than the 30% of total nodes
        nspontaneous = nactives_spontaneous[ nactives_spontaneous < 0.3*Size ]
        nstimulated = nactives_stimulated[ nactives_stimulated < 0.3*Size ]
        try:
            xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2
        except ValueError:
            nspontaneous = nactives_spontaneous.copy()
            nstimulated = nactives_stimulated.copy()
            xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2


        xsp = np.linspace(argxmin, xmax, len(nspontaneous)) # spontaneous
        xst = np.linspace(argxmin, xmax, len(nstimulated)) # stimulated
        mean_sp, sigma_sp = stats.norm.fit(nspontaneous) # get mean and standard deviation
        mean_st, sigma_st = stats.norm.fit(nstimulated) # get mean and standard deviation
        pdfSp = stats.norm.pdf(xsp, mean_sp, sigma_sp) # get theoretical values in our interval
        pdfSt = stats.norm.pdf(xst, mean_st, sigma_st) # get theoretical values in our interval
        # Saving values in array to do boostsrapping
        meansp_arr[irep] = mean_sp
        meanst_arr[irep] = mean_st
    # END FOR LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    # MAKING BOOTSTRAPPING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # >>> spontaneous
    meansp_err_min, meansp_err_max, meansp_err[n] = bootstrap(meansp_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
    stdsp_err_min, stdsp_err_max, stdsp_err[n] = bootstrap(meansp_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

    # >>> stimulated
    meanst_err_min, meanst_err_max, meanst_err[n] = bootstrap(meanst_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
    stdst_err_min, stdst_err_max, stdst_err[n] = bootstrap(meanst_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

    # >>> Differences between means
    diffmean_err_min,diffmean_err_max,diffmean_err = bootstrap(meanst_arr-meansp_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
    diffstd_err_min,diffstd_err_max,diffstd_err = bootstrap(meanst_arr-meansp_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

    # END BOOTSTRAPPING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    #------------------------------------------------------------------------
    # SOME VALUES WITH BOOTSTRAP RESULTS

    # >>> Delta of mean values obtained by bootsrapping
    DiffMeanBS[n] = meanst_err[n] - meansp_err[n]
    # >>> Error of Delta of mean values obtained by bootsrapping
    DiffMeanBS_err[n] = np.sqrt(stdst_err[n]**2 + stdsp_err[n]**2)
    # >>> Mean of Delta of means
    MeanDiffMean[n] = diffmean_err
    # >>> Error of Mean of Delta of means
    MeanDiffMean_err[n] = diffstd_err
    # print n,diffstd_err
    #------------------------------------------------------------------------

    # np.savetxt("%s/MFRS_vs_J-h%d.dat"%(ofname,idx),np.array([time,nactives]).T,fmt="%10f")
    #np.savetxt("%s/NneuronsMean_spontaneous.dat"%(ofname),np.array([time[Tsp_min:Tsp_max],nactives_spontaneous]).T,fmt="%10f")
    #np.savetxt("%s/NneuronsMean_stimulated.dat"%(ofname),np.array([time[Tsp_max:],nactives_stimulated]).T,fmt="%10f")

    #*************************************
    # Plot histogram curves
    #*************************************

    Bins = range(Size)
    plt.figure()
    NcountSp = plt.hist(nactives_spontaneous,bins=Bins,histtype='stepfilled',label="spontaneous",alpha=0.4,normed=True , color = 'g')[0]
    NcountSt = plt.hist(nactives_stimulated,bins=Bins,histtype='stepfilled',label="Stimulated",alpha=0.4,normed=True , color = 'b')[0]
    #NcountSp = plt.hist(nactives_spontaneous,bins=range(len(Af)),histtype='stepfilled',label='spontaneous',alpha=0.4,normed=True, color='g')[0]
    #NcountSt = plt.hist(nactives_stimulated,bins=range(len(Af)),histtype='stepfilled',label='stimulated',alpha=0.4,normed=True , color='b')[0]

    # Plotting error BAR---------------------------------------------------------------------
    plt.errorbar(meansp_err[n],np.max(pdfSp)*1.1,xerr=3*stdsp_err[n],fmt='o',ms=10,mfc='k',mec='k',
                                              elinewidth=3,ecolor='k',capthick=3,capsize=5)
    plt.errorbar(meanst_err[n],np.max(pdfSt)*1.1,xerr=3*stdst_err[n],fmt='o',ms=10,mfc='k',mec='k',
                                              elinewidth=3,ecolor='k',capthick=3,capsize=5)


    # Plotting the fit -----------------------------------------------------------------------------------------
    plt.plot(xsp, pdfSp,'-',color='g',lw=2)
    #plt.plot(xsp[np.argmax(pdfSp)],np.max(pdfSp),'o',color='black')
    plt.plot(xst, pdfSt,'-',color='b',lw=2)



    plt.xlim(xmax=xmax,xmin=argxmin)
    plt.grid(which='both')
    plt.xlabel("Active Neurons",fontsize=15)
    plt.ylabel("Normalized Counts ",fontsize=15)
    leg = plt.legend(loc='best')
    plt.title("profile %s"%NAME[n].replace("_"," "))

    plt.savefig("%s/Sensitivity_histo.pdf"%(ofname))
    plt.close()

  # SAVING DATA IN ARRAY -------------------------------------------------------------------------
  #STAT = np.array(zip(NAME,JC-DsgmJ*SIGMAJ,mean_sp,mean_st,sigma_sp,sigma_st),dtype=[\
    #('name','S14'),('J0','f8'),('meanSp','f8'),('meanSt','f8'),('sigmaSp','f8'),('sigmaSt','f8')])
  STAT = np.array(zip(NAME,JC-PERCENT*JC,
                      meansp_err,meanst_err,
                      stdsp_err,stdst_err,
                      DiffMeanBS,MeanDiffMean,
                      DiffMeanBS_err,MeanDiffMean_err),dtype=[\
                      ('name','S14'),('J0','f8'),
                      ('meansp_err','f8'),('meanst_err','f8'),
                      ('stdsp_err','f8'),('stdst_err','f8'),
                      ('DiffMeanBS','f8'),('MeanDiffMean','f8'),
                      ('DiffMeanBS_err','f8'),('MeanDiffMean_err','f8')])

  np.savetxt("%sSens-statistic.txt"%(ParentDir),STAT,fmt="%14s %10f %10f %10f %10f %10f %10f %10f %10f %10f",\
  header="# PROFILE   J0   meanSp   meanSt  sigmaSp  sigmaSt DiffMeanBS  MeanDiffMean DiffMeanBS_err MeanDiffMean_err ")

#===============================================================================================
# TRAINING SELECTED PROFILES
#===============================================================================================
if OPTION == "SELECTP":

  #////////////////////////////////////////
  # DEFINING INITIAL PARAMETER
  #////////////////////////////////////////
  r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
  idx = 0
  DsgmJ = 15 #Delta SigmaJ
  PERCENT = float(Argv[4])

  SENSPRM = {"pc":0.1,
		"Dt":10E-3,
		"Nr": 1,
        "Nrep":20,
		"r0":1.0,
		"NiActv":10,
		"Nstim": 5,
		"ncol":190,
		"Tstb":50,
		"Tsp":200,
		"Tst": 200}

  # General parameters
  GRPRM = {"ParentDir": Argv[2],"filePselected":Argv[3],"savefile":True,"showplot":False}

  # Parameter for boostrapping
  PrmBst = {"nsample":1000,
            "alpha":95,
            "do-boostrp":True}

  # J = np.arange(0,45,0.2)

  filenames = ['../data/names.txt',
	     '../data/condiciones.txt',
	     '../data/combinaciones.txt']

  #////////////////////////////////////////
  # INITIALIZING IMPORTANT VARIABLES
  #////////////////////////////////////////
  Tsp_min = SENSPRM['Tstb']
  Tsp_max = Tsp_min + SENSPRM['Tsp']
  Tst_min = Tsp_max
  ntimes = SENSPRM['Tstb'] + SENSPRM['Tsp'] + SENSPRM['Tst']
  time = np.arange(0,ntimes)
  # nrs_nonstimulated = np.ones(len(AI),dtype="bool")
  # nrs_nonstimulated[:6] = False

  #////////////////////////////////////////
  # READING CONDITIONS OF EACH PROFILES
  #////////////////////////////////////////
  ParentDir = GRPRM["ParentDir"]
  # NAME = list()
  # ifile2 = open(filenames[0],"r")
  #
  # for line in ifile2:
  #   NAME.append(line.strip())

  Size = int(get("awk 'END {print NR}' %s"%(ParentDir+"MatrixInitial.txt")))

  #-------------------------------------------------
  # Get the index of neurons with:
  #  * higher out degree
  #  * lower out degree
  #  * stimulated neurons
  #-------------------------------------------------
  Ai = np.loadtxt(ParentDir+"MatrixInitial.txt")
  din,dout = GetDegree(Ai)
  idx = np.argsort(dout)
  IDX = idx[-SENSPRM['Nstim']:] #for neurons with highest dout
  nrs_nonstimulated = np.ones(len(Ai),dtype="bool")
  nrs_nonstimulated[IDX] = False


  #//////////////////////////////////////////////
  # Reading file with jcritics
  #//////////////////////////////////////////////
  NAME = np.loadtxt("%sJcritics2.txt"%ParentDir,usecols=[0],dtype="|S14")
  JC,SIGMAJ = np.loadtxt("%sJcritics2.txt"%ParentDir,usecols=[1,2],unpack=True)
  SizeJC = len(JC)
  Nl = len(NAME)

  # Initialiting variables to save statistic
  mean_sp = np.zeros(Nl)
  mean_st = np.zeros(Nl)
  sigma_sp = np.zeros(Nl)
  sigma_st = np.zeros(Nl)

  # Variables for boostrapping ----------------------------
  meansp_arr = np.zeros(SENSPRM['Nrep'])
  meanst_arr = np.zeros(SENSPRM['Nrep'])

  #.............................................................
  # VARIABLES THAT CONTAIN STATISTIC OF BOOTSTRAPPING
  # >>> spontaneous
  meansp_err = np.zeros(Nl)
  # >>> stimulated
  meanst_err = np.zeros(Nl)
  # >>> standar deviation spontaneous
  stdsp_err = np.zeros(Nl)
  # >>> standar deviation spontaneous
  stdst_err = np.zeros(Nl)
  # >>> diff between means of final results
  DiffMeanBS = np.zeros(Nl)
  # >>> Mean of meandiff of each iterations
  MeanDiffMean = np.zeros(Nl)
  # >>> Error of DiffMean_bp
  DiffMeanBS_err = np.zeros(Nl)
  # >>> Error of MeanDiffMean
  MeanDiffMean_err = np.zeros(Nl)


  #.............................................................


  #//////////////////////////////////////////
  # DETERMINING INDEX OF SELECTED PROFILES
  #//////////////////////////////////////////
  profSelected = np.array([np.loadtxt(GRPRM["filePselected"],dtype="|S14")])
  print profSelected
  IDselected = np.array([np.where(NAME == Prof)[0][0] for Prof in profSelected])


  # Ji,Ri = np.loadtxt("%sMFRS_Minitial_h%d.txt"%(ParentDir,0),unpack = True)

  #///////////////////////////////////
  # Begining to calculate the dynamic
  # for each profile
  #///////////////////////////////////
  loop = 0
  for n in IDselected:#(Nl):
    ifname = ParentDir + NAME[n] + "/Matrix" + NAME[n] + ".txt"
    ofname = ParentDir + NAME[n]
    Af = np.loadtxt(ifname,dtype="int")
    # rprom = np.zeros_like(J)
    loop+=1
    sys.stdout.write("Dynamic profile --> %s     progress: %.2f %%\n"%(NAME[n],(100.0*(loop))/(len(IDselected))))
    sys.stdout.flush()

    #-----------------------------------
    # Initializing some needed variables
    #-----------------------------------
    #nactives = np.zeros(ntimes)
    J0 = JC[n] - PERCENT*JC[n]#JC[n] - DsgmJ*SIGMAJ[n]

    # LOOP FOR TO CREATE SAMPLE NEEDED FOR BOOSTSTRAP METHOD  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for irep in xrange(SENSPRM['Nrep']):
        nactives = np.zeros(ntimes)

        #-----------------------------------
        # Determining stability curve
        #-----------------------------------
        stb.DynamicSensibilityOneP(Af,SENSPRM,J0,nrs_nonstimulated,nactives)

        nactives_spontaneous = nactives[Tsp_min:Tsp_max]
        nactives_stimulated = nactives[Tsp_max:]

        # Counting number of actives neurons -----------------------------------------------------------------------------------------
        NcountSp = np.histogram(nactives_spontaneous,bins=range(Size))[0]
        NcountSt = np.histogram(nactives_stimulated,bins=range(Size))[0]

        # Determining the xmax -------------------------
        argXmaxSp = np.argwhere(NcountSp>0)[-1,-1]
        argXmaxSt = np.argwhere(NcountSt>0)[-1,-1]
        argxmax = argXmaxSp + 4 if argXmaxSp > argXmaxSt else argXmaxSt + 4
        # Determining the xmin -------------------------
        argXminSp = np.argwhere(NcountSp>0)[0,0]
        argXminSt = np.argwhere(NcountSt>0)[0,0]
        argxmin = argXminSp if argXminSp < argXminSt else argXminSt
        argxmin = argxmin - 4 if argxmin - 4>=0 else 0

        #*****************************************************************************************************************
        # Fit to gaussian distribution
        #*****************************************************************************************************************
        # -----------------------------------------------------------------------------------
        # # Determining the zone where the number of active nodes is less than the 30% of total nodes
        nspontaneous = nactives_spontaneous[ nactives_spontaneous < 0.3*Size ]
        nstimulated = nactives_stimulated[ nactives_stimulated < 0.3*Size ]
        try:
            xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2
        except ValueError:
            nspontaneous = nactives_spontaneous.copy()
            nstimulated = nactives_stimulated.copy()
            xmax = nstimulated.max() + 2 if nstimulated.max() > nspontaneous.max() else nspontaneous.max() + 2


        xsp = np.linspace(argxmin, xmax, len(nspontaneous)) # spontaneous
        xst = np.linspace(argxmin, xmax, len(nstimulated)) # stimulated
        mean_sp, sigma_sp = stats.norm.fit(nspontaneous) # get mean and standard deviation
        mean_st, sigma_st = stats.norm.fit(nstimulated) # get mean and standard deviation
        pdfSp = stats.norm.pdf(xsp, mean_sp, sigma_sp) # get theoretical values in our interval
        pdfSt = stats.norm.pdf(xst, mean_st, sigma_st) # get theoretical values in our interval
        # Saving values in array to do boostsrapping
        meansp_arr[irep] = mean_sp
        meanst_arr[irep] = mean_st
    # END FOR LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    # MAKING BOOTSTRAPPING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # >>> spontaneous
    meansp_err_min, meansp_err_max, meansp_err[n] = bootstrap(meansp_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
    stdsp_err_min, stdsp_err_max, stdsp_err[n] = bootstrap(meansp_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

    # >>> stimulated
    meanst_err_min, meanst_err_max, meanst_err[n] = bootstrap(meanst_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
    stdst_err_min, stdst_err_max, stdst_err[n] = bootstrap(meanst_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

    # >>> Differences between means
    diffmean_err_min,diffmean_err_max,diffmean_err = bootstrap(meanst_arr-meansp_arr,PrmBst['nsample'],np.mean,PrmBst['alpha'])
    diffstd_err_min,diffstd_err_max,diffstd_err = bootstrap(meanst_arr-meansp_arr,PrmBst['nsample'],np.std,PrmBst['alpha'])

    # END BOOTSTRAPPING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    #------------------------------------------------------------------------
    # SOME VALUES WITH BOOTSTRAP RESULTS

    # >>> Delta of mean values obtained by bootsrapping
    DiffMeanBS[n] = meanst_err[n] - meansp_err[n]
    # >>> Error of Delta of mean values obtained by bootsrapping
    DiffMeanBS_err[n] = np.sqrt(stdst_err[n]**2 + stdsp_err[n]**2)
    # >>> Mean of Delta of means
    MeanDiffMean[n] = diffmean_err
    # >>> Error of Mean of Delta of means
    MeanDiffMean_err[n] = diffstd_err
    # print n,diffstd_err
    #------------------------------------------------------------------------

    # np.savetxt("%s/MFRS_vs_J-h%d.dat"%(ofname,idx),np.array([time,nactives]).T,fmt="%10f")
    #np.savetxt("%s/NneuronsMean_spontaneous.dat"%(ofname),np.array([time[Tsp_min:Tsp_max],nactives_spontaneous]).T,fmt="%10f")
    #np.savetxt("%s/NneuronsMean_stimulated.dat"%(ofname),np.array([time[Tsp_max:],nactives_stimulated]).T,fmt="%10f")

    #*************************************
    # Plot histogram curves
    #*************************************

    Bins = range(Size)
    plt.figure()
    NcountSp = plt.hist(nactives_spontaneous,bins=Bins,histtype='stepfilled',label="spontaneous",alpha=0.4,normed=True , color = 'g')[0]
    NcountSt = plt.hist(nactives_stimulated,bins=Bins,histtype='stepfilled',label="Stimulated",alpha=0.4,normed=True , color = 'b')[0]
    #NcountSp = plt.hist(nactives_spontaneous,bins=range(len(Af)),histtype='stepfilled',label='spontaneous',alpha=0.4,normed=True, color='g')[0]
    #NcountSt = plt.hist(nactives_stimulated,bins=range(len(Af)),histtype='stepfilled',label='stimulated',alpha=0.4,normed=True , color='b')[0]

    # Plotting error BAR---------------------------------------------------------------------
    plt.errorbar(meansp_err[n],np.max(pdfSp)*1.1,xerr=3*stdsp_err[n],fmt='o',ms=10,mfc='k',mec='k',
                                              elinewidth=3,ecolor='k',capthick=3,capsize=5)
    plt.errorbar(meanst_err[n],np.max(pdfSt)*1.1,xerr=3*stdst_err[n],fmt='o',ms=10,mfc='k',mec='k',
                                              elinewidth=3,ecolor='k',capthick=3,capsize=5)


    # Plotting the fit -----------------------------------------------------------------------------------------
    plt.plot(xsp, pdfSp,'-',color='g',lw=2)
    #plt.plot(xsp[np.argmax(pdfSp)],np.max(pdfSp),'o',color='black')
    plt.plot(xst, pdfSt,'-',color='b',lw=2)
    plt.ylim(ymax = np.round(1.1*np.max([np.max(pdfSp)*1.1, np.max(NcountSp)]),1) )


    plt.xlim(xmax=xmax,xmin=argxmin)
    plt.grid(which='both')
    plt.xlabel("Active Neurons",fontsize=15)
    plt.ylabel("Normalized Counts ",fontsize=15)
    leg = plt.legend(loc='best')
    plt.title("profile %s"%NAME[n].replace("_"," "))

    plt.savefig("%s/Sensitivity_histo.pdf"%(ofname))
    plt.close()

  # SAVING DATA IN ARRAY -------------------------------------------------------------------------
  #STAT = np.array(zip(NAME,JC-DsgmJ*SIGMAJ,mean_sp,mean_st,sigma_sp,sigma_st),dtype=[\
    #('name','S14'),('J0','f8'),('meanSp','f8'),('meanSt','f8'),('sigmaSp','f8'),('sigmaSt','f8')])
  STAT = np.array(zip(NAME[IDselected],JC[IDselected]-PERCENT*JC[IDselected],
                      meansp_err[IDselected],meanst_err[IDselected],
                      stdsp_err[IDselected],stdst_err[IDselected],
                      DiffMeanBS[IDselected],MeanDiffMean[IDselected],
                      DiffMeanBS_err[IDselected],MeanDiffMean_err[IDselected]),dtype=[\
                      ('name','S14'),('J0','f8'),
                      ('meansp_err','f8'),('meanst_err','f8'),
                      ('stdsp_err','f8'),('stdst_err','f8'),
                      ('DiffMeanBS','f8'),('MeanDiffMean','f8'),
                      ('DiffMeanBS_err','f8'),('MeanDiffMean_err','f8')])
  Ofile = open("%sSens-statistic_selected.txt"%(ParentDir),"a")
  np.savetxt(Ofile,STAT,fmt="%14s %10f %10f %10f %10f %10f %10f %10f %10f %10f")
  Ofile.close()
  # np.savetxt("%sSens-statistic_selected.txt"%(ParentDir),STAT,fmt="%14s %10f %10f %10f %10f %10f %10f %10f %10f %10f",\
  # header="# PROFILE   J0   meanSp   meanSt  sigmaSp  sigmaSt DiffMeanBS  MeanDiffMean DiffMeanBS_err MeanDiffMean_err ")
