#from distutils.core import setup
#from Cython.Build import cythonize
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import os,sys

argv = sys.argv
#Name = argv[1]
#setup(name="stability", ext_modules=cythonize('Stability.pyx'))

ext_modules=[ Extension("stability",
              ["Stability.pyx"],
              libraries=["m"],
              extra_compile_args = ["-ffast-math"])]

setup(
  name = "stability",
  cmdclass = {"build_ext": build_ext},
  ext_modules = ext_modules)