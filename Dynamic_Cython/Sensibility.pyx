from cython.view cimport array as cvarray
import numpy as np
from libc.math cimport exp,sqrt,pow,log

import os,sys

#==============
# SHORCUTS
#==============
system=os.system
argv = sys.argv


#=========================================
# Probability function for neuron activity
# - Stochastic mode
#=========================================
def Firing_Probability(x,h0,J,pc,A):
  N = len(x)
  nu_new = np.zeros(N)
  fc = J/(N*pc)
  nu_new = 1.0/( 1.0 + np.exp( h0 - fc*np.dot(A,x) ) )
  return nu_new

#=========================================
# CYTHON
# Probability function for neuron activity
# - Stochastic mode
#=========================================
def FiringProb(long [::1] X, double h0, double J, double pc, long [:,::1] A):

  cdef N = len(X)
  cdef double [::1] Nu = np.zeros(N)
  cdef int i,j
  cdef double Sum
  cdef double fc = J/(N*pc)

  for i in xrange(N):
    Sum = 0.0
    for j in xrange(N):
      Sum += A[i,j]*X[j]

    Nu[i] = 1.0/( 1.0 + exp( h0 - fc*Sum ))

  return Nu

#=========================================
# CYTHON
# Dynamic stability for one profile
# - Stochastic mode
#=========================================
cpdef void DynamicSensibilityOneP(A, Params, double J, neurons_nonstimulated, NactMean):
  #*********************************************************************
  #                    GENERAL DECLARATIONS
  #*********************************************************************
  cdef:
    int i,j,k,t=0
    int row,column
    int sumNrs

  #*********************************************************************
  #                    ASSIGMENT INITIAL PARAMETERS
  #*********************************************************************

  #Filename of the Adjacency matrix
  #fileini = Params["filename"]

  # Probability of conection
  cdef double pc = Params["pc"]

  # Size of timebin(DT)
  cdef double Dt = Params["Dt"]

  # baseline firing rate
  cdef double r0 = Params["r0"]

  # Number of repetitions
  cdef long Nr = Params["Nr"]

  # Number of Initial active neurons
  cdef long NiAct = Params["NiActv"]

  # Number of stimulated neurons
  cdef long Nstim = Params["Nstim"]

  # Number of columns to calculate firing rate
  cdef long ncol = Params["ncol"]

  # Stability Time
  cdef long Tstb = Params["Tstb"]

  # Size of spontaneous temporal windows
  cdef long Tsp = Params["Tsp"]

  # Size of stimulated temporal windows
  cdef long Tst = Params["Tst"]

  # Total time
  cdef long Ntimes = Tstb + Tsp + Tst

  ##*********************************************************************
  ##     DEFINING, CALCULATING AND READING SOME PARAMETERS/VARIABLES
  ##*********************************************************************

  # Baseline probability of firing.
  cdef double h0 = log(1.0/(r0*Dt)-1)


  # Reading Adjacency matrix
  cdef long [:,::1] Ai = A #np.loadtxt(fileini,dtype=np.int)
  cdef unsigned int Size = len(Ai)

  # Initial random activity of neurons
  xin = np.zeros(Size,dtype=np.int)
  loc = np.random.randint(Size,size=NiAct)
  xin[loc] = 1
  cdef long [::1] xi = xin[:] #making a copy to cython array

  # Initialiting array of rasterplot
  #cdef long [:,::1] rasterplot = np.zeros((Size,Ntimes),dtype=np.int)
  #for i in range(Size):
    #rasterplot[i,0] = xi[i]

  # Defining array of firing rate
  #cdef double [::1] FrRate = np.zeros(len(J))
  #cdef double [::1] rprom = np.zeros(len(J))

  # Defining array of Actives neurons per bin
  cdef long [::1] Nactives = np.zeros(Ntimes,dtype='int')



  #*********************************************************************
  #                 CALCULATING THE RASTERPLOT
  #*********************************************************************
  cdef double [::1] Threshold
  cdef double [::1] nu
  cdef long [::1] x = xi
  cdef double sumFR
  cdef double ft = 1.0/Nr

  Nactives[0]  = np.sum(x)

  for nr in range(Nr): # For of repetitions
    for t in range(1,Ntimes):
      # Calculating the firing probability
      nu = FiringProb(x,h0,J,pc,Ai)

      # nu = Firing_Probability(x,h0,J[j],pc,Ai)

      # Generating uniform random number to determine if
      # neurons spike or not
      Threshold = np.random.rand(Size)

      #Spike = nu >= Threshold
      for k in range(Size):
        #Final State
        if nu[k] >= Threshold[k]:
          x[k] = 1
        else:
          x[k] = 0


      # Save Number of actives Neurons

      # Save the activity in rasterplot
      #rasterplot[k,t] = x[k]

      # Time between Tstability and Tspontaneous
      if t>=Tstb and t < Tstb + Tsp:
        Nactives[t] = np.sum(x)
      elif t >= Tstb + Tsp:
        sumNrs = 0
        for k in range(Size):
          if neurons_nonstimulated[k]:
            sumNrs = sumNrs + x[k]
          else:
            x[k] = 1
        Nactives[t] = sumNrs
      else:
        Nactives[t] = np.sum(x)
      NactMean[t] = NactMean[t] + ft*Nactives[t]
      #rprom = rprom + r
      #-------------------------------------------
      # Calculating the Firing Rate
      #-------------------------------------------
      #sumFR = 0.0
      #for row in range(Size):
        #for column in range(-ncol,0):
          #sumFR += rasterplot[row,column]
      ##FrRate[j] = sumFR/(Size*ncol*Dt)
      ##rprom[j] += ft*sumFR/(Size*ncol*Dt)


  #print t
  ##"""
