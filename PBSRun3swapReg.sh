#!/bin/sh

# set name of job
#PBS -N 3swapReg

# set name of output and error file
#PBS -o log3swapReg.out
#PBS -e log3swapReg.err

# send mail to this address
#PBS -M alfredo.uribe@udea.edu.co

# mail alert at start, end and abortion of execution
#PBS -m bea

# start job from the directory it was submitted
cd $PBS_O_WORKDIR

python ./RunProfiles3swap.py 100 ./Profiles3swap/ ./data/MatrixN100.txt > OutRun3swapReg.txt
