#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#=================================================================================================================
# initial files
#=================================================================================================================
filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
DirProfiles = Argv[1]
fileini = DirProfiles + "MatrixInitial.txt"
Ai = np.loadtxt(fileini)
Size = len(Ai)

folders = np.loadtxt(filenames[0],dtype=str)
profiles = np.loadtxt(filenames[2],dtype=str)
Nprofiles = len(folders)

#=================================================================================================================
# Creating digraph in networkx
#=================================================================================================================
Di = nx.DiGraph(Ai)
print "INITIAL MATRIX"
print "Is weakly Connected?: ",nx.is_weakly_connected(Di)
print "Is strongly Connected?: ",nx.is_strongly_connected(Di)
print "No of strongly connected components: ",nx.number_strongly_connected_components(Di)
print "No of weakly connected components: ",nx.number_weakly_connected_components(Di)

#=================================================================================================================
# Creating digraph in networkx for each profiles
#=================================================================================================================
Test = list()
for i in xrange(Nprofiles):
    #print "percentage %f"%((100.0*i)/Nprofiles)
    File = DirProfiles + folders[i] + "/Matrix" + folders[i] + ".txt"
    Af = np.loadtxt(File,dtype='int')
    Df = nx.DiGraph(Af)
    Test.append([folders[i],
                 nx.is_weakly_connected(Df),
                 nx.is_strongly_connected(Df),
                 nx.number_weakly_connected_components(Df),
                 nx.number_strongly_connected_components(Df)])
    print Test[i]
