#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Allvars.h"
#include "Prototypes.h"

//===========================
// DYNAMIC
//===========================
void FiringProb(int *X, double h0, double J, double pc, int *AIJ, double *Nu){
  int N;
  int i,j;
  double Sum,fc;
  
  N = Netwk.Nnodes;
  fc = J/(N*pc);
  
  
  // Firing probability function calculated in state t+1 from state t
  for(i=0;i<N;i++){
    Sum = 0.0;
    for(j=0;j<N;j++){
      Sum += AIJ[i*Nc + j]*X[j];
    }
    Nu[i] = 1.0/(1.0 + exp(h0 - fc*Sum));
  }  
}


void get_rnd_uniform_vector(double *Vec,int size){
  int i;
  for(i=0;i<size;i++){
    Vec[i] = gsl_rng_uniform(rand_generator);
  }
}

void active_initial_neurons(int *xin, int size, int Nact){
  int i;
  int nrnd;
  for(i=0;i<Nact;i++){
    nrnd = gsl_rng_uniform_int(rand_generator,size);
    xin[nrnd] = 1;
  }
}

void copy_vector(int *xi, int *xf,int size){
  int i;
  for(i=0;i<size;i++){
    xf[i]= xi[i];
  }
}

// =========================================
//  DYNAMIC STABILITY FOR ONE PROFILE
//  - STOCHASTIC MODE 
// =========================================
void DynamicSensibilityOneP( int *AIJ, //Initial Matrix
			   struct Params prm, //Structure with basic parameters
			   double *J, // Overall coupling strength
			   int SizeJ, // Number of elements of J array
			   double *rprom)
{
  //*********************************************************************
  //                    GENERAL DECLARATIONS
  //*********************************************************************
  int i,j,k,t,nr,ncol,nrd;
  int row,column;
  int *loc;
  int *xin,*raster;
  double *FrRate;
  
  //*********************************************************************
  //     DEFINING, CALCULATING AND READING SOME PARAMETERS/VARIABLES
  //*********************************************************************
  
  // Baseline probability of firing.
  double h0 = log(1.0/(prm.r0*prm.Dt) - 1);
  
  // Size of Network
  int Size = Netwk.Nnodes;
  
  
  // Initial activity vector
  xin = (int *)calloc(Size,sizeof(int));
  
  // Initial random activity of neurons
  active_initial_neurons( xin, Size, prm.NiAct); //CONSTRUIR
  //RndIntVector(loc,prm.NiAct);
  
  // Allocating rasterplot vector
  raster = (int *)calloc(Size*prm.Ntimes,sizeof(int));
  // Saving Initial state
  for(i=0;i<Size;i++) { raster[i*prm.Ntimes + 0] = xin[i]; }
  
  
  
  
  
  //*********************************************************************
  //                 CALCULATING THE RASTERPLOT
  //*********************************************************************
  double Threshold[Size];
  double nu[Size];
  int x[Size];
  double SumFR;
  double ft = 1.0/prm.Nr;

  // Copy vector
  copy_vector(xin,x,Size);
  
  for(nr=0;nr<prm.Nr;nr++){
    printf("Repetion %d --- ",nr);
    for(j=0;j<SizeJ;j++){
      for(t=1;t<prm.Ntimes;t++){
	  
	// Calculating the firing probability
	FiringProb(x,h0,J[j],prm.pc,AIJ,nu);
	
	// Generating uniform random number to determine if
	// neurons spike or not
	get_rnd_uniform_vector(Threshold,Size);
	
	for(k=0;k<Size;k++){
	  Threshold[k] = gsl_rng_uniform(rand_generator);
// 	  printf("nrd:%e\n",Threshold[k]);
	  // Final State
	  if( nu[k] >= Threshold[k] ) { x[k] = 1; }
	  else {x[k] = 0;}
	  //Save activity in rasterplot
	  raster[k*prm.Ntimes + t] = x[k];
	   
	}
      }
      //-------------------------------------------
      // Calculating the Firing Rate
      //-------------------------------------------
      SumFR = 0.0;
      ncol = prm.Ntimes - prm.ncol ;
      for( column=ncol; column < prm.Ntimes; column++ ){
	for(row=0;row<Size;row++){
	  SumFR += raster[row*prm.Ntimes + column];
	}
	  
      }
//       printf("Repetion %d ---  j: %d final:%d\n",nr,j,SizeJ-1);
      rprom[j] = rprom[j] + ft*SumFR/(1.0*Size*prm.ncol*prm.Dt);
    }
  }
  
}

void file2dump( char filename[] )
{
    char cmd[256];
    sprintf(cmd,"grep -v \"\%\" %s | grep -v \"^$\" > %s.dump",filename,filename);
//     sprintf( cmd, "grep -v \"#\" %s > %s.dump", 
// 	     filename, filename );
    system( cmd );
}

void read_conf_dynamicfile(struct Params *prm, char filename[])
{
  int i=0, j=0;
  char cmd[256],filenamedump[256];
  FILE *ifile;
  
  //File Detection
  ifile = fopen( filename, "r" );
  if( ifile==NULL ){
    printf( "  * The file '%s' don't exist!\n", filename );}
  fclose(ifile);
  
  //Conversed to plain text
  file2dump( filename );
  sprintf( filenamedump, "%s.dump", filename );
  ifile = fopen( filenamedump, "r" );
  
  //Reading
//   do{
    fscanf(ifile,"%lf %lf %lf %d %d %d %d %lf %lf %lf",&prm->pc, &prm->Dt, &prm->r0, 
					   &prm->Ntimes, &prm->Nr, &prm->NiAct, &prm->ncol,
					   &prm->jmin, &prm->jmax, &prm->jstep);
//   }while(getc( ifile ) != EOF);
  fclose( ifile );
//   printf("in function:\n");
//   fprintf(stdout,"%lf %E %lf %d %d %d %d\n",prm->pc, prm->Dt, prm->r0, 
// 	   prm->Ntimes, prm->Nr, prm->NiAct, prm->ncol);
  sprintf( cmd, "rm -rf %s.dump", filename );
  system( cmd );
}