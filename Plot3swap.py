import matplotlib.pyplot as plt
import numpy as np
import sys,os
import commands as cm

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

Option = Argv[1]

if Argv[1] == "ONEPROFILE":
  AssorFile = Argv[2]
  title = Argv[3]
  NamePlot = Argv[4]
  
  
  vals = np.loadtxt(AssorFile)
  rw = vals[:,0]
  Rho = vals[:,1:]
  Label = ["In-out","Out-In","In-In","Out-Out"]
  
  plt.figure(figsize=(8,6))
  for mode in xrange(4):
    plt.plot(rw,Rho[:,mode],'-',label=Label[mode])
  plt.axhline(y=Rho[0,1],ls='--',color='k')
  plt.axhline(y=0.0,ls='--',color='k')
  plt.xlabel("Number of rewirings")
  plt.ylabel("Assortativity")
  plt.ylim(ymax=1)
  plt.title(title)
  plt.grid()
  plt.legend(loc="best")
  plt.savefig(NamePlot)
  #plt.savefig("./3swap/Profiles_R20e6_N100_2swap.pdf")
  plt.show()
  
if Argv[1] == "TWOPROFILE":
  AssorFile1 = Argv[2]
  AssorFile2 = Argv[3]
  NamePlot = Argv[4]
  
  vals1 = np.loadtxt(AssorFile1)
  rw1 = vals1[:,0]
  Rho1 = vals1[:,1:]
  
  vals2 = np.loadtxt(AssorFile2)
  rw2 = vals2[:,0]
  Rho2 = vals2[:,1:]
  
  # To shift rw2 of profile 2 in order to joint with profile 1
  rw2shifted = rw1[-1] + rw2
  
  Label = ["In-out","Out-In","In-In","Out-Out"]
  Color = ["red","green","blue","cyan"]
  
  for mode in xrange(4):
    plt.plot(rw1,Rho1[:,mode],'-',color=Color[mode])
    plt.plot(rw2shifted,Rho2[:,mode],'-',label=Label[mode],color=Color[mode])
  plt.axhspan(ymin=Rho2[0,1] - 0.01*np.fabs(Rho2[0,1]),ymax=Rho2[0,1] + 0.01*np.fabs(Rho2[0,1]),xmin=0.5,color="g",alpha=0.3)
  plt.xlabel("Number of rewirings")
  plt.ylabel("Assortativity")
  plt.title("Profile (0,-1,-1,-1)+(-1,0,0,0)")
  plt.ylim([-1,1])
  plt.yticks(np.arange(-1,1.1,0.2))
  plt.axvline(x=rw1[-1],ymin=-1,ymax=1,color='k',lw=1.3,linestyle='--')
  plt.grid()
  plt.legend(loc="best")
  plt.savefig(NamePlot)
  plt.show()
  
  
if Argv[1] == "PROFCMP":
  rho = np.zeros((4,3))
  filename = Argv[2]
  vals = np.loadtxt(filename)
  vals = vals.T
  rho = vals[1:,]
    
  Label = ["In-out","Out-In","In-In","Out-Out"]
  #Color = ["red","green","blue","cyan"]
  x = range(4)
  plt.figure()
  plt.plot(x,rho[:,0],'g^--',label='2swap',markersize=3,linewidth=2)
  plt.plot(x,rho[:,1],'ro--',label='3swap',markersize=3,linewidth=2)
  plt.plot(x,rho[:,2],'bs--',label='3+2swap',markersize=3,linewidth=2)
  plt.xticks(x,Label,fontsize=18)
  plt.yticks(fontsize=14)
  plt.ylabel("Assortativity")
  plt.title("Final values")
  plt.xlim(xmin=-1,xmax=4)
  plt.ylim(ymin=0,ymax=1)
  plt.legend(fontsize=12)
  plt.grid()
  plt.savefig("./3swap/FinalValues_R3e6_N100_joined.pdf")
  plt.close()
  
if Argv[1] == "ALL":
  filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']
  
  #Reading name of profiles
  NAME = np.loadtxt(filenames[0],dtype="|S14")

  # Total number of profiles
  Ntotal = len(NAME)
  
  #profiles folder
  DirProfiles = Argv[2]
  
  #Label
  Label = ["In-out","Out-In","In-In","Out-Out"]
  
  print "*"*70
  for i in xrange(Ntotal):
    
    fileassort = DirProfiles + NAME[i] + "/Assort" + NAME[i] + ".txt"
    ofname = DirProfiles + NAME[i]
    
    data = np.loadtxt(fileassort)
    rw = data[:,0]
    rho = data[:,1:]
    del data
    
    #***********************************************
    # Plot of assortativity vs effective rewirings
    #***********************************************
    plt.figure()
    for j in xrange(0,4):
      plt.plot(rw,rho[:,j],'-', label=Label[j], lw=1.4, ms=7)
    plt.grid()
    plt.xlabel("Number of rewiring")
    plt.ylabel("Assortativity")
    plt.legend(loc="best",fontsize=13)
    plt.title("Profile %s"%(NAME[i].replace("_"," ")),fontsize=15)
    plt.savefig("%s/Profile%s.pdf"%(ofname,NAME[i]))
    plt.close()
      
    print "\n--------> Profile %s  --  progress: %.2f %%"%(NAME[i],(1.0*i)/(Ntotal-1)*100)
    
  print "\n"+"*"*70
