#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *

#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#=================================================================================================================
# initial files
#=================================================================================================================
#filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']
DirProfiles = Argv[1]
fileini = DirProfiles + "MatrixInitial.txt"
Ai = np.loadtxt(fileini)
Size = len(Ai)

folders = np.loadtxt(filenames[0],dtype=str)
profiles = np.loadtxt(filenames[2],dtype=str)
Nprofiles = len(folders)

#==================================================================================================
# 
#==================================================================================================
print"-> Creating graph with networkx and obtaining normalized laplacian matrix..."

# Graph digraph-type in networkx
Di = nx.DiGraph(Ai)
# Normalized laplacian matrix - initial matrix
Li = nx.directed_laplacian_matrix(Di)

#---------------------------------------------------------------
# Initializating variables
#---------------------------------------------------------------

print "-> Get eigenvalues of initial matrix ..."

# Initializing values for eigenvalues
EigI = la.eigvals(Li)
MaxEigI = np.sort(EigI.real)[1]
MAXEIG = np.array([])
#MAXEIG = np.append(MAXEIG,MaxEigI)
MAXEIGi = MaxEigI*np.ones(Size)



"""
# CALCULATION FOR EACH PROFILE
print "-> Get eigenvalues of Final matrix ..."
for i in xrange(len(folders)):
  print "-> Profile  %s... i:%d"%(folders[i],i)
  Dir = DirProfiles + folders[i]
  File = Dir + "/Matrix" + folders[i] + ".txt"
  Af = np.loadtxt(File)
  Df = nx.DiGraph(Af)
  # Normalized laplacian matrix - final matrix
  try:
    Lf = nx.directed_laplacian_matrix(Df)
    Eigvals = la.eigvals(Lf) # only eigenvalues
    MAXEIG = np.append(MAXEIG,np.sort(Eigvals.real)[1])
  except:
    MAXEIG = np.append(MAXEIG,[0.0])
    print "nan in Df %s with i:%d"%(folders[i],i)
    pass
  
  
  
  
#-----------------------------------------------------------------------------
# Saving file with the corresponfing maximun eigenvalue of each profile
#-----------------------------------------------------------------------------
print "-> Saving data of eigenvalues..."
data = np.array(zip(folders,MAXEIGi,MAXEIG),dtype=[('folders','S14'),('MAXEIGi','float64'),('MAXEIG','float64')])
np.savetxt(DirProfiles + 'Lambda2-profiles.txt', data, fmt=["%15s"] + ["%15.10f",]*2)
system("sort -n -k 3 %s -o %s"%(DirProfiles + 'Lambda2-profiles.txt',DirProfiles + 'Lambda2-Profiles-sorted.txt'))


"""