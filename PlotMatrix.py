import matplotlib.pyplot as plt
import os,sys
import math
import commands as Cm
import scipy.linalg as la
from mymodules import *
from matplotlib import cm
import matplotlib as mpl


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GENERAL PARAMETERS OF PLOT
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mpl.rcParams['xtick.labelsize'] = 17
mpl.rcParams['ytick.labelsize'] = 17
mpl.rcParams['font.family'] = 'Freeserif' #cmr10
#mpl.rcParams['xtick.minor.size'] = 3
mpl.rcParams['font.size'] = 23.0

#==============
# SHORCUTS
#==============
system=os.system
get=Cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

DirName = Argv[1]
DirProfile = Argv[2]
split=DirProfile.split("/")



Ai = np.loadtxt("%sMatrixInitial.txt"%DirName)
Af = np.loadtxt("%sMatrix%s.txt"%(DirProfile,split[-2]))

Edgesi = GetList(Ai)
Edgesf = GetList(Af)
din,dout = GetDegree(Ai)
colorsi=dout[Edgesi[:,0]] + dout[Edgesi[:,1]]
colorsf=dout[Edgesf[:,0]] + dout[Edgesf[:,1]]

plt.figure(figsize=(16,14))
plt.scatter(Edgesi[:,0],Edgesi[:,1],c=colorsi,s=6,edgecolor='none')
plt.colorbar()
#plt.plot(Edgesi[:,0],Edgesi[:,1],'o',color=(.38,.80,.98),ms=1,mec=(0.03, 0.62, 0.87))
plt.xlabel("Row")
plt.ylabel("Column")
plt.xlim([0,500])
plt.ylim([0,500])
plt.title("Initial Matrix")
plt.savefig("%sInitialMatrix.pdf"%DirName)

plt.figure(figsize=(16,14))
plt.scatter(Edgesf[:,0],Edgesf[:,1],c=colorsf,s=6,edgecolor='none')
plt.colorbar()
plt.xlabel("Row")
plt.ylabel("Column")
plt.xlim([0,500])
plt.ylim([0,500])
plt.title("Profile %s - Final Matrix"%split[-2])
plt.savefig("%sFinalMatrix%s.pdf"%(DirName,split[-2]))

#plt.show()
