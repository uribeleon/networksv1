#---codigo para rewiring usando IGRAPH....solo incrementa el rio

import numpy as np
import matplotlib.pyplot as plt
#from igraph import *
import networkx as nx
import igraph as ig

# Reading initial adjacency matrix
fileini = "./data/MatrixInitial.txt"
A = np.loadtxt(fileini)

#gr1=Graph()
#gr1=Graph.Read("rat1.graphml",format="graphml")
Gig=ig.Graph.Adjacency(A.tolist())
R=np.array([])
rw = np.array([])

ri=Gig.assortativity(Gig.degree(mode="in"),Gig.degree(mode="out"),directed=True)
R=np.append( R, [ri])
rw = np.append(rw,[0])



#------elimina multiples y selflopps
#gr1.simplify(multiple=True, loops=True, combine_edges="ignore")

for i in range(0,50000):
  G2=Gig.copy()
  G2.rewire(n=1,mode="simple")
  a=Gig.assortativity(Gig.degree(mode="in"),Gig.degree(mode="out"),directed=True)
  b=G2.assortativity(G2.degree(mode="in"),G2.degree(mode="out"),directed=True)
  if (b>a):
    Gig=G2.copy()
    R = np.append( R, [b])
    rw = np.append(rw,[i+1])

np.savetxt("./data/datosA.txt",np.transpose([rw,R]),fmt="%f")
#--------------------------------------------------------