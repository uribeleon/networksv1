#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
import numpy as np
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math
import commands as cm
import scipy.linalg as la
import networkx as nx
from mymodules import *


#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
Argv=sys.argv

#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
filenames = ['./data/names_selected.txt','./data/condiciones_selected.txt','./data/combinaciones_selected.txt']
#filenames = ['./data/names.txt','./data/condiciones.txt','./data/combinaciones.txt']

#profiles folder
DirProfiles = Argv[1]

# Reading initial adjacency matrix
print "="*100
print "-> Reading initial matrix ..."
fileini = DirProfiles + "MatrixInitial.txt"
Ai = np.loadtxt(fileini)
Size = len(Ai)

folders = np.loadtxt(filenames[0],dtype=str)
profiles = np.loadtxt(filenames[2],dtype=str)




#==================================================================================================
# GET EIGENVALUES AND ENTROPY FOR EACH PROFILE
#==================================================================================================

print"-> Creating graph with networkx and obtaining normalized laplacian matrix..."

# Graph digraph-type in networkx
Di = nx.DiGraph(Ai)
# Normalized laplacian matrix - initial matrix
Li = nx.directed_laplacian_matrix(Di)


#---------------------------------------------------------------
# Initializating variables
#---------------------------------------------------------------

print "-> Get eigenvalues of initial matrix ..."

# Initializing values for eigenvalues
EigI = la.eigvals(Li)
MaxEigI = np.max(EigI.real)
MAXEIG = np.array([])
MAXEIG = np.append(MAXEIG,MaxEigI)
MAXEIGi = MaxEigI*np.ones(Size)


print "-> Get initial values of entropy..."

# Initializing values for entropy
SiE = VNEntropy_directed_exact(EigI.real,Size)*np.ones(len(folders)) # Exact form
SiN = VNEntropy_directed(Ai)*np.ones(len(folders))  # regular form
SiNj = VNEntropy_directed(Ai,1)*np.ones(len(folders))  # regular form - Jvn
SfE = np.array([])
SfN = np.array([])
SfNj = np.array([])


#---------------------------------------------------------------
# CALCULATION FOR EACH PROFILE
#---------------------------------------------------------------
print "-> Calculating entropy and eigenvalues for each profile..."
for i in xrange(len(folders)):
  Dir = DirProfiles + folders[i]
  File = Dir + "/Matrix" + folders[i] + ".txt"
  Af = np.loadtxt(File)
  Df = nx.DiGraph(Af)
  # Normalized laplacian matrix - final matrix
  Lf = nx.directed_laplacian_matrix(Df)
  Eigvals = la.eigvals(Lf) # only eigenvalues
  
  SfE = np.append(SfE,VNEntropy_directed_exact(Eigvals.real,Size))
  SfN = np.append(SfN,VNEntropy_directed(Af))
  SfNj = np.append(SfNj,VNEntropy_directed(Af,1)) # Jvn
  MAXEIG = np.append(MAXEIG,np.max(Eigvals.real))



#-----------------------------------------------------------------------------
# Saving file with the corresponfing maximun eigenvalue of each profile
#-----------------------------------------------------------------------------
print "-> Saving data of eigenvalues..."
data = np.array(zip(folders,MAXEIGi,MAXEIG),dtype=[('folders','S14'),('MAXEIGi','float64'),('MAXEIG','float64')])
np.savetxt(DirProfiles + 'MaxEigVal-profiles.txt', data, fmt=["%15s"] + ["%15.10f",]*2)

system("sort -n -k 3 %s -o %s"%(DirProfiles + 'MaxEigVal-profiles.txt',DirProfiles + 'MaxEigVal-Profiles-sorted.txt'))

#-----------------------------------------------------------------------------
# Saving file with the corresponfing entropy of each profile
#-----------------------------------------------------------------------------
print "-> Saving data of entropies..."
#types = [(('folders','S14'),('SiE','float64'),('SfE','float64'),('SiN','float64'),('SfN','float64'),
	  #('SiNj','float64'),('SfNj','float64'))]
TABLE = np.array(zip(folders,SiE,SfE,SiN,SfN,SiNj,SfNj),dtype=[('folders','S14'),
								('SiE','float64'),('SfE','float64'),
								('SiN','float64'),('SfN','float64'),
								('SiNj','float64'),('SfNj','float64')])

np.savetxt(DirProfiles + 'Entropy-values.txt',TABLE,fmt=["%15s"] + ["%15.10f",]*6)


print "="*100