#==================================================================================================
# IMPORTING LIBRARIES
#==================================================================================================
from mymodules import *
from Colors import *
import commands as cm
import scipy as sp
import scipy.linalg as la
import scipy.optimize as so
import scipy.stats as st
import argparse
import textwrap


import matplotlib as mpl
from matplotlib.collections import LineCollection
from matplotlib.colors import colorConverter
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


parser=argparse.ArgumentParser(
  prog='Dynamic-TheoricCurves.py',
  formatter_class=argparse.RawDescriptionHelpFormatter,
  description=textwrap.dedent('''\
**********************************************************************************************************************
  This program plots the theoretical curves for dynamics of networks
    
  USAGE: python Dynamic-TheoreticalCurves.py <OPTIONS> <ARGS>
    
  where OPTIONS are:
    
   * ROOT  --> Plot the curves of root in the mean field limit
      
   * MFR-MF-ONER --> Plot the mean firing rate vs J for a specific baseline firing rate for
		     a infinite-size network
	>> EXAMPLE: python Dynamic-TheoricCurves.py MFR-MF-ONER # (where # is 0,1,2,3 for h0[#])
	    
    '''),epilog='''
**********************************************************************************************************************
  ''')


#=====================================
# GENERAL PARAMETERS OF PLOT
#=====================================
mpl.rcParams['xtick.labelsize'] = 17
mpl.rcParams['ytick.labelsize'] = 17
mpl.rcParams['font.family'] = 'Freeserif' #cmr10
mpl.rcParams['xtick.major.width'] = 2
mpl.rcParams['font.size'] = 23.0
mpl.rcParams['savefig.directory'] = "/home/cesar/workspace/Networks/Profiles/Sensibility"
mpl.rcParams['savefig.format'] = "pdf"
#mpl.rcParams['xtick.minor.width'] = 1
majorFormatter = FormatStrFormatter('%.2lf')
#majorLocator   = MultipleLocator(1)
#minorLocator = MultipleLocator(0.5)




#==========================================================================
# SOME FUNCTIONS
#==========================================================================
# Equation for Gaussian
def f(x, a, b, c):
    return a * np.exp(-(x - b)**2.0 / (2 * c**2))



#==============
# SHORCUTS
#==============
system=os.system
get=cm.getoutput
getdir=os.getcwd
chdir=os.chdir
argv = sys.argv

#***************************
# PARSER ARGUMENTS OPTIONS
#***************************
parser.add_argument('option', action="store")
parser.add_argument('-if',dest='ifile',action="store",default=0,help="path to initial file",type=str)
parser.add_argument('-v',dest='value', action="store",default=0,help='firing rate index',type=int)
parser.add_argument('-p',dest='pathDir',action="store",default=0,help="path of parent folder",type=str)
parser.add_argument('-j',dest='JJ', action="store",default=20.0,help='Strengh value of J',type=float)
args=parser.parse_args()

OPTION = args.option
PATHD = args.pathDir


#==================================================================================================
# GET PRELIMINAR VALUES
#==================================================================================================

# Some parameter files
filenames = ['./data/names_selected.txt',
	     './data/condiciones_selected.txt',
	     './data/combinaciones_selected.txt']


#if OPTION != "FR-STOCHASTIC-ONER-ALLP":
# Reading initial adjacency matrix
#fileini = "./data/MatrixInitial.txt"
fileini = PATHD + "MatrixInitial.txt"
#fileini = "./data/Matrix2000N_0001_1e6r.txt"
Ai = np.loadtxt(fileini)
Size = len(Ai)


#==================================================================================================
# STUDY THE DYNAMICS OF NETWORK
#==================================================================================================

pc = 0.1 # conection probability
r0 = np.array([1.0,2.0,3.0,4.0]) # Hz
Dt = 10E-3 # 10ms
h0 = np.log(1.0/(r0*Dt)-1)
#J = np.arange(0.0, 45,0.1)
#nu = 0.1*np.random.rand(Size)

if OPTION == "SENS-ONEP":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate 
  #-------------------------------------------------------------------------------
  Nneurons = 25
  Nactivated = 5
  ncol = 1
  ts = 50  # stable time
  dtime = 1000  # temporal window
  Ntimes = ts + 2*dtime
  J0 = args.JJ
  
  FRxbin1 =  np.zeros(Size,dtype=float) # Firing Rate per bin
  FRxbin2 =  np.zeros(Size,dtype=float) # Firing Rate per bin
  rp1 = np.zeros((Size,dtime),dtype=float)
  rp2 = np.zeros((Size,dtime),dtype=float)
  Nactives = np.zeros(Ntimes)
  Nnrs = np.arange(Size)
  Nnrs_mask = np.ones(Size,dtype="bool")
  
  #-------------------------------------------------
  # Get the index of neurons with higher out degree
  #-------------------------------------------------
  din,dout = GetDegree(Ai)
  idx = np.argsort(dout)
  Nnrs_mask[idx[-Nactivated:]] = False
  
  
  #---------------------------------------
  # Stablishing initial active neurons
  #---------------------------------------
  xi = np.zeros(Size)
  loc1 = np.random.randint(500,size=Nneurons)
  xi[loc1] = 1.0
  #xi[idx[-Nactivated:]]=1.0
  x1 = xi.copy()
  Nactives[0] = np.sum(xi)
  #rp1[:,0] = xi.copy()
  
  
  
  i = args.value
  
  ifile2 = args.ifile
  names = ifile2.split("/")[2]
  Af = np.loadtxt(ifile2)
  for t in xrange(1,Ntimes):
    # Calculating the firing probability
    nu1 = Firing_Probability(x1,h0[i],J0,pc,Af)
	
    # Generating uniform random number to determine if
    # neurons spike or not
    Threshold1 = np.random.rand(Size)
    Spike1 = nu1 >= Threshold1
    
    #Final state
    x1 = Spike1.astype(int)
    Nactives[t] = np.sum(x1)
	
    
    if t>=ts and t<ts+dtime:
      rp1[:,t-ts] = x1.copy()
      #x1 = Spike1.astype(int)  
      FRxbin1 = FRxbin1 + x1
      #FRxbin2[t] = np.sum(x2)/(Size*Dt)
    elif t>=ts+dtime:
      x1[idx[-Nactivated:]] = 1.0
      #FRxbin1[t] = (np.sum(x1)-Nneurons)/((Size-Nneurons)*Dt)
      FRxbin2 = FRxbin2 + x1
      Nactives[t] = np.sum(x1[Nnrs_mask])
  
  # Not including neurons with greater out-degree
  FRxbin2[idx[-Nactivated:]] = 1E-10
  
  # Calculating the median
  
  Median1 = np.median(Nactives[ts:ts+dtime])
  Median2 = np.median(Nactives[ts+dtime:ts+2*dtime])
  Mean1 = np.mean(Nactives[ts:ts+dtime])
  Mean2 = np.mean(Nactives[ts+dtime:ts+2*dtime])
  sigma1 = np.sqrt(np.var(Nactives[ts:ts+dtime]))
  sigma2 = np.sqrt(np.var(Nactives[ts+dtime:ts+2*dtime]))
  
  #Median1 = np.median(FRxbin1)
  #Median2 = np.median(FRxbin2[Nnrs_mask])
  #Mean1 = np.mean(FRxbin1)
  #Mean2 = np.mean(FRxbin2[Nnrs_mask])
  #sigma1 = np.sqrt(np.var(FRxbin1))
  #sigma2 = np.sqrt(np.var(FRxbin2[Nnrs_mask]))
  
 

  print """
  PARAMETROS:
  Media1: %f
  Mediana1: %f
  Sigma1: %f
  Media2: %f
  Mediana2: %f
  sigma2: %f 
  """%(Mean1,Median1,sigma1,Mean2,Median2,sigma2)
  
  '''  
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # NORMAL
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  plt.figure(figsize=(16,8))
  plt.plot(FRxbin1,'g-',label="Spontaneous",lw=1.5)
  plt.plot(Nnrs[Nnrs_mask],FRxbin2[Nnrs_mask],'-',color = CL['Darkgoldenrod 1'],label="Stimulated",lw=1.5)
  #plt.semilogy(FRxbin1,'g-',label="Spontaneous")
  #plt.semilogy(FRxbin2,'-',color = CL['Darkgoldenrod 1'],label="Stimulated")
  plt.grid()
  plt.ylim(ymin=1,ymax=1200)
  plt.xlabel("Neurons")
  plt.ylabel("Number of times activated (%s)"%names)
  plt.title("$J: %.1f$"%J0)
  
  leg = plt.legend(bbox_to_anchor=(0.05, 0.9, 0.5, .2),loc=3,shadow=True,ncol=2,mode="expand",borderaxespad=0.,fontsize=20)
    # set the linewidth of each legend object
  for legobj in leg.legendHandles:
    legobj.set_linewidth(2.0)
  
  plt.savefig(PATHD + "Sensibility/Sens%s.pdf"%names)
  
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # LOG
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  plt.figure(figsize=(16,8))
  plt.semilogy(FRxbin1,'g-',label="Spontaneous",lw=1.5)
  plt.semilogy(Nnrs[Nnrs_mask],FRxbin2[Nnrs_mask],'-',color = CL['Darkgoldenrod 1'],label="Stimulated",lw=1.5)
  #plt.semilogy(FRxbin1,'g-',label="Spontaneous")
  #plt.semilogy(FRxbin2,'-',color = CL['Darkgoldenrod 1'],label="Stimulated")
  plt.grid()
  plt.ylim(ymin=1,ymax=5000)
  plt.xlabel("Neurons")
  plt.ylabel("Number of times activated (%s)"%names)
  plt.title("$J: %.1f$"%J0)
  
  leg = plt.legend(bbox_to_anchor=(0.05, 0.9, 0.5, .2),loc=3,shadow=True,ncol=2,mode="expand",borderaxespad=0.,fontsize=20)
  #leg = plt.legend(bbox_to_anchor=(0, 1.02, 1.0, .102),loc=3,shadow=True,ncol=2,mode="expand",borderaxespad=0.,fontsize=20)
    # set the linewidth of each legend object
  for legobj in leg.legendHandles:
    legobj.set_linewidth(2.0)
    
  plt.savefig(PATHD + "Sensibility/Sens%s__Log.pdf"%names)
  '''
  
  
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # TRANSIENT
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  fig=plt.figure(figsize=(16,8))
  ax=fig.add_subplot(111)
  plt.plot(100*Nactives/Size,'-',lw=1.2,color = CL["Salmon 2"],label="$J=%.2f$"%J0)
  #plt.semilogx(Nactives,'-',lw=1.2,color = CL["Salmon 2"],label="J=%.2f"%J0)
  plt.xlabel("Time/timebin")
  plt.ylabel("Percentage of Neurons activated" )
  plt.title("Profile %s"%names)
  plt.xlim(xmax=ts + 2*dtime + ts)
  #plt.xticks(np.arange(0,ts+2*dtime,250))
  
  plt.axvline(x=ts-1, color ='k', ls="--", lw=2)
  plt.axvline(x=ts-1+dtime, color ='k', ls="--", lw=2)
  plt.axvline(x=ts-1 + 2*dtime,color ='k', ls="--",lw=2)
  leg = plt.legend(loc="best")
  for legobj in leg.legendHandles:
    legobj.set_linewidth(2.0)
  plt.grid(which='both')
  ax.xaxis.set_major_locator(MultipleLocator(250))
  ax.xaxis.set_minor_locator(MultipleLocator(50))
  
  #-------------------------------
  # ZOOM
  #-------------------------------
  plt.axes([0.6,.42,0.25,0.2])
  plt.plot(100.0*Nactives/Size,'-',lw=2,color = CL["Salmon 2"],label="J=%.2f"%J0)
  plt.xlim(xmax = 30)
  plt.ylim(ymax=10)
  plt.grid(which='both')
  
  plt.savefig(PATHD + "Sensibility/Sens_transient%s.pdf"%names)
  
  
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # HISTOGRAMs
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  ##################
  # Histogram 1
  ##################
  
  tmin = ts
  tmax = ts + dtime
  Bins = np.linspace(1,Size,Size)
  
  fig1 = plt.figure(figsize=(16,8))
  Ncount = plt.hist(Nactives[tmin:tmax],Bins,histtype="stepfilled",align='left',color=CL["Darkgoldenrod 1"],alpha=0.7)[0]
  Xmax = np.where((Ncount>0)==True)[0][-1]
  plt.xlim(xmax=Xmax)
  plt.xlabel("Number of active neurons")
  plt.ylabel("Number of times active")
  plt.title("Spontaneous temporal window - %s"%names)
  plt.grid()
  plt.savefig(PATHD + "Sensibility/SensHisto_w1%s.pdf"%names)
  
  ##################
  # Histogram 2
  ##################
  
  fig2 = plt.figure(figsize=(16,8))
  Ncount = plt.hist(Nactives[tmax:tmax+dtime],Bins,histtype="stepfilled",align='left',color=CL["Rosybrown"],alpha=0.7)[0]
  Xmin = np.where((Ncount>1)==True)[0][0]
  plt.xlim(xmin=Xmin)
  plt.xlabel("Number of active neurons")
  plt.ylabel("Number of times active")
  plt.title("Stimulated temporal window - %s"%names)
  plt.grid()
  plt.savefig(PATHD + "Sensibility/SensHisto_w2%s.pdf"%names)
  
   ##################
  # Histogram 2
  ##################
  
  fig3 = plt.figure(figsize=(16,8))
  Ncount = plt.hist(Nactives[tmin:tmax],Bins,histtype="stepfilled",align='left',color=CL["Darkgoldenrod 1"],alpha=0.7)[0]
  Ncount = plt.hist(Nactives[tmax:tmax+dtime],Bins,histtype="stepfilled",align='left',color=CL["Rosybrown"],alpha=0.7)[0]
  plt.xlabel("Number of active neurons")
  plt.ylabel("Number of times active")
  plt.title("Full temporal window - %s"%names)
  plt.grid()
  plt.savefig(PATHD + "Sensibility/SensHisto_wfull%s.pdf"%names)
  
  
  #plt.show()
  """
  fig, ax = plt.subplots(1, 1)
  Maxr = np.ceil(FRxbin1.max())
  Bins = np.arange(0,Maxr+0.1,0.1)
  #weights = np.ones_like(FRxbin[ts+1:])/len(FRxbin[ts+1:])
  F1,bins,pt=plt.hist(FRxbin1[ts+1:],Bins, histtype="stepfilled",align='left',alpha=0.6,label="Initial")
  F2,bins,pt=plt.hist(FRxbin2[ts+1:],Bins, histtype="stepfilled",align='left',alpha=0.6,label="Final")
  
  #vals = st.norm(loc=mu,scale=sigma)
  mu1 = np.mean(FRxbin1[ts+1:])
  sigma1 = np.sqrt(np.var(FRxbin1[ts+1:]))
  mu2 = np.mean(FRxbin2[ts+1:])
  sigma2 = np.sqrt(np.var(FRxbin2[ts+1:]))
  plt.title("$\mu_1: %.3f, \; \sigma_1: %.3f, \; \mu_12: %.3f, \; \sigma_2: %.3f$"%(mu1,sigma1,mu2,sigma2))
  plt.ylim(ymax=F1.max()*1.5)
  plt.legend()
  #x = np.linspace(0.0,Maxr,100)
  #scale = len(FRxbin[ts+1:])*0.1
  #y=st.norm.pdf(x,loc=mu,scale=sigma)
  #y=st.norm.pdf(x,mu,sigma)*scale
  #ax.plot(x,y)
  #plt.plot(x,y)
  plt.show()
  """
#********************************************************************************************************************************************
#********************************************************************************************************************************************
#********************************************************************************************************************************************

if OPTION == "SENS-ALLP":
  #-------------------------------------------------------------------------------
  #  Calculating the Mean Firing Rate 
  #-------------------------------------------------------------------------------
  Nneurons = 25
  Nactivated = 5
  ncol = 1
  ts = 25  # stable time
  dtime = 50  # temporal window
  Ntimes = ts + 2*dtime
  
  #------------------------------------------------------------
  # Initializing some util variables
  #------------------------------------------------------------
  FRxbin1 =  np.zeros(Size,dtype=float) # Firing Rate per bin
  FRxbin2 =  np.zeros(Size,dtype=float) # Firing Rate per bin
  rp1 = np.zeros((Size,dtime),dtype=float)
  rp2 = np.zeros((Size,dtime),dtype=float)
  Nactives = np.zeros(Ntimes)
  Nout = np.zeros(Ntimes)
  Neff = np.zeros(Ntimes)
  Nnrs = np.arange(Size)
  Nnrs_mask = np.ones(Size,dtype="bool")
     
  
  #-------------------------------------------------
  # Get the index of neurons with:
  #  * higher out degree
  #  * lower out degree
  #  * stimulated neurons
  #-------------------------------------------------
  din,dout = GetDegree(Ai)
  idx = np.argsort(dout)
  #IDX = idx[:Nactivated]  #for neurons with lowest dout
  #IDX = idx[-Nactivated:] #for neurons with highest dout
  # ---- for random neurons ---
  nactivated = np.random.randint(500,size=Nneurons)
  IDX = idx[nactivated] 
  
  #Nnrs_mask[idx[-Nactivated:]] = False
  Nnrs_mask[IDX] = False
  
  
  
  
  
  #---------------------------------------
  # Reading file with jcritics
  #---------------------------------------
  NAME = np.loadtxt("%sJcritics.txt"%PATHD,usecols=[0],dtype="|S14")
  JC,SIGMAJ = np.loadtxt("%sJcritics.txt"%PATHD,usecols=[1,2],unpack=True)
  SizeJC = len(JC)
  
  #--------------------------------------
  # Initializing statistical variables
  #--------------------------------------
  Median2 = np.zeros(SizeJC)
  Median1 = np.zeros(SizeJC)
  Mean1 = np.zeros(SizeJC)
  Mean2 = np.zeros(SizeJC)
  Sigma1 = np.zeros(SizeJC)
  Sigma2 = np.zeros(SizeJC)
  
  MeanDeg1 = np.zeros(SizeJC)
  SigmaDeg1 = np.zeros(SizeJC)
  MeanDeg2 = np.zeros(SizeJC)
  SigmaDeg2 = np.zeros(SizeJC)
  
  
  i = args.value
  
  #ifile2 = args.ifile
  #names = ifile2.split("/")[2]
  
  # Loop over jcritics
  #-------------------------------
  for n in xrange(SizeJC):
    
    #---------------------------------------
    # Stablishing initial active neurons
    #---------------------------------------
    xi = np.zeros(Size)
    loc1 = np.random.randint(500,size=Nneurons)
    xi[loc1] = 1.0
    #xi[idx[-Nactivated:]]=1.0
    x1 = xi.copy()
    xold = xi.copy()
    Nactives[0] = np.sum(xi)
    Neff[0] = np.sum(xi)
    Nout[0] = np.sum(dout*xi)
    #rp1[:,0] = xi.copy()
    
    #-----------------------------------------------------------
    # Reading files
    #-----------------------------------------------------------
    Af = np.loadtxt("%s%s/Matrix%s.txt"%(PATHD,NAME[n],NAME[n]))
    J0 = JC[n] - 4*SIGMAJ[n]
    
    # Progress
    #-------------------------------
    print "Profile --> %s     progress: %.2f %%"%(NAME[n],(100.0*(n+1))/(SizeJC))
    
    #Loop over time
    #-------------------------------
    for t in xrange(1,Ntimes):
      
      # Calculating the firing probability
      nu1 = Firing_Probability(x1,h0[i],J0,pc,Af)
	  
      # Generating uniform random number to determine if
      # neurons spike or not
      Threshold1 = np.random.rand(Size)
      Spike1 = nu1 >= Threshold1
      
      #Final state
      x1 = Spike1.astype(int)
      Nactives[t] = np.sum(x1)
      #Neff[t] = Neffective(Af,xold)
      Nout[t] = np.sum(dout*x1)
      xold = x1.copy()
      
	  
      
      if t>=ts and t<ts+dtime:
	rp1[:,t-ts] = x1.copy()
	#x1 = Spike1.astype(int)  
	FRxbin1 = FRxbin1 + x1
	#FRxbin2[t] = np.sum(x2)/(Size*Dt)
      elif t>=ts+dtime:
	#x1[idx[-Nactivated:]] = 1.0
	x1[IDX] = 1.0
	#FRxbin1[t] = (np.sum(x1)-Nneurons)/((Size-Nneurons)*Dt)
	FRxbin2 = FRxbin2 + x1
	Nactives[t] = np.sum(x1[Nnrs_mask])
  
    # Not including neurons with greater out-degree
    FRxbin2[IDX] = 1E-10
    #FRxbin2[idx[-Nactivated:]] = 1E-10
  
    # Calculating the median  
    Median1[n] = np.median(Nactives[ts:ts+dtime])
    Median2[n] = np.median(Nactives[ts+dtime:ts+2*dtime])
    Mean1[n] = np.mean(Nactives[ts:ts+dtime])
    Mean2[n] = np.mean(Nactives[ts+dtime:ts+2*dtime])
    Sigma1[n] = np.sqrt(np.var(Nactives[ts:ts+dtime]))
    Sigma2[n] = np.sqrt(np.var(Nactives[ts+dtime:ts+2*dtime]))
    
    MeanDeg1[n] = np.mean(Nout[ts:ts+dtime])
    MeanDeg2[n] = np.mean(Nout[ts+dtime:ts+2*dtime])
    SigmaDeg1[n] = np.sqrt(np.var(Nout[ts:ts+dtime]))
    SigmaDeg2[n] = np.sqrt(np.var(Nout[ts+dtime:ts+2*dtime]))
  
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # HISTOGRAMS
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
    ##################
    # Histogram 1
    ##################
  
    tmin = ts
    tmax = ts + dtime
    Bins = np.linspace(1,Size,Size)
  
    fig1 = plt.figure(figsize=(16,8))
    Ncount = plt.hist(Nactives[tmin:tmax],Bins,histtype="stepfilled",align='left',color=CL["Darkgoldenrod 1"],alpha=0.7)[0]
    Xmax = np.where((Ncount>0)==True)[0][-1]
    plt.xlim(xmax=Xmax)
    plt.xlabel("Number of active neurons")
    plt.ylabel("Number of times active")
    plt.title("Spontaneous temporal window - %s"%NAME[n])
    plt.grid()
    plt.savefig(PATHD + NAME[n] + "/SensHisto_w1%s.pdf"%NAME[n])
    plt.close()
  
    ##################
    # Histogram 2
    ##################
  
    fig2 = plt.figure(figsize=(16,8))
    Ncount = plt.hist(Nactives[tmax:tmax+dtime],Bins,histtype="stepfilled",align='left',color=CL["Rosybrown"],alpha=0.7)[0]
    Xmin=0
    #try:
      #Xmin = np.where((Ncount>2)==True)[0][0]
    #except IndexError:
      #Xmin = 0
    try:
      Xmax = np.where((Ncount>0)*(Ncount<3)==True)[0][-1]
    except IndexError:
      Xmax = Mean2[n] + 3*Sigma[n]
    #plt.xlim(xmin=Xmin,xmax=Xmax)
    plt.xlabel("Number of active neurons")
    plt.ylabel("Number of times active")
    plt.title("Stimulated temporal window - %s"%NAME[n])
    plt.grid()
    plt.savefig(PATHD + NAME[n] + "/SensHisto_w2%s.pdf"%NAME[n])
    plt.close()
    
    ##################
    # Histogram 1+2
    ##################
  
    fig3 = plt.figure(figsize=(16,8))
    
    Ncount = plt.hist(Nactives[tmin:tmax],Bins,histtype="stepfilled",align='left',color=CL["Darkgoldenrod 1"],alpha=0.7)[0]
    Ncount = plt.hist(Nactives[tmax:tmax+dtime],Bins,histtype="stepfilled",align='left',color=CL["Rosybrown"],alpha=0.7)[0]
    Xmin = 0.0 #np.where((Ncount>1)==True)[0][0]
    try:
      Xmax = np.where((Ncount>0)*(Ncount<3)==True)[0][-1]
    except IndexError:
      Xmax = Mean2[n] + 3*Sigma[n]
    #plt.xlim(xmin=Xmin,xmax=Xmax)
    plt.xlabel("Number of active neurons")
    plt.ylabel("Number of times active")
    plt.title("Full temporal window - %s"%NAME[n])
    plt.grid()
    plt.savefig(PATHD + NAME[n] + "/SensHisto_wfull%s.pdf"%NAME[n])
    plt.close()
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # TRANSIENT
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fig=plt.figure(figsize=(16,8))
    ax=fig.add_subplot(111)
    plt.plot(100*Nactives/Size,'-',lw=2.0,color = "red",label="$J=%.2f$"%J0)
    #plt.semilogx(Nactives,'-',lw=1.2,color = CL["Salmon 2"],label="J=%.2f"%J0)
    plt.xlabel("Time/timebin")
    plt.ylabel("Percentage of Neurons activated" )
    plt.title("Profile %s"%NAME[n])
    plt.xlim(xmax=ts + 2*dtime + ts)
    plt.yticks(np.linspace(0,100,11))
    plt.xticks(np.linspace(0,ts+2*dtime+ts,21))
    #plt.xticks(np.arange(0,ts+2*dtime,250))
  
    plt.axvline(x=ts-1, color ='k', ls="--", lw=2)
    plt.axvline(x=ts-1+dtime, color ='k', ls="--", lw=2)
    plt.axvline(x=ts-1 + 2*dtime,color ='k', ls="--",lw=2)
    
    
    leg = plt.legend(loc="best")
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)
    plt.grid(which='both')
    #ax.xaxis.set_major_locator(MultipleLocator(250))
    #ax.xaxis.set_minor_locator(MultipleLocator(50))
  
    #-------------------------------
    # ZOOM
    #-------------------------------
    #dsigma=3
    #plt.axes([0.6,.42,0.25,0.25])
    ##plt.plot(100.0*Nactives/Size,'-',lw=2,color = CL["Salmon 2"],label="J=%.2f"%J0)
    #plt.plot(100.0*Nactives/Size,'-',lw=2,color = CL["Salmon 2"],label="J=%.2f"%J0)
    #idmax = np.where(100.0/Size*Nactives[ts+dtime-1:]>5)[0][0] # greater than 5%
    #plt.xlim(xmin = ts+dtime,xmax=ts-1+dtime+idmax)
    #plt.ylim(ymax=5)
    #plt.grid(which='both')
    ## Zone between the mean
    #Xfill = np.linspace(ts-1 +dtime,ts-1+2*dtime,dtime)
    #Ymin = (Mean1[n] - dsigma*Sigma1[n])*100.0/Size*np.ones(len(Xfill))
    #Ymax = (Mean1[n] + dsigma*Sigma1[n])*100.0/Size*np.ones(len(Xfill))
    #plt.fill_between(Xfill,Ymin,Ymax,alpha=0.6,color=CL["Tan"])
    #plt.axhline(y=Mean1[n]*100/Size,color='k',ls='--')
    #plt.title("\t\tWith 3$\sigma$",fontsize=14)
    
    #-------------------------------
    # ZOOM2
    #-------------------------------
    #plt.axes([0.2,.42,0.25,0.25])
    #plt.plot(100.0*Nactives/Size,'-',lw=2,color = CL["Salmon 2"],label="J=%.2f"%J0)
    #plt.xlim(xmax = ts+dtime,xmin=ts)
    #plt.ylim(ymax=3)
    #plt.grid(which='both')
    ## Zone between the mean
    #Xfill = np.linspace(ts-1,ts-1+dtime,dtime)
    #Ymin = (Mean1[n] - dsigma*Sigma1[n])*100.0/Size*np.ones(len(Xfill))
    #Ymax = (Mean1[n] + dsigma*Sigma1[n])*100.0/Size*np.ones(len(Xfill))
    #plt.fill_between(Xfill,Ymin,Ymax,alpha=0.6,color=CL["Tan"])
    #plt.axhline(y=Mean1[n]*100/Size,color='k',ls='--')
  
    plt.savefig(PATHD + NAME[n] + "/Sens_transient%s.pdf"%NAME[n])
    plt.close()
    
    #subplots pf transient
    SubPlots_Transient(Nactives,Size,ts,dtime,J0,PATHD,NAME[n],
		       Mean1[n],Sigma1[n],Mean2[n],Sigma2[n],CL)
    
    np.savetxt(PATHD + NAME[n] + "/Sens_transient%s.txt"%NAME[n],Nactives)
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # PLOT OF Nout
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fig = plt.figure(figsize=(10,6))
    plt.plot(Nout,'-')
    plt.xlabel("time/timebin")
    plt.ylabel("N out")
    plt.grid()
    plt.savefig(PATHD + NAME[n] + "/Nout%s.pdf"%NAME[n],bbox_inches='tight')
    #plt.savefig("%sNout.pdf"%PATHD,bbox_inches='tight')
    plt.close()
    
    np.savetxt(PATHD + NAME[n] + "/Nout%s.txt"%NAME[n],Nout)
    
      
  LABELFILE = '''\
  #########################################################################################################
  COLUMN LABELS: (Name, J0, Mean1, Median1, Sigma1, Mean2, Median2, Sigma2, Mean2-Mean1,Median2-Median1)
    1 : Temporal window for spontaneous behavior
    2 : Temporal window for stimulated behavior
    
    Network stimulated with nodes with higher out-degree
    Number of stimulated nodes: %d
  #########################################################################################################'''%(Nactivated)
  
  STAT = np.array(zip(NAME,JC-4*SIGMAJ,Mean1,Median1,Sigma1,Mean2,Median2,Sigma2,Mean2-Mean1,Median2-Median1),dtype=[\
    ('name','S14'),('J0','f8'),('mean1','f8'),('median1','f8'),('sigma1','f8'),('mean2','f8'),('median2','f8'),('sigma2','f8'),\
      ('meandiff','f8'),('mediandiff','f8')])    

  np.savetxt(PATHD + "Sens-statistic.txt",STAT,fmt="%14s %10f %10f %10f %10f %10f %10f %10f %10f %10f",header=LABELFILE)
  
  
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # PLOT OF "SENSIBILITY"
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  STAT.sort(order="meandiff")
  index = [ i for i in xrange(len(STAT['name'])) if STAT['name'][i] in NAME]
  fig = plt.figure(figsize=(10,6))
  ax=fig.add_subplot(111)
  plt.plot(range(SizeJC),STAT['meandiff'][index],'go--')
  plt.ylabel("$\mu_2-\mu_1$(Neuron actives)")
  plt.xticks(range(SizeJC),STAT['name'][index],rotation=70,fontsize=8,ha='right')
  ##plt.axes().yaxis.set_minor_locator(minorLocator)
  #ax.yaxis.set_minor_locator(AutoMinorLocator(5))
  #plt.grid(which="major",color='k',linestyle=':',lw=0.1,alpha=0.5)
  #plt.minorticks_on()
  #plt.grid(which="minor",axis='y',color='k',linestyle=':',lw=0.1,alpha=0.5)
  plt.grid()
  plt.savefig("%sSensVsProfiles.pdf"%PATHD,bbox_inches='tight')
  plt.close()
  
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # PLOT OF Neff
  #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  #fig = plt.figure(figsize=(10,6))
  #plt.plot(Neff,'-')
  #plt.xlabel("time/timebin")
  #plt.ylabel("N effective")
  #plt.grid()
  #plt.savefig(PATHD + NAME[n] + "/Neffective%s.pdf"%NAME[n].bbox_inches='tight')
  #plt.savefig("%sNeffective.pdf"%PATHD,bbox_inches='tight')
  
  
  
  
  
else:
  print "No se hace nada..."
  
  
