#!/bin/bash

DIRFILE="Profiles"
DFILES=`ls -d ./$DIRFILE/_*/`
FOLDERBC=$1
# if [ -z "$1"];then
# 	FOLDERBC="backup"
# 	# Creating backup folder
# 	NAMEFOLDER="./$DIRFILE/$FOLDERBC/"
# 	echo $NAMEFOLDER
# else
	
# 	# Creating backup folder
# 	NAMEFOLDER=$FOLDERBC
# 	echo $NAMEFOLDER
# fi
if [ -z "$1" ];then
	FOLDERBC="backup"
	NAMEFOLDER="./$DIRFILE/$FOLDERBC/"
	echo $NAMEFOLDER
else
	NAMEFOLDER=$FOLDERBC
	echo $NAMEFOLDER 
fi

mkdir -p ${NAMEFOLDER}

for folder in $DFILES
do
	files=$(ls ${folder}Sens_transient*)
	files2=$(ls ${folder}Nout*)
	cp $files ${NAMEFOLDER}
	cp $files2 ${NAMEFOLDER}
done