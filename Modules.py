import numpy as np
import matplotlib as mpl
#mpl.use('PDF')
import matplotlib.pyplot as plt
import os,sys
import math



def VNEntropy_directed(A,flag=0):
  
  Indeg , Outdeg = GetDegree(A)
  Vol = len(A)
  Elm = GetList(A)
  Sum = 0
  
  for i in range(len(Elm)):
    Sum = Sum + 1.0*Indeg[Elm[i,0]]/(Indeg[Elm[i,1]]*Outdeg[Elm[i,0]]**2)
    if A[Elm[i,0],Elm[i,1]] == A[Elm[i,1],Elm[i,0]]:
      Sum = Sum + 1.0/(Outdeg[Elm[i,0]]*Outdeg[Elm[i,1]])
  if flag==0:
    EntropyVN = 1.0 - 1.0/Vol - Sum/(2.0*Vol*Vol)
  else:
    EntropyVN = 0.5*Sum/Vol
  return EntropyVN

def VNEntropy_directed_exact(eigval, size,flag=0):
  Sum=0
  f = 1.0/size
  if flag == 0: #Exact Form
    for i in range(size):
      if eigval[i] >= 1E-7:
	Sum =  Sum + f*eigval[i]*np.log(eigval[i]*f)
    EntropyVN = -Sum
  if flag==1: # Approximate form
    for i in range(size):
      Sum = Sum + f*eigval[i]*(1-eigval[i]*f)
    EntropyVN = Sum
  return EntropyVN
