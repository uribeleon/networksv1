#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================

# COLOR DEFINITIONS
#--------------------------------------------------------------------------------------
set border linewidth 1.5
set style line 2 lt 1 lc rgb '#1B7837' # dark green
set style line 1 lt 1 lc rgb '#D73027' # red
set style line 3 lt 1 lc rgb '#FDAE61' # 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' # medium blue
set style line 5 lt 1 lc rgb '#9970AB' # medium purple
# set style line 1 lc rgb '#800000' lt 1 lw 2
# set style line 2 lc rgb '#ff0000' lt 1 lw 2
# set style line 3 lc rgb '#ff4500' lt 1 lw 2
# set style line 4 lc rgb '#ffa500' lt 1 lw 2
# set style line 5 lc rgb '#006400' lt 1 lw 2
# set style line 6 lc rgb '#0000ff' lt 1 lw 2
# set style line 7 lc rgb '#9400d3' lt 1 lw 2

# ARGUMENTS
#--------------------------------------------------------------------------------------
pathdir = ARG1
profile = ARG2


# GENERAL FUNCTIONS
#--------------------------------------------------------------------------------------
TheTitle(name) = sprintf("echo '%s' | sed 's/[a-z]//g;s/[\/*,._,]/ /g;s/  //g'",name)


# SET TERMINAL CONFIGURATION
#--------------------------------------------------------------------------------------
set terminal pdf size 10,8 font 'Verdana,20' enhanced color lw 3
# set terminal wxt enhanced font 'Verdana,10' persist size 1080,800
#set terminal pngcairo size 1080,800 enhanced font 'Verdana,12'EVERY FILE IN SEPARATED FOLDERS
# set terminal pdf size 1080,800 enhanced font 'Verdana,12'

set output pathdir.'Profile'.profile.'_prof.pdf'


# SET GENERAL (global) PARAMETERS OF THE PLOTS
#--------------------------------------------------------------------------------------

#  *** Axes ***
#set style line 11 lc rgb '#808080' lt 1
set border lw 1
#set border 3 back ls 11
#set tics nomirror out scale 0.75

#  *** Grid ***
set style line 102 lc rgb '#d6d7d9' lt 0 lw 1
set grid back ls 102
# set style line 12 lc rgb'#808080' lt 0 lw 1
# set grid back
# set grid back ls 12
#set xtics 1 font ",8"
#set ytics 1 font ",8"

#  *** Axes Label ***
#set xlabel "Effective Rewirings (x 10^6)" font ",35"
set ylabel "Assortativity" font ",35" offset -1
# set title "Profile -1 1 -1 1" font ",18"

#  **** Margins ***
set rmargin 4
set bmargin 4
set lmargin 11
#set tmargin 4

#  *** Axes format ***
# set format x '%.1tx10^{%T}'
#set format x '%g'
#set xtics font ",30"
set xtics ("(In,Out)" 1, "(Out,In)" 2, "(In,In)" 3, "(Out,Out)" 4) font ',28'
set ytics font ",28"

#  *** Configurating palette ***
set loadpath '../ConfigGnuplot'

#  *** KeyBox ***
#set key at 39,0.64 box font ',28' spacing 5 width 3 opaque
set key at screen 0.93,0.88 box font ',28' spacing 5 width -0.5 opaque
###set key top right box font ',28' spacing 2 width 0 opaque
#set key horiz
#set key left top


# ADDITIONAL CONFIGURATIONS
#---------------------------------------------------------------------------------

#  *** LabelBox of each plot ***
title = 'in-out out-in in-in out-out'
mytitle(n,m) = m==0?word(title,n):""

#  *** Error bars configuration ***
# set style line 9 lt 1 lc rgb '#252525' lw 2 ps 0.7 pt 7 # dark grey

# PLOT PROFILE WITH ERROR BARS
#--------------------------------------------------------------------------------------

# >>> Set title of plot
Title = "Profile ".system(TheTitle(profile))
set title Title font ",40"

ifile = pathdir.'Assort'.profile.'.txt'
#ifile = pathdir.'Assort'.profile.'_V0.txt'

plot [0:5][*:*] "< tail -n 1 ".ifile." | tr -s \' \' \'\n\' | tail -n +2" u ($0 +1):1 t "After" w lp ls 2 pt 5 ps 2 lw 1.5,\
"< head -n 1 ".ifile." | tr -s \' \' \'\n\' | tail -n +2" u ($0 +1):1 t "Before" w lp ls 1 pt 7 ps 2 lw 1.5

#for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls l-1 pt 7 ps 0.05 ,\

# plot [*:*] for[m=2:5] "./_-1_1_-1_1/Assort_-1_1_-1_-1_.txt" u ($$1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 3
# plot [*:*][*:*] for [m=2:5] for[n=0:19:2] pathdir.'Assort'.prof.'_V'.n.'.txt' every 100 u ($1/1e6):m t '' w p ls 2*(m-1) lw 1.3 pt m ps 0.5, \
# plot [*:*][*:*] for[m=2:5] ifile u ($1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 2.5
# plot  [*:*][*:*] for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls 9
 #l-1 ps 1.2, \
# for[k=2:5] pathdir.'ErrorBars_mode'.(k-2).'.txt' u ($1/1e6):2:3:4 t '' w yerrorbars ls k-1 lw 2
# plot [*:*] for [m=2:5] for[n=0:19] 1/0  t mytitle(m-1,n) w p ls 2*(m-1) ps 2 pt m, \
#  for [m=2:5] for[n=0:19] '../ErrorBars/_1_1_-1_-1/Assort_1_1_-1_-1_V'.n.'.txt' u 1:m t '' w p ls 2*(m-1) lw 1.3 pt m ps 0.5
unset out
reset
