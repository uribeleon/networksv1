#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================


#****************************************************************************
#  PLOTS
#****************************************************************************
# set style line 1 lc rgb '#0060ad' lt 1 lw 1.3 pt 7 ps 2  #blue
# set style line 2 lc rgb '#FF6347' lt 1 lw 1.5 pt 7 ps 2  #tomato-red
# set style line 3 lc rgb '#008000' lt 1 lw 1.5 pt 7 ps 2  #green
# set style line 4 lc rgb '#800080' lt 1 lw 1.5 pt 7 ps 2  #purple
# set style line 5 lc rgb '#FF8C00' lt 1 lw 1.5 pt 7 ps 2  #dark orange
# set style line 4 lc rgb '#708090' lt 1 lw 1.5 pt 7 ps 2  #slate gray

# color definitions
set border linewidth 1.5
set style line 1 lt 1 lc rgb '#D73027' lw 2 # red
set style line 2 lt 1 lc rgb '#1B7837' lw 2 pt 7 ps 2 # dark green
set style line 3 lt 1 lc rgb '#FDAE61' lw 2 pt 7 ps 2# 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' lw 2 # medium blue
set style line 5 lt 1 lc rgb '#9970AB' lw 2 pt 7 ps 2# medium purple
set style line 6 lt 1 lc rgb '#00FFFF' lw 2 pt 7 ps 2 # cyan
set style line 7 lt 1 lc rgb '#FF8C00' lw 2 pt 7 ps 2  #dark orange
set style line 10 lt 1 lc rgb '#00FFFF' lw 2 # dark cyan
# set style line 1 lc rgb '#800000' lt 1 lw 2
# set style line 2 lc rgb '#ff0000' lt 1 lw 2
# set style line 3 lc rgb '#ff4500' lt 1 lw 2
# set style line 4 lc rgb '#ffa500' lt 1 lw 2
# set style line 5 lc rgb '#006400' lt 1 lw 2
# set style line 6 lc rgb '#0000ff' lt 1 lw 2
# set style line 7 lc rgb '#9400d3' lt 1 lw 2



# ARGUMENTS
#--------------------------------------------------------------------------------------
pathdir = ARG1
namefile = 'Lambda2-Profiles-sorted.txt'

names  = system('ls -d -1 ./'.pathdir.'*/ | tr "\n" "\0" | xargs -0 -n 1 basename')
#names = system('ls -d ./'.pathdir.'*/ | cut -d ')


# GENERAL FUNCTIONS
#--------------------------------------------------------------------------------------
TheTitle(name) = sprintf("echo '%s' | sed 's/[a-z]//g;s/[\/*,._,]/ /g;s/  //g'",name)


# SET TERMINAL CONFIGURATION
#--------------------------------------------------------------------------------------
set term 'unknown'
#set term postscript size 20,10 eps color enhanced
#set output '|ps2pdf - '.pathdir.'outputfile.pdf'
#set terminal pdf size 12,8 font 'Verdana,20' enhanced color lw 2 rounded
# set terminal wxt enhanced font 'Verdana,10' persist size 1080,800
#set terminal pngcairo size 1080,800 enhanced font 'Verdana,12'
# set terminal pdf size 1080,800 enhanced font 'Verdana,12'
# >>> Set output name of file
#set output pathdir.'PLOT_Lambda2_probe.pdf'

# SET GENERAL (global) PARAMETERS OF THE PLOTS
#--------------------------------------------------------------------------------------

#  *** Axes ***
#set style line 11 lc rgb '#808080' lt 1
set border lw 1
#set border 3 back ls 11
#set tics nomirror out scale 0.75

#  *** Grid ***
set style line 102 lc rgb '#d6d7d9' lt 0 lw 1
set grid back ls 102
# set style line 12 lc rgb'#808080' lt 0 lw 1
# set grid back
# set grid back ls 12
#set xtics 1 font ",8"
#set ytics 1 font ",8"

#  *** Axes Label ***
#set xlabel "Effective Rewirings (x 10^6)" font ",35"
set ylabel " {/Symbol l}_2 of a directed laplacian matrix" font ",40" offset 0
# set title "Profile -1 1 -1 1" font ",18"

#  **** Margins ***
set rmargin 4
set bmargin 7
set lmargin at screen 0.1

#  *** Axes format ***
# set format x '%.1tx10^{%T}'
set xtics rotate font ",24"
set xtics format "{/:Bold %.0s}"
set ytics font ",26"


#  *** Configurating palette ***
set loadpath '../ConfigGnuplot'
# load 'rdylbu.pal'
# load 'jet.pal'
# load 'moreland.pal'
# load 'set1.pal'
# load 'spectral.pal'


# ADDITIONAL CONFIGURATIONS
#---------------------------------------------------------------------------------
#  *** LabelBox of each plot ***
title = 'Before After'
set key center left box font ',28' spacing 5 width 1 opaque


# PLOT LAMBDA 2
#--------------------------------------------------------------------------------------

# >>> text file with data
ifile = pathdir.namefile


# CONFIGURATION OF BROKEN AXES
#-----------------------------------------
# >>> White rectangle to cover the plot
#set object 1 polygon from first 9.4,17 to 9.4,30 to 9.6,30 to 9.6,17 to 9.4,17 front
#set object 1 fillstyle solid 0.0 noborder front
set object 1 rect from 9.4,0 to 9.6,8
set object 1 rect fc rgb 'red' fillstyle solid 0.0 noborder front

#plot 'AvgShortPath-Profiles-sorted.txt' u 2:xtic(1) t 'Before' w lp pt 7 ps 1.5 lt 1 lw 3
plot "< head -n 11 ".ifile."| tail -n +2 && tail ".ifile u ($0):($0>=10?$3:1/0):xtic(1) t 'After- the most' w lp ls 5 lw 1.8,\
"" u ($0):($0>=10?$3:1/0):xtic(1) t '' w p pt 6 ps 2 lc 0,\
"" u ($0):($0<10?$3:1/0):xtic(1) t 'After- the least' w lp ls 7 lw 1.8,\
"" u ($0):($0<10?$3:1/0):xtic(1) t '' w p pt 6 ps 2 lc 0,\
'' u 2 t 'Before' w l ls 2 lw 4
clear

# >>>>>> To replot the figure in order to the file only have 1 plot
set terminal pdf size 12,8 font 'Verdana,20' enhanced color lw 2 rounded
set output pathdir.'PLOT_Lambda2_probe.pdf'
# >>> Draw lines that indicate break axis
#stats "< tail -n +4 ".ifile." | head -n 10 && tail ".ifile u 2 nooutput
stats "< head -n 11 ".ifile."| tail -n +2 && tail ".ifile u 3 nooutput

YMIN = GPVAL_Y_MIN
YMAX = GPVAL_Y_MAX
delta = 0.02
set arrow 1  from 9.4,YMIN to 9.6,YMIN nohead front lw 1. lc rgb "white"
set arrow 2  from 9.4,YMAX to 9.6,YMAX nohead front lw 1. lc rgb "white"
set arrow 3  from 9.3,YMIN-delta to 9.5,YMIN + delta nohead front lw 1.5
set arrow 4  from 9.5,YMIN-delta to 9.7,YMIN + delta nohead front lw 1.5
set arrow 5  from 9.3,YMAX-delta to 9.5,YMAX + delta nohead front lw 1.5
set arrow 6  from 9.5,YMAX-delta to 9.7,YMAX + delta nohead front lw 1.5
replot
print YMIN,YMAX



#plot ifile u ($0):($0>10?$3:1/0):xtic(1) t 'After- the most' w lp ls 5 lw 1.8
#'' u $1>10?$3:1/0):xtic(1): t 'After - the least' w lp ls 7 lw 1.8

#plot "< head ".ifile."| tail -n +2  " u 3:xtic(1) t 'After - the least' w lp ls 7 lw 1.8,\
#'' u 3:xtic(1) t '' pt 6 ps 2 lc 0 lw 0.7,\
#'' u 2:xtic(1) t 'Before' w l ls 2 lw 4
#, '' u 2:xtic(1) notitle pt 6 ps 2 lc 0 lw 0.7

#plot [*:*][*:*] for[m=2:5] ifile u ($1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 2.5,\
#for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls l-1 pt 7 ps 0.7 ,\
#for[k=2:5] pathdir.'ErrorBars_mode'.(k-2).'.txt' u ($1/1e6):2:3:4 t '' w yerrorbars ls k-1 lw 2

unset out
reset
