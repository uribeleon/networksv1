#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================


#****************************************************************************
#  PLOTS
#****************************************************************************
# set style line 1 lc rgb '#0060ad' lt 1 lw 1.3 pt 7 ps 2  #blue
# set style line 2 lc rgb '#FF6347' lt 1 lw 1.5 pt 7 ps 2  #tomato-red
# set style line 3 lc rgb '#008000' lt 1 lw 1.5 pt 7 ps 2  #green
# set style line 4 lc rgb '#800080' lt 1 lw 1.5 pt 7 ps 2  #purple
# set style line 5 lc rgb '#FF8C00' lt 1 lw 1.5 pt 7 ps 2  #dark orange
# set style line 4 lc rgb '#708090' lt 1 lw 1.5 pt 7 ps 2  #slate gray

# color definitions
set border linewidth 1.5
set style line 1 lt 1 lc rgb '#D73027' lw 3 # red
set style line 2 lt 1 lc rgb '#1B7837' lw 3 # dark green
set style line 3 lt 1 lc rgb '#FDAE61' lw 3 # 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' lw 3 # medium blue
set style line 5 lt 1 lc rgb '#9970AB' lw 3 # medium purple
# set style line 1 lc rgb '#800000' lt 1 lw 2
# set style line 2 lc rgb '#ff0000' lt 1 lw 2
# set style line 3 lc rgb '#ff4500' lt 1 lw 2
# set style line 4 lc rgb '#ffa500' lt 1 lw 2
# set style line 5 lc rgb '#006400' lt 1 lw 2
# set style line 6 lc rgb '#0000ff' lt 1 lw 2
# set style line 7 lc rgb '#9400d3' lt 1 lw 2


# ARGUMENTS
# pathdir = '$0'
# ifile   = '$1'
# ofile   = '$2'
# proftitle = '$3'

#ParentDir = '../Paper_Profiles/'
#prof = '_1_1_1_0'
#pathdir = '../Paper_Profiles/'.prof.'/'
#ifile = ParentDir.prof.'/Assort'.prof.'.txt'


pathdir = ARG1
prof = ARG2

#TheTitle(name) = sprintf("echo '%s' | sed 's/[a-z]//g;s/[\/*,._,]/ /g;s/  //g'",name)
TheTitle(name) = sprintf("echo '%s' | sed 's/[\/*,._,]/ /g;s/  //g'",name)

set terminal pdf size 10,8 font 'Verdana,20' enhanced color lw 3 rounded
# set out 'grafico.pdf'
set output pathdir.'Profile'.prof.'.pdf'
# set output pathdir.ofile

## SET GENERAL (global) PARAMETERS OF THE PLOTS
#--------------------------------------------------------------------------------------

#  *** Axes ***
#set style line 11 lc rgb '#808080' lt 1
set border lw 1
#set border 3 back ls 11
#set tics nomirror out scale 0.75

#  *** Grid ***
set style line 102 lc rgb '#d6d7d9' dt 3
set grid back ls 102
# set style line 12 lc rgb'#808080' lt 0 lw 1
# set grid back
# set grid back ls 12
#set xtics 1 font ",8"
#set ytics 1 font ",8"
set ytics 0.1
set mytics 2

#  *** Axes Label ***
set xlabel "Effective Rewirings (x 10^6)" font ",35"
set ylabel "Assortativity" font ",35" offset -1
# set title "Profile -1 1 -1 1" font ",18"

#  **** Margins ***
set rmargin 4
set bmargin 4
#set tmargin 4

#  *** Axes format ***
# set format x '%.1tx10^{%T}'
set format x '%g'
set xtics font ",30"
set ytics font ",30"

#  *** Configurating palette ***
set loadpath '../ConfigGnuplot'
# load 'rdylbu.pal'
# load 'jet.pal'
# load 'moreland.pal'
# load 'set1.pal'
# load 'spectral.pal'

#  *** KeyBox ***
#set key at screen 0.93,0.8 box font ',28' spacing 5 width -0.5 opaque
set key at screen 0.93,0.6 box font ',28' spacing 5 width -0.5 opaque
#set key center right box font ',28' spacing 5 width 3 opaque
#set key horiz
#set key left top


# ADDITIONAL CONFIGURATIONS
#---------------------------------------------------------------------------------

#  *** LabelBox of each plot ***
title = 'in-out out-in in-in out-out'
mytitle(n,m) = m==0?word(title,n):""

#  *** Error bars configuration ***
# set style line 9 lt 1 lc rgb '#252525' lw 2 ps 0.7 pt 7 # dark grey


# PLOT PROFILE
#--------------------------------------------------------------------------------------

# >>> Set title of plot
Title = "Profile ".system(TheTitle(prof))
set title Title font ",40"

# ifile = pathdir.'Assort2'.prof.'.txt'
ifile = pathdir.'Assort'.prof.'.txt'

plot [*:*][*:*] for[m=2:5] ifile u ($1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 2
#for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls l-1 pt 7 ps 0.7 ,\
#for[k=2:5] pathdir.'ErrorBars_mode'.(k-2).'.txt' u ($1/1e6):2:3:4 t '' w yerrorbars ls k-1 lw 2

# plot [*:*] for[m=2:5] "./_-1_1_-1_1/Assort_-1_1_-1_-1_.txt" u ($$1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 3
# plot [*:*][*:*] for [m=2:5] for[n=0:19:2] pathdir.'Assort'.prof.'_V'.n.'.txt' every 100 u ($1/1e6):m t '' w p ls 2*(m-1) lw 1.3 pt m ps 0.5, \
# plot [*:*][*:*] for[m=2:5] ifile u ($1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 2.5
# plot  [*:*][*:*] for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls 9
 #l-1 ps 1.2, \
# for[k=2:5] pathdir.'ErrorBars_mode'.(k-2).'.txt' u ($1/1e6):2:3:4 t '' w yerrorbars ls k-1 lw 2
# plot [*:*] for [m=2:5] for[n=0:19] 1/0  t mytitle(m-1,n) w p ls 2*(m-1) ps 2 pt m, \
#  for [m=2:5] for[n=0:19] '../ErrorBars/_1_1_-1_-1/Assort_1_1_-1_-1_V'.n.'.txt' u 1:m t '' w p ls 2*(m-1) lw 1.3 pt m ps 0.5
unset out
reset
