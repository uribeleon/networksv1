# reset
#
pathdir = ARG1
namefile = 'Jcr_sorted.txt'
ifile = pathdir.namefile
#
# set border linewidth 1.5
# set style line 1 lt 1 lc rgb '#D73027' lw 2 # red
# set style line 2 lt 1 lc rgb '#1B7837' lw 2 pt 7 ps 2 # dark green
# set style line 3 lt 1 lc rgb '#FDAE61' lw 2 pt 7 ps 2# 'orange'
# set style line 4 lt 1 lc rgb '#74ADD1' lw 2 # medium blue
# set style line 5 lt 1 lc rgb '#9970AB' lw 2 pt 7 ps 2# medium purple
# set style line 6 lt 1 lc rgb '#00FFFF' lw 2 pt 7 ps 2 # cyan
# set style line 7 lt 1 lc rgb '#FF8C00' lw 2 pt 7 ps 2  #dark orange
# set style line 10 lt 1 lc rgb '#00FFFF' lw 2 # dark cyan
#
# set loadpath '../ConfigGnuplot'
# #set term png font "Times,15"
# #set output "broken_axes1.png"

#
# #x,y axis label and title label
# # set label 1 "Time: t(s)" at screen 0.475,0.025
# # set label 2 "Signal:U(mV)" at screen 0.025,0.44 rotate by 90
# # set label 3 center "U=exp(-t)sin(500t)" at screen 0.5,0.95
#
# # #  **** Margins ***
# # set rmargin 4
# # set bmargin 7
#

#
# #The left part
# set border 1+2+4    #the right border is not plotted
# set lmargin at screen 0.1   #the left-part's location
# set rmargin at screen 0.51
# # set xtics 0,0.02,0.08
# plot [*:*]"< tail -n +4 ".ifile." | head -n 10 && tail ".ifile u ($0):($0<10?$2:1/0):xtic(1) t 'After- the least' w lp ls 7 lw 1.8
# # plot [0:0.1] f(x) w l lt 1 lw 2 notitle
#
# #unset the labels and arrows, otherwise they will be plot
# #for the second time
# unset label 1
# unset label 2
# unset label 3
#
# unset arrow 1
# unset arrow 2
# unset arrow 3
# unset arrow 4
#
#
# #the right part
# set border 1+4+8    #the left border is not plotted
# set lmargin at screen 0.53      #the right-part's location
# set rmargin at screen 0.94
# #ytics is not plotted, as the second plot will share it with the first one
# unset ytics
# # set xtics 0.9,0.02,1.0
# plot "< tail -n +4 ".ifile." | head -n 10 && tail ".ifile u ($0+1):($0>=10?$2:1/0):xtic(1) t 'After- the most' w lp ls 5 lw 1.8,\
# "" u ($0):($0>=10?$2:1/0):xtic(1) t '' w p pt 6 ps 2 lc 0
#
# # plot [0.9:1] f(x) w l lt 1 lw 2
#
# unset multiplot

#  *** Axes format ***
# set format x '%.1tx10^{%T}'
set xtics rotate
set xtics format "{/:Bold %.0s}"
# set ytics font ",26"

# set border 2+8
set tmargin at screen 0.9   #the later two plots will share this tmargin
set bmargin  7 #at screen 0.1   #------------------------------------b------
#
# set multiplot    #begin multiplot mode
#
set object 1 polygon from first 9.4,0 to 9.4,30 to 9.4,30 to 9.6,30 to 9.6,0 to 9.6,0 to 9.4,0 front
set object 1 fillstyle solid 0.0 noborder front

#axes broken line
stats "< tail -n +4 ".ifile." | head -n 10 && echo \" \" && tail ".ifile u 2
set arrow 1  from 9.3,floor(STATS_min)-0.3 to 9.5,floor(STATS_min) + 0.3 nohead front lw 1.5
set arrow 2  from 9.5,floor(STATS_min)-0.3 to 9.7,floor(STATS_min) + 0.3 nohead front lw 1.5
set arrow 3  from 9.3,ceil(STATS_max)-0.3 to 9.5,ceil(STATS_max) + 0.3 nohead front lw 1.5
set arrow 4  from 9.5,ceil(STATS_max)-0.3 to 9.7,ceil(STATS_max) + 0.3 nohead front lw 1.5
# set arrow 5 from screen 0.5,0.1 to screen 0.54,0.1 nohead front lc rgb 'white' lw 1

# set xtics (0,1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19)
# set arrow 6 from 0,18 to 10,18 nohead lw 2
# set arrow 2 from 13.5,25 to 26.5,25 nohead
# set arrow 3 from 0,70 to 13,70 nohead
# set arrow 4 from 13.5,70 to 26.5,70 nohead

set grid
pr ifile

plot "< tail -n +4 ".ifile." | head -n 10 && echo \" \" && tail ".ifile u ($0):($0>=10?$2:1/0):xtic(1) index 0 t 'After- the most' w lp ls 5 lw 1.8,\
"" u ($0):($0>=10?$2:1/0):xtic(1) index 0 t '' w p pt 6 ps 2 lc 0,\
"" u ($0):($0<10?$2:1/0):xtic(1) t 'After- the least' w lp ls 7 lw 1.8,\
"" u ($0):($0<10?$2:1/0):xtic(1) t '' w p pt 6 ps 2 lc 0,\
'' u 4 t 'Initial'  w l ls 2 lw 4

# set xtics ("ffff" 10)
# newx(x) = (x<=100) ? x : (x>=1000) ? (x-1000)/10+110 : 0/0
# set xtics (0, 50, 100, '1000' newx(1000), '1500' newx(1500))
# set param
# set trange [10:1700] ; set samples 200
# set xrange [newx(10):newx(1700)]
# set log y
# set arrow from newx(10), graph 0 to newx(100), graph 0 nohead
# set arrow from newx(1000), graph 0 to newx(1700), graph 0 nohead
# plot newx(t), (newx(t))**2 title 'y=x^2'
# reset
