#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================


# COLOR DEFINITIONS
#--------------------------------------------------------------------------------------
set border linewidth 1.5
set style line 1 lt 1 lc rgb '#D73027' lw 2 # red
set style line 2 lt 1 lc rgb '#1B7837' lw 2 # dark green
set style line 3 lt 1 lc rgb '#FDAE61' lw 2 # 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' lw 2 # medium blue
set style line 5 lt 1 lc rgb '#9970AB' lw 2 # medium purple
# set style line 1 lc rgb '#800000' lt 1 lw 2
# set style line 2 lc rgb '#ff0000' lt 1 lw 2
# set style line 3 lc rgb '#ff4500' lt 1 lw 2
# set style line 4 lc rgb '#ffa500' lt 1 lw 2
# set style line 5 lc rgb '#006400' lt 1 lw 2
# set style line 6 lc rgb '#0000ff' lt 1 lw 2
# set style line 7 lc rgb '#9400d3' lt 1 lw 2

# ARGUMENTS
#--------------------------------------------------------------------------------------
#pathfile = '$0'
#profiles = system('ls -d ./'.pathdir.'*/ -1')
#names  = system('ls -d -1 ./'.pathdir.'*/ | tr "\n" "\0" | xargs -0 -n 1 basename')
#names = system('ls -d ./'.pathdir.'*/ | cut -d ')

ParentDir = '../Simulations/'
SimFolder = 'P3swapRankRegN500_homog/'
#pathdir = '../P3swapRankRegN500/ErrorBars/'.prof.'/'
ifile = 'sens_sorted.txt'
fileplot = ParentDir.SimFolder.ifile


# GENERAL FUNCTIONS
#--------------------------------------------------------------------------------------
#TheTitle(name) = sprintf("echo '%s' | sed 's/[a-z]//g;s/[\/*,._,]/ /g;s/  //g'",name)
#Title = "Profile ".system(TheTitle(prof)




# SET TERMINAL CONFIGURATION
#--------------------------------------------------------------------------------------
set terminal pdf size 20,10 font 'Verdana,20' enhanced color lw 3
# set terminal wxt enhanced font 'Verdana,10' persist size 1080,800
#set terminal pngcairo size 1080,800 enhanced font 'Verdana,12'
# set terminal pdf size 1080,800 enhanced font 'Verdana,12'
#set output 'SensitivityProfiles_sorted.pdf'
set output ParentDir.SimFolder.'SensitivityProfiles_sorted.pdf'


# SET GENERAL (global) PARAMETERS OF THE PLOTS
#--------------------------------------------------------------------------------------

#  *** Axes ***
#set style line 11 lc rgb '#808080' lt 1
set border lw 2
#set border 3 back ls 11
#set tics nomirror out scale 0.75


#  *** Grid ***
set style line 102 lc rgb '#d6d7d9' lt 0 lw 2
set grid back ls 102
# set style line 12 lc rgb'#808080' lt 0 lw 1
# set grid back
# set grid back ls 12
#set xtics 1 font ",8"
#set ytics 1 font ",8"


#  *** Axes Label ***
set ylabel "{/Symbol m}_{stimulated} - {/Symbol m}_{spontaneous} " font ",40" offset 0
# set title "Profile -1 1 -1 1" font ",18"
#set title Title font ",40"


#  **** Margins ***
set rmargin 4
set bmargin 7


#  *** Axes format ***
# set format x '%.1tx10^{%T}'
#set format x '%g'
set xtics rotate font ",24"
set xtics format "{/:Bold %.0s}"
set ytics font ",26"


#  *** Axes format ***
set loadpath '../ConfigGnuplot'
# load 'rdylbu.pal'
# load 'jet.pal'
# load 'moreland.pal'
# load 'set1.pal'
# load 'spectral.pal'


#  *** KeyBox ***
#set key horiz
#set key left top



# ADDITIONAL CONFIGURATIONS
#---------------------------------------------------------------------------------
#  *** LabelBox of each plot ***
title = 'in-out out-in in-in out-out'
set key top right box font ',28' spacing 5 width 3 opaque

#  *** Error bars configuration ***
# set style line 9 lt 1 lc rgb '#252525' lw 2 ps 0.7 pt 7 # dark grey


# list = "apple banana cabbage daikon eggplant"
# plot for [i in list] i.".dat" title i
# list = "new stuff"
# list = "apple banana cabbage daikon eggplant"
# item(n) = word(list,n)
# plot for [i=1:words(list)] item[i].".dat" title item(i)
# list = "new stuff"
#mytitle(n,m) = m==0?word(title,n):""


# PLOT SENSITIVITY
#--------------------------------------------------------------------------------------

plot fileplot u 2:xtic(1) t 'sensitivity' w lp pt 7 ps 1.5 lt 1 lw 3
#plot [*:*][*:*] for[m=2:5] ifile u ($1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 2.5,\
#for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls l-1 pt 7 ps 0.7 ,\
#for[k=2:5] pathdir.'ErrorBars_mode'.(k-2).'.txt' u ($1/1e6):2:3:4 t '' w yerrorbars ls k-1 lw 2

unset out
