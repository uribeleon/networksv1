#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================


#****************************************************************************
#  PLOTS
#****************************************************************************
# set style line 1 lc rgb '#0060ad' lt 1 lw 1.3 pt 7 ps 2  #blue
# set style line 2 lc rgb '#FF6347' lt 1 lw 1.5 pt 7 ps 2  #tomato-red
# set style line 3 lc rgb '#008000' lt 1 lw 1.5 pt 7 ps 2  #green
# set style line 4 lc rgb '#800080' lt 1 lw 1.5 pt 7 ps 2  #purple
# set style line 5 lc rgb '#FF8C00' lt 1 lw 1.5 pt 7 ps 2  #dark orange
# set style line 4 lc rgb '#708090' lt 1 lw 1.5 pt 7 ps 2  #slate gray

# COLOR DEFINITIONS
#--------------------------------------------------------------------------------------
set border linewidth 1.5
set style line 1 lt 1 lc rgb '#D73027' lw 2 # red
set style line 2 lt 1 lc rgb '#1B7837' lw 2 pt 7 ps 2 # dark green
set style line 3 lt 1 lc rgb '#FDAE61' lw 2 # 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' lw 2 # medium blue
set style line 5 lt 1 lc rgb '#9970AB' lw 2 # medium purple
set style line 6 lt 1 lc rgb '#00FFFF' lw 2 pt 7 ps 2 # cyan
set style line 10 lt 1 lc rgb '#00FFFF' lw 2 # dark cyan
# set style line 1 lc rgb '#800000' lt 1 lw 2
# set style line 2 lc rgb '#ff0000' lt 1 lw 2
# set style line 3 lc rgb '#ff4500' lt 1 lw 2
# set style line 4 lc rgb '#ffa500' lt 1 lw 2
# set style line 5 lc rgb '#006400' lt 1 lw 2
# set style line 6 lc rgb '#0000ff' lt 1 lw 2
# set style line 7 lc rgb '#9400d3' lt 1 lw 2


# ARGUMENTS
#--------------------------------------------------------------------------------------
#pathfile = '$0'
#profiles = system('ls -d ./'.pathdir.'*/ -1')
#names  = system('ls -d -1 ./'.pathdir.'*/ | tr "\n" "\0" | xargs -0 -n 1 basename')
#names = system('ls -d ./'.pathdir.'*/ | cut -d ')

ParentDir = '../Simulations/'
SimFolder = 'P3swapRankRegN500_homog/'
#pathdir = '../P3swapRankRegN500/ErrorBars/'.prof.'/'
ifile = 'AvgShortPath-Profiles-sorted.txt'
fileplot = ParentDir.SimFolder.ifile



# GENERAL FUNCTIONS
#--------------------------------------------------------------------------------------
#TheTitle(name) = sprintf("echo '%s' | sed 's/[a-z]//g;s/[\/*,._,]/ /g;s/  //g'",name)
#Title = "Profile ".system(TheTitle(prof)


# set terminal wxt enhanced font 'Verdana,10' persist size 1080,800

# SET TERMINAL CONFIGURATION
#--------------------------------------------------------------------------------------
set terminal pdf size 20,10 font 'Verdana,20' enhanced color lw 3
# set terminal wxt enhanced font 'Verdana,10' persist size 1080,800
#set terminal pngcairo size 1080,800 enhanced font 'Verdana,12'
# set terminal pdf size 1080,800 enhanced font 'Verdana,12'
#set output 'SensitivityProfiles_sorted.pdf'
set output ParentDir.SimFolder.'PLOT_AvgSP.pdf'


# AXES ---------------------------------------------------------
#set style line 11 lc rgb '#808080' lt 1
set border lw 2
#set border 3 back ls 11
#set tics nomirror out scale 0.75

# GRID ----------------------------------------------------------
set style line 102 lc rgb '#d6d7d9' lt 0 lw 2
set grid back ls 102
# set style line 12 lc rgb'#808080' lt 0 lw 1
# set grid back
# set grid back ls 12
#set xtics 1 font ",8"
#set ytics 1 font ",8"

# AXES LABEL -----------------------------------------------------
set ylabel "Average Shortest Path Length " font ",40" offset 0
# set title "Profile -1 1 -1 1" font ",18"
#set title Title font ",40"
set rmargin 4
set bmargin 7



# AXES FORMAT ----------------------------------------------------
# set format x '%.1tx10^{%T}'
#set format x '%g'
set xtics rotate font ",24"
set xtics format "{/:Bold %.0s}"
set ytics font ",26"




# CONFIGURATING PALETTE ------------------------------------------
set loadpath '../ConfigGnuplot'
# load 'rdylbu.pal'
# load 'jet.pal'
# load 'moreland.pal'
# load 'set1.pal'
# load 'spectral.pal'


# ADDITIONAL CONFIGURATIONS
#---------------------------------------------------------------------------------
# Title of each plot
title = 'Before After'
set key top left box font ',28' spacing 5 width 3 opaque

#set key horiz
#set key left top

#Error bars
# set style line 9 lt 1 lc rgb '#252525' lw 2 ps 0.7 pt 7 # dark grey


# list = "apple banana cabbage daikon eggplant"
# plot for [i in list] i.".dat" title i
# list = "new stuff"
# list = "apple banana cabbage daikon eggplant"
# item(n) = word(list,n)
# plot for [i=1:words(list)] item[i].".dat" title item(i)
# list = "new stuff"
#mytitle(n,m) = m==0?word(title,n):""

# PLOTTING -------------------------------------------------------------------
#plot 'AvgShortPath-Profiles-sorted.txt' u 2:xtic(1) t 'Before' w lp pt 7 ps 1.5 lt 1 lw 3

plot fileplot u 3:xtic(1) t 'After' w lp ls 6 lw 2, '' u 3:xtic(1) t '' pt 6 ps 2 lc 0 lw 1,\
'' u 2:xtic(1) t 'Before' w lp ls 2 lw 2, '' u 2:xtic(1) notitle pt 6 ps 2 lc 0 lw 1

#plot [*:*][*:*] for[m=2:5] ifile u ($1/1e6):m t mytitle(m-1,0) w l ls m-1 lw 2.5,\
#for[l=2:5] pathdir.'ErrorBars_mode'.(l-2).'.txt' u ($1/1e6):2 t '' w p ls l-1 pt 7 ps 0.7 ,\
#for[k=2:5] pathdir.'ErrorBars_mode'.(k-2).'.txt' u ($1/1e6):2:3:4 t '' w yerrorbars ls k-1 lw 2

unset out
