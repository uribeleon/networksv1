/*
 * 
 * 
 * USAGE: 
 * 	For one profile       ->    ./DPRewiring.out ./data/condiciones_selected.txt 900000 "Delta[0]>0"
 * 			      ->     /DPRewiring.out inifile 1000000 "Delta[0]>0 and Delta[3]<0" ./Probe-Profiles/Asortati.probe.txt ./Probe-Profiles/Matrix.txt
 * 	For several profiles  ->    ./DPRewiring.out 1000 ./data/condiciones_selected.txt ./data/names_selected.txt ./profiles/
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include "Allvars.h"
#include "Prototypes.h"
#include "conditions.cpp"

int main(int argc, char *argv[]){
  
  int i,j,training,nr;
  int EdgeR[4];
  int flag=0, ncond, mode, Nrewirings, EffRewirings=0;
  char cmd[100],*filename1,*filename2,*namedir,*inifile;
  char COND[80][256],NAMECOND[80][30],*cond;
  char *pos;
  double r,(*Rho)[4], terms[4], Delta[4], TMS[4][4];
  int *cij , *rw;
  int  resp;
  int condition[4];
  double rho[4];
  bool ans;
  FILE *ifile;
  FILE *ofAsort[NPROF],*ofMat,*ofPrm,*OFASORT;
  
  
  //===================================================================================
  // Initialiting variables
  //===================================================================================
  
  inifile = argv[1];  // File with initial matrix
  
  
  // Allocating general variables
  allocate_variables(inifile);
  
  // Reading adjacency matrix
  ReadMatrix(inifile);

  // Get number of conections
  Netwk.Nedges = GetNedges(Netwk.CIJ);
  
  // Allocating edge arrays
  for(i=0;i<NROWS;i++){
    Netwk.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); // 2 rows
    GV.edges[i] = (int *)malloc(Netwk.Nedges * sizeof(int)); 
  }
  

  // Get degrees of each node 
  GetDegreeNodes(Netwk.CIJ);

  // Get edges of network
  GetEdges(Netwk.CIJ,Netwk.edges);
  
  // Make copy of Edges
  CopyEdges(Netwk.edges,GV.edges,Netwk.Nedges);
  
  // Make copy of Adjacency Matrix
  CopyMatrix(Netwk.CIJ,GV.AIJ,Nc);

    
  //===================================================================================
  // Starting with Rewiring
  //===================================================================================

//-----------------------------------------------------------------
// TRAINING ONE PROFILE
//-----------------------------------------------------------------
#ifdef ONEPROFILE
  
  char *ofileassort;
  char *ofilematrix;
  
  // Get number of rewirings
  Nrewirings = atoi(argv[2]);
  
  // Get condition
  cond = argv[3];
  split_condition(argv[3]," ",condition);
  
  //Get ofile of assortativity
  ofileassort = argv[4];
  
  //Get ofile of final Matrix
  ofilematrix = argv[5];
  
  // Allocating assortativity arrays
  Rho = (double (*)[4])malloc(Nrewirings*4*sizeof(double));
  cij = (int *)malloc(Nc*Nc*sizeof(int));
  rw = (int *)malloc(Nrewirings*sizeof(int));
  
  //Calculating initial assortativity
  for(mode=0;mode<4;mode++){
    Rho[0][mode] = Assortativity(Netwk.CIJ,mode,&TMS[mode][0],true);
    printf("%f \n",Rho[0][mode]);
  }
  
  // 1. Doing rewiring for an specific condition
  for(nr=1;nr<Nrewirings;nr++){
    
    
    Rewire(GV.AIJ,GV.edges,EdgeR,&flag);

    // 2. If Rewire was succesfull -> flag=1
    if(flag){

      // Calculating DeltaR
      for(mode=0;mode<4;mode++){
	Delta[mode] = DeltaR(&TMS[mode][0],EdgeR,mode);
      }
      
      
      // 3. Verifiying if rewiring increment assortativity
//       if( EvalCondition(cond,Delta) ){
      if(evaluate_condition(Delta,condition)){
	EffRewirings++;
	
	//Calculating new asortativity
	for(mode=0;mode<4;mode++){
	  Rho[EffRewirings][mode] = Rho[EffRewirings-1][mode] + Delta[mode];
// 	  Rho[EffRewirings][mode] = rho[mode];
	}
	// Get edges of network
	GetEdges(GV.AIJ,GV.edges);

	rw[EffRewirings] = nr;
      }
      else{
	// Restoring adjacency matrix
	GV.AIJ[EdgeR[0]*Nc + EdgeR[1]] = 1;   GV.AIJ[EdgeR[0]*Nc + EdgeR[3]] = 0;
	GV.AIJ[EdgeR[2]*Nc + EdgeR[3]] = 1;   GV.AIJ[EdgeR[2]*Nc + EdgeR[1]] = 0;
      }
    }
  }
  
  //Saving files
  OFASORT = fopen(ofileassort,"w");
  for(i=0;i<EffRewirings;i++)
    fprintf(OFASORT,"%d %lf %lf %lf %lf\n",rw[i],Rho[i][0],Rho[i][1],Rho[i][2],Rho[i][3]);
//   fwrite(&Rho[0][0],sizeof(double),4*EffRewirings,ofAsort);
  fclose(OFASORT);
  
  SaveMatrix(GV.AIJ,ofilematrix);
  
  /*ofPrm = fopen("./data/OutPrm.txt","w");
  fprintf(ofPrm,"%d\n",EffRewirings);
  fclose(ofPrm)*/;
  
#endif

//-----------------------------------------------------------------
// TRAINING SEVERAL PROFILES
//-----------------------------------------------------------------
#ifdef SELECTEDPROFILES
  
  // Get number of rewirings
  Nrewirings = atoi(argv[1]);
  
  // Determining number of training
  filename1 = argv[2];
  sprintf(cmd,"awk 'END {print NR}' %s",filename1);
  ncond = exec(cmd);
  printf("ncond: %d\n",ncond);
  
  // names
  filename2 = argv[3];
  
  // Get Name of dir
  namedir = argv[4];
  
  // Allocating assortativity arrays
  Rho = (double (*)[4])malloc(Nrewirings*4*sizeof(double));
//   cij = (int *)malloc(Nc*Nc*sizeof(int));
  rw = (int *)calloc(Nrewirings,sizeof(int));
  
  //---------------------------------------------
  // Opening and reading conditions file
  //---------------------------------------------
  i=0;
  ifile = fopen(filename1,"r");
  while(fgets(cmd,99,ifile)){
    if((pos=strchr(cmd,'\n')) != NULL)
      *pos = '\0'; //eliminate \n at the end of line
    strcpy(COND[i],cmd);
//      printf("Line %d: %s\n",i,COND[i]);
    i++;
  }
  fclose(ifile);
  
  //----------------------------------------------
  // Reading file with the name of each condition
  // in order to training each profile
  //----------------------------------------------
  i=0;
  ifile = fopen(filename2,"r");
  while(fgets(cmd,99,ifile)){
    if((pos=strchr(cmd,'\n')) != NULL)
      *pos = '\0'; //eliminate \n at the end of line
    strcpy(NAMECOND[i],cmd);
//      printf("Line %d: %s\n",i,COND[i]);
    i++;
  }
  fclose(ifile);
  
  //----------------------------------------------
  // Creating the directories
  //----------------------------------------------
  struct stat s;
  cmd[0] = 0;
  if (stat(namedir,&s)==-1){
    printf("***  Creating Parent folder with each profile folders  \n");
    resp = mkdir(namedir,0700);
    for(i=0;i<ncond;i++){
      sprintf(cmd,"%s%s/",namedir,NAMECOND[i]);
      mkdir(cmd,00700);
    }
  }
  else{
    printf("***  folder %s already exist!!!. \n     You must provide a new name or rename the existing folder\n",namedir);
    printf("      Deleting current folder and creating the new folder...\n");
    sprintf(cmd,"rm -rf %s",namedir);
    system(cmd);
    
    resp = mkdir(namedir,0700);
    for(i=0;i<ncond;i++){
      sprintf(cmd,"%s%s/",namedir,NAMECOND[i]);
      mkdir(cmd,00700);
    }
  }
  
  //Calculating initial assortativity
  for(mode=0;mode<4;mode++){
    Rho[0][mode] = Assortativity(Netwk.CIJ,mode,&TMS[mode][0],true);
  }
  
   
  //----------------------------------------------------------------------------------------------------------------------
  // STARTING WITH THE TRAININGS
  //----------------------------------------------------------------------------------------------------------------------
  // 1. Doing trainings for each conditions
  for(training=0;training<ncond;training++){
    
    rw[0] = 0;
    EffRewirings = 0;
    StateOfProcess(training,ncond,NAMECOND[training]);
    //Make copy of initial Adjacency Matrix
    CopyMatrix(Netwk.CIJ,GV.AIJ,Nc);
    
    // 2. Doing rewiring for an specific condition
    for(nr=1;nr<Nrewirings;nr++){
      
      
      Rewire(GV.AIJ,GV.edges,EdgeR,&flag);
      
      // 3. If Rewire was succesfull
      if(flag){
	
	// Calculating DeltaR
	for(mode=0;mode<4;mode++){
	  Delta[mode] = DeltaR(&TMS[mode][0],EdgeR,mode);
	}
	
	//****************************************************************
	// 4. Verifiying if rewiring increment assortativity
	//****************************************************************
	if( EvalCondition(COND[training],Delta) ){
	  
	  EffRewirings++;
	  
	  //Calculating new asortativity
	  for(mode=0;mode<4;mode++){
	    Rho[EffRewirings][mode] = Rho[EffRewirings-1][mode] + Delta[mode];
	  }
	  
	  // Get edges of network
	  GetEdges(GV.AIJ,GV.edges);

	  rw[EffRewirings] = nr;
	}
	else{
	  // Restoring adjacency matrix
	  GV.AIJ[EdgeR[0]*Nc + EdgeR[1]] = 1;   GV.AIJ[EdgeR[0]*Nc + EdgeR[3]] = 0;
	  GV.AIJ[EdgeR[2]*Nc + EdgeR[3]] = 1;   GV.AIJ[EdgeR[2]*Nc + EdgeR[1]] = 0;
	}
      }
    }
    
    //--------------------------------------------------------------------------------------
    // SAVING FILES
    //--------------------------------------------------------------------------------------
        
    // ASSORTATIVITY
    sprintf(cmd,"%s%s/Assort%s.txt",namedir,NAMECOND[training],NAMECOND[training]);
    ofAsort[training] = fopen(cmd,"w");
    for(i=0;i<EffRewirings;i++){
      fprintf(ofAsort[training],"%d %lf %lf %lf %lf\n",rw[i],Rho[i][0],Rho[i][1],Rho[i][2],Rho[i][3]);
    }
    
    //MATRIX
    sprintf(cmd,"%s%s/Matrix%s.txt",namedir,NAMECOND[training],NAMECOND[training]);
    SaveMatrix(GV.AIJ,cmd,false);
    
  }
  fcloseall();
  free(Rho);
  free(rw);
#endif
  
  free(rw);
  free(Rho);
  free_memory();
  
//   for(i=0;i<NROWS;i++){
//     free(Netwk.edges[i]);
//   }

//   for(i=0;i<NROWS;i++){
//     free(GV.edges[i]);
//   }


}
